var bcrypt = require('bcrypt-nodejs'),
    SALT_WORK_FACTOR = 10,
    mongoose = require('mongoose'),
    async = require('async')
    _ = require('underscore');


exports.comparePassword = function comparePassword(candidatePassword, cb) {
  var user = this;
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if(err) {
      console.log(err);
      return cb(err);
    }
    console.log(isMatch)
    cb(null, isMatch);
  });
};

exports.hashPassword = function hashPassword(next) {
  var user = this;
  if(!user.isModified('password')) return next();
  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
      if(err) return next(err);
      bcrypt.hash(user.password, salt, function (err, hash) {
        if(err) return next(err);
        user.password = hash;
        next();
      });
  });
};

exports.filterUser = function filterUser(user) {
  if(user) {
    user.password = undefined;
    return user;
  } else {
    return null;
  }
}

exports.validatePresenceOf = function validatePresenceOf(value) {
  return (value && value.length);
};

exports.validateUniqueUsername = function validateUniqueUsername(value, respond) {
  var User = mongoose.model('User');
  var AdminUser = mongoose.model('AdminUser');
  async.parallel([
    function (callback) {
      User.find({ username: value }, function (err, user) {
        callback(null, err || user.length === 0);
      })
    },
    function (callback) {
      AdminUser.find({ username: value }, function (err, user) {
        callback(null, err || user.length === 0);
      });
    }
  ], function (err, result) {
    console.log(result)
    respond(result[0]&&result[1]);
  });
};