var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/users');
var AdminUser = require('../models/adminusers');
var ObjectId = require('mongoose').Types.ObjectId;
var _ = require('underscore');
var async = require('async');


passport.serializeUser(function (user, done) {
  console.log(user)
  done(null, user._id);
});

passport.deserializeUser(function (id, done) {
  async.parallel([
    function (cb) {
      User.findById(id, cb);
    },
    function (cb) {
      AdminUser.findById(id, cb);
    }
  ], function (err, user) {
    done(err, user[0]||user[1]);
  });
});

passport.use('user-local', new LocalStrategy({usernameField: 'username', passwordField:'password', passReqToCallback: true }, function (req, username, password, done) {
  User.findOne({ username: username, 'store.storeId': new ObjectId(req.body.storeId) }, function (err, user) {
    if (err) { 
      return done(err);
    } else if (!user) {
      return done(null, false, { message: 'Unknown user ' + username });
    } else {
      user.comparePassword(password, function (err, isMatch) {
        if (err) return done(err);
        if(isMatch) {
          return done(null, user);
        } else {
          return done(null, false, { message: 'Invalid password' });
        }
      });
    }
  });
}));

passport.use('adminuser-local', new LocalStrategy({usernameField: 'username', passwordField:'password'}, function (username, password, done) {
  AdminUser.findOne({ username: username }, function (err, user) {
    if (err) { 
      return done(err);
    } else if (!user) {
      return done(null, false, { message: 'Unknown user ' + username });
    } else {
      user.comparePassword(password, function (err, isMatch) {
        if (err) return done(err);
        if(isMatch) {
          return done(null, user);
        } else {
          return done(null, false, { message: 'Invalid password' });
        }
      });
    }
  });
}));

function isAdminUser(req) {
  return req.isAuthenticated() && req.user && req.user.admin_user;
}

function isStoreUser(req) {
  return req.isAuthenticated() && req.user && _.isUndefined(req.user.admin_user);
}

exports.ensureAdminUserAdminRole = function ensureAdminUserAdminRole(req, res, next) {
  if(isAdminUser(req) && req.user.admin) {
    return next();
  } else {
    return res.status(401).json("ERROR NOT AN ADMIN OF ADMINUSER");
  }
}

exports.ensureAdminUser = function ensureAdminUser(req, res, next) {
  if(isAdminUser(req)) {
    return next();
  } else {
    return res.status(401).json("ERROR NOT AN ADMINUSER");
  }
}

exports.ensureAuthenticated = function ensureAuthenticated(req, res, next) {
  if(isAdminUser(req) || isStoreUser(req)) {
    return next();
  } else {
    return res.status(401).json("ERROR NOT AUTHENTICATED");
  }
}

exports.adminRequired = function adminRequired(req, res, next) {
  if(isAdminUser(req)) {
    return next();
  } else {
    if (req.user && req.user.admin ) {
      return next();
    } else {
      return res.status(401).json("Need higher access role");
    }
  }
};