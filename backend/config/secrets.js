path = require('path');

module.exports = {

  //- TODO: clone remote DB to local
  db: {
    remote: process.env.MONGOLAB_URI,
    local: process.env.MONGOLAB_URI
  },

  twilio: {
    sid: process.env.TWILIO_SID,
    token: process.env.TWILIO_TOKEN
  },

  aws: {
    region: 'us-west-2',
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    s3Bucket: process.env.AWS_S3_BUCKET
  },

  server: {
    listenPort: process.env.PORT,
    securePort: process.env.SECURE_PORT || 8433,
    publicStore: {
      binFolder: path.resolve(__dirname, '../../public_store/bin'), // minified application files (production)
      buildFolder: path.resolve(__dirname, '../../public_store/build'), // build fileds (development)
    },
    publicAdmin: {
      binFolder: path.resolve(__dirname, '../../public_admin/bin'), // build fileds (development)
      buildFolder: path.resolve(__dirname, '../../public_admin/build'), // build fileds (development)
    },
    cookieSecret: 'telekom-admin panegl heks',
    cookieKey:'cellbetter-app.sid',
    privateKey: path.resolve(__dirname, '../cert/key.pem'),
    certificate: path.resolve(__dirname, '../cert/cert.pem')
  }

};
