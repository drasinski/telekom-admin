var ObjectId = require('mongoose').Types.ObjectId;


exports.newNote = function newNote(Note) {
  return function (req, res, next) {
    var note = new Note({
      content: req.body.content,
      date: req.body.date,
      store: {
        storeId: new ObjectId(req.params.storeId)
      }
    });
    note.save(function (err, note) {
      if(!err) {
        res.send(201, note);
      } else {
        return next(err);
      }
    });
  }
};

exports.updateNote = function updateNote(Note) {
  return function (req, res, next) {
    Note.findOne({'_id': req.params.noteId, 'store.storeId':req.params.storeId}, function (err, note) {
      if(!err && note) {
        note.content = req.body.content;
        note.save(function (err, note) {
          if(!err) {
            return res.send(200, note);
          } else {
            return next(err);
          }
        });
      } else if(err) {
        return next(err);
      } else {
        return res.send(404, "Couldn't find Note");
      }
    });
  }
};

exports.getNotes = function getNotes(Note) {
  return function (req, res, next) {
    Note.apiQuery(req.query, function (err, notes) {
      if(err) {
        return next(err);
      } else if(notes) {
        return res.send(200, notes);
      } else {
        return res.send(404, "Couldn't find any notes");
      }
    });
  }
};


exports.deleteNote = function deleteNote(Note) {
  return function (req, res, next) {
    Note.findOne({'_id':req.params.noteId, 'store.storeId':req.params.storeId}, function (err, note) {
      if(note && !err) {
        note.remove(function (err) {
          if(err) {
            return next(err);
          } else {
            res.send(200, req.params.noteId);
          }
        })
      } else if(err) {
        return next(err)
      } else {
        return res.send(404, "Couldn't Find Note");
      }
    })
  }
};