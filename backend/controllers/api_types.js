/* The API controller
   Exports 3 methods:
   * post - Creates a new client
   * list - Returns all clients
   * show - Displays a thread and its posts
*/

var _ = require('underscore'),
  async = require('async'),
  ObjectId = require('mongoose').Types.ObjectId;

exports.getTypes = function getTypes(Type) {
  return function (req, res, next) {
    Type.apiQuery(req.query, function (err, types) {
      if(err) {
        return next(err);
      } else if(types) {
        return res.send(200, types);
      } else {
        return res.send(404, "Couldn't find any types");
      }
    });
  };
};

exports.newType = function newType(Type) {
  return function (req, res, next) {
    var type = new Type({
      category: req.body.category,
      active: req.body.active,
      description: req.body.description,
      icon: req.body.icon,
      system: req.body.system,
      profit: req.body.profit,
      prediction: req.body.prediction,
      store: {
        storeId: new ObjectId(req.params.storeId)
        // locationId: req.body.store.locationId
      }
    });
    type.save(function (err, type) {
      if(!err) {
        return res.send(201, type);
      } else {
        return next(err);
      }
    });
  }
};

exports.updateType = function updateType(Type) {
  return function (req, res, next) {
    Type.findOne({'store.storeId':req.params.storeId, '_id':req.params.typeId }, function (err, type) {
      if(!err && type) {
        type.category = req.body.category;
        type.active = req.body.active;
        type.description = req.body.description;
        type.icon = req.body.icon;
        type.system = req.body.system
        type.profit = req.body.profit;
        type.prediction = req.body.prediction;
        // type.store.locationId = req.body.store.locationId;
        type.save(function (err, type) {
          if(!err) {
            return res.send(200, type);
          } else {
            return next(err);
          }
        });
      } else {
        return next(err);
      }
    });
  }
};

exports.deleteType = function deleteType(Type) {
  return function (req, res, next) {
    Type.findOne({'_id':req.params.typeId, 'store.storeId':req.params.storeId }, function (err, type) {
      if(type && !err) {
        type.remove(function (err) {
          if(err) {
            return next(err);
          } else {
            return res.send(200, req.params.typeId);
          }
        });
      } else {
        return next(err);
      }
    });
  }
}


