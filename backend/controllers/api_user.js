var passport = require('passport');
var _ = require('underscore');
var mime = require('mime');
var secrets = require('../config/secrets');
var ObjectId = require('mongoose').Types.ObjectId;
var async = require('async');
var userHelper = require('../lib/user-helpers');

exports.login = function login(req,res,next) {
  async.parallel([
    function (callback) {
      passport.authenticate('user-local', function (err, user) {
        callback(err, user);
      })(req,res,next);
    },
    function (callback) {
      passport.authenticate('adminuser-local', function (err, user) {
        callback(err, user);
      })(req,res,next);
    }
  ], function (err, userArray) {
    var user = userArray[0]||userArray[1];
    if(err) {
      console.log(err + '  Error1');
      return next(err)      
    }
    if(!user) {
      console.log(user + '  No User');
      return res.send(null);      
    }
    req.logIn(user, function (err) {
      if(err) {
        console.log(err + 'Error2');
        return next(err);
      }
      if(req.body.rememberme) {
        req.session.cookie.maxAge = 1000 * 60 * 60 * 24 * 7;
      }
      if(user.admin_user) {
        user.store = {
          storeId: req.params.storeId,
        }
        user.admin = true;
      }
      return res.status(200).json(userHelper.filterUser(user));
    });
  });
}

exports.logout = function logout(req,res) {
  req.logout();
  return res.send(204);
}


exports.currentUser = function currentUser(req,res){
  var storeId = new ObjectId(req.params.storeId);
  if(req.isAuthenticated()) {
    if(req.user.admin_user) {
      req.user.admin = true;
      req.user.store = {
        storeId: storeId,
      }
      return res.json(200, userHelper.filterUser(req.user))
    } else if(req.user.store.storeId.equals(storeId)) {
      return res.json(200, userHelper.filterUser(req.user));
    } else {
      console.log("User is NOT Apart of this store");
      return res.send(401);      
    }
  } else {
    console.log("User is NOT Authenticated");
    return res.send(401);
  }
};

exports.getUsers = function getUsers(User) {
  return function (req, res, next) {
    User.apiQuery(req.query, function (err, users) {
      if(!err) {
        users = _.map(users, function (value) {
          return userHelper.filterUser(value);
        });
        return res.status(200).send(users);
      } else {
        return next(err);
      }
    });
  };
}

exports.getUser = function getUser(User) {
  return function (req,res, next) {
    User.findOne({'username':req.params.username,'store.storeId': new ObjectId(req.params.storeId) }, function (err, user) {
      if(!err && user) {
        console.log("Retrieving user:  " + user);
        return res.send( 200, userHelper.filterUser(user) );
      } else {
        return next(err);
      }
    });
  };
};

exports.deleteUser = function deleteUser(User, AWS) {
  return function (req,res, next) {
    var query = {'_id':req.params.userId, 'store.storeId': new ObjectId(req.params.storeId) };
    User.findOneAndRemove(query, function (err, user) {
      if(!err) {
        if(!_.isUndefined(user.thumbnail)) {
          var s3 = new AWS.S3();
          var filename = user.thumbnail.substring(user.thumbnail.lastIndexOf('/')+1);
          console.log(filename)
          s3.deleteObject({Bucket: secrets.aws.s3Bucket, Key:filename}, function (err, response) {
            if(err) {
              console.log("Could not delete");
            } else {
              console.log("Successfully deleted thumbnail");
            }
          });
        }
        console.log("Successfully removed user");
        return res.send( 200, userHelper.filterUser(user) );
      } else {
        return next(err);
      }
    });
  };
};

exports.newUser = function newUser(User) {
  return function (req,res, next) {
    var u = new User({
      username:req.body.username,
      firstname:req.body.firstname,
      lastname:req.body.lastname,
      email:req.body.email,
      store: {
        locationId: req.body.store.locationId,
        storeId: new ObjectId(req.params.storeId)
      },
      admin:req.body.admin,
      password:req.body.password,
    })
    u.save(function (err) {
      if(!err) {
        console.log("Successfully created user");
        return res.send( 201, userHelper.filterUser(u) );
      } else {
        return next(err);
      }
    });
  };
};

exports.uploadImage = function uploadImage(User, AWS) {
  return function (req, res, next) {
    var filename = req.params.userId+'-thumbnail.png';
    var s3 = new AWS.S3();
    var url = 'https://s3-'+secrets.aws.region+'.amazonaws.com/'+secrets.aws.s3Bucket+'/'+filename;
    var data = {
      Bucket: secrets.aws.s3Bucket,
      Key: filename,
      ACL: 'public-read',
      Body: req.files.file.buffer,
      ContentType: mime.lookup(req.files.file.originalname)
    };
    s3.putObject(data, function (err, response) {
      if (err) {
        return next(err);
      } else {
        User.findOne({'_id':req.params.userId,'store.storeId': new ObjectId(req.params.storeId)}, function (err, user) {
          if(!err && user) { 
            user.thumbnail = url;
            user.save(function (err) {
              if(!err) {
                res.send(201, user.thumbnail);
              } else {
                return next(err);
              }
            })
          }
          else { 
            return next(err);
          }
        });
      }
    });
  };
};

exports.updateUser = function updateUser(User) {
  return function (req,res, next) {
    var query = {
      '_id':req.params.userId, 
      'store.storeId': new ObjectId(req.params.storeId)
    };
    User.findOne(query, function (err, user) {
      if(!err) {
        user.email = req.body.email;
        // user.username = req.body.username;
        user.firstname = req.body.firstname;
        user.lastname = req.body.lastname;
        user.store.locationId = req.body.store.locationId;
        if (req.user && (req.user.admin || req.user.admin_user)) {
          user.admin = req.body.admin;
        }
        if(req.body.password) {
          user.password = req.body.password;
        }
        user.save(function (err) {
          if(!err) {
            console.log("Successfully updated user");
            return res.send( 200, userHelper.filterUser(user) );
          } else {
            return next(err);
          }
        });
      } else {
        return next(err);
      }
    });
  };
};

