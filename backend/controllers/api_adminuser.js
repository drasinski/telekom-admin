var LocalStrategy = require('passport-local').Strategy;
var passport = require('passport');
var _ = require('underscore');
var userHelper = require('../lib/user-helpers');


exports.login = function login(req,res,next) {
  passport.authenticate('adminuser-local', function (err, user) {
    if(err) {
      console.log(err + '  Error1');
      return next(err)
    }
    if(!user) {
      console.log(user + '  No User');
      return res.send(null);
    }
    req.logIn(user, function (err) {
      if(err) {
        console.log(err + 'Error2');
        return next(err);
      }
      if(req.body.rememberme) {
        req.session.cookie.maxAge = 1000 * 60 * 60 * 24 * 7;
      }
      return res.status(200).json(userHelper.filterUser(user));
    });
  })(req, res, next);
};

exports.logout = function logout(req,res){
  req.logout();
  return res.send(204);
};

exports.currentUser = function currentUser(req,res){
  if (req.isAuthenticated() && req.user && req.user.admin_user) {
    return res.status(200).json(userHelper.filterUser(req.user));
  } else {
    console.log("User is NOT Authenticate d");
    return res.send(401);
  }
};

exports.getUsers = function getUsers(AdminUser) {
  return function (req, res, next) {
    AdminUser.apiQuery(req.query, function (err, users) {
      if(!err) {
        users = _.map(users, function (value) {
          return userHelper.filterUser(value);
        });
        return res.send( 200, users );
      } else {
        return next(err);
      }
    });
  };
};

exports.updateUser = function updateUser(AdminUser) {
  return function (req, res, next) {
    AdminUser.findOne(req.params.adminUserId, function (err, user) {
      if(!err) {
        user.email = req.body.email;
        user.firstname = req.body.firstname;
        user.lastname = req.body.lastname;
        user.admin = req.body.admin;
        if(req.body.password) {
          user.password = req.body.password;
        }
        user.save(function (err) {
          if(!err) {
            console.log("Successfully updated user");
            return res.send( 200, userHelper.filterUser(user) );
          } else {
            return next(err);
          }
        });
      } else {
        return next(err);
      }    
    });
  }
};

exports.deleteUser = function deleteUser(AdminUser) {
  return function (req, res, next) {
    AdminUser.findOneAndRemove({'_id':req.params.adminUserId}, function (err, user) {
      if(!err) {
        return res.send(200, userHelper.filterUser(user));
      } else {
        return next(err);
      }
    });
  }
};

exports.newUser = function newUser(AdminUser) {
  return function (req,res, next) {
    var u = new AdminUser({
      username:req.body.username,
      firstname:req.body.firstname,
      lastname:req.body.lastname,
      email:req.body.email,
      admin:req.body.admin,
      admin_user:true,
      password:req.body.password,
    });
    u.save(function (err) {
      if(!err) {
        console.log("Successfully created user");
        return res.send( 201, userHelper.filterUser(u) );
      } else {
        return next(err);
      }
    });
  };
};
