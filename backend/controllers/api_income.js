var moment = require('moment'),
    ObjectId = require('mongoose').Types.ObjectId;


exports.updateIncome = function updateIncome(Income) {
  return function (req, res, next) {
    var query = { '_id':req.params.incomeId, 'store.storeId':req.params.storeId};
    Income.findOne(query, function (err, income) {
      if(!err && income) {
        income.store = req.body.store;
        income.date = req.body.date;
        income.expenses = req.body.expenses;
        income.income = req.body.income;
        income.summary = req.body.summary;
        income.other_income = req.body.other_income;
        income.save(function (err) {
          if(!err) {
            return res.send(200, income);
          } else {
            return next(err);
          }
        })
      } else {
        return res.send(404,"Couldn't find:  " + req.params.incomeId);
      }
    });
  } 
};

exports.newIncome = function newIncome(Income) {
  return function (req, res, next) {
    console.log(req.body)
    var income = new Income({
      income: req.body.income,
      other_income: req.body.other_income,
      expenses: req.body.expenses,
      summary: req.body.summary,
      date: req.body.date,
      store:{
        storeId: new ObjectId(req.params.storeId)
      }
    });
    income.save(function (err, income) {
      if(!err) {
        return res.send(201, income);
      } else {
        return next(err);
      }
    });
  }
}

exports.getIncomes = function getIncomes(Income) {
  return function (req, res, next) {
    console.log(JSON.stringify(req.query));
    Income.apiQuery(req.query, function (err, incomes) {
      if(err) {
        return next(err);
      } else if(incomes) {
        return res.send(200,incomes);
      } else {
        return res.send(404, "Could not send incomes");
      }
    })
  }
}

// GET /incomes/:incomeId
exports.getIncome = function getIncome(Income) {
  return function (req,res,next) {
    Income.findOne({'_id':req.params.incomeId, 'store.storeId': req.params.storeId}, function (err, income) {
      if(err) {
        return next(err);
      } else if(income) {
        return res.send(200,income);
      } else {
        return res.send(404, "Couldn't find income statement:  " + 2);
      }
    });
  }
};
