/* The API controller
   Exports 3 methods:
   * post - Creates a new client
   * list - Returns all clients
   * show - Displays a thread and its posts
*/

var _ = require('underscore'),
  async = require('async'),
  ObjectId = require('mongoose').Types.ObjectId;

exports.deleteInvoice = function deleteInvoice(Invoice) {
  return function (req,res, next) {
    Invoice.findOne({'_id':req.params.invoiceId, 'store.storeId':req.params.storeId}, function (err, invoice) {
      if(invoice && !err) {
        res.locals.invoice = invoice;
        invoice.remove(function (err) {
          if(err) {
            return next(err);
          } else {
            return next();
          }
        });
      } else if(err) {
        return next(err);
      } else {
        return res.send(404, "Couldn't find invoice");
      }
    });
  };
};

exports.newInvoice = function newInvoice(Invoice) {
  return function (req, res, next) {
    var invoice = new Invoice({
      subtotal: req.body.subtotal,
      taxtotal: req.body.taxtotal,
      grandtotal: req.body.grandtotal,
      credit_total: req.body.credit_total,
      cash_total: req.body.cash_total,
      check_total: req.body.check_total,
      invoice_number: req.body.invoice_number,
      servicecharge: req.body.servicecharge,
      user: req.body.user,
      client: req.body.client,
      store: {
        storeId: new ObjectId(req.params.storeId),
        locationId: req.body.store.locationId
      },
      transactions: req.body.transactions,
    });    
    invoice.save(function (err, invoice) {
      if(!err) {
        res.locals.invoice = invoice;
        return next();
      } else {
        return next(err);
      }
    });
  }
}

// PUT CLIENT
exports.updateInvoice = function updateInvoice(Invoice, Inventory, Client) {
  return function (req, res, next) {
    Invoice.findOne({'_id':req.params.invoiceId, 'store.storeId':req.params.storeId}, function (err, invoice) {
      if(!err && invoice) {
        // if(_.intersection(invoice.getPhoneTransactions.call(req.body), invoice.getPhoneTransactions()).length !== invoice.getPhoneTransactions().length) {
        //   console.log(_.intersection(invoice.getPhoneTransactions.call(req.body), invoice.getPhoneTransactions()))
          async.series([
            function (callback) {
                Inventory.returnPhonesToInventory(invoice.getPhoneTransactions(), invoice.store.storeId, callback);
            },
            function (callback) {
                Inventory.addPhonesToSoldInventory(invoice.getPhoneTransactions.call(req.body), invoice.store.storeId, invoice._id, callback);
            }
          ]);
        // }
        invoice.subtotal = req.body.subtotal;
        invoice.taxtotal = req.body.taxtotal;
        invoice.client = req.body.client
        invoice.grandtotal = req.body.grandtotal;
        invoice.cash_total = req.body.cash_total;
        invoice.credit_total = req.body.credit_total;
        invoice.check_total = req.body.check_total;
        invoice.transactions = req.body.transactions;
        invoice.servicecharge = req.body.servicecharge;
        invoice.saletotal = req.body.saletotal;
        invoice.store.locationId = req.body.store.locationId;
        invoice.user = req.body.user;
        // A different number is supplied to the invoice, overwrite the existing one
        // and remove that invoice from that client
        // var oldTel = _.clone(invoice.telephone);
        // if(oldTel != req.body.telephone && !_.isEmpty(oldTel)) {
        //     Client.findOne({telephone:oldTel, storeId:new ObjectId(req.params.storeId)}, function (err, client) {
        //         if(!err && client) {
        //             client.removeInvoice(invoice._id, function (err) {
        //                 if(!err) {
        //                     console.log("Successfully removed invoice from old client");
        //                 }
        //             });
        //         }
        //     });
        // }
        // invoice.telephone = req.body.telephone;
        invoice.save(function (err, invoice) {
          if(!err) {
            return res.send(200, invoice);
          } else {
            return next(err);
          }
        });
      } else if(err) {
          return next(err);
      } else {
        return res.send(404, "Couldn't find invoice");
      }
    });
  }
}

exports.getInvoices = function getInvoices(Invoice) {
  return function (req, res, next) {
    var select = 'category category_slug active description icon system system_slug store' 
    if(req.user && (req.user.admin || req.user.admin_user)) {
      select = select + ' profit';
    }
    Invoice.apiQuery(req.query).populate([
      {
        path:'transactions.typeId',
        select: select
      },
      {
        path:'transactions.inventoryId'
      }
    ]).exec(function (err, invoices) {
      if(err) {
        return next(err);
      } else if(invoices) {
        return res.send(200, invoices);
      } else {
        return res.send(404, "Couldn't find any invoices");
      }
    });
  };
};

exports.getInvoice = function getInvoice(Invoice) {
  return function (req, res, next) {
    Invoice.findOne({'_id':req.params.invoiceId, 'store.storeId':req.params.storeId}, function (err, invoice) {
      if(err) {
        return next(err);
      } else if(invoice) {
        console.log("Found Invoice:  " + req.params.invoiceId);
        return res.send(200,invoice);
      } else {
        return res.send(404,"Couldn't find any invoices");
      }
    });
  };
};
