
exports.getStore = function getStore(Store) {
  return function (req,res,next) {
    Store.findOne({slug: req.params.storeId, active:true}, function (err, store) {
      if(err) {
        return next(err);
      } else if(store && !err) {
        return res.send(200, store);
      } else {
        return res.send(404, "Couldn't find store:  " + req.params.storeId);
      }
    });
  }
};

exports.getStores = function getStores(Store) {
  return function (req, res, next) {
    Store.apiQuery(req.query, function (err, stores) {
      if(err) { 
        return next(err) 
      } else if(stores) {
        return res.send(200, stores);
      } else {
        return res.send(404,"Couldn't find any stores");
      }
    });
  }
};

exports.updateStore = function updateStore(Store) {
  return function (req, res, next) {
    Store.findById(req.params.storeId, function (err, store) {
      if(!err && store) {
        store.slug = req.body.slug;
        store.twilio_number = req.body.twilio_number;
        store.active = req.body.active;
        store.cc_fee = req.body.cc_fee;
        store.tax_fee = req.body.tax_fee;
        store.website = req.body.website;
        store.service_agreement = req.body.service_agreement;
        store.contact = req.body.contact;
        store.name = req.body.name;
        store.locations = req.body.locations;
        store.save(function (err, store) {
          if(!err) {
            return res.send(200, store);
          } else {
            return next(err);
          }
        });
      } else {
        return next(err);
      }
    });
  }
};

exports.newStore = function newStore(Store) {
  return function (req, res, next) {
    var newStore = new Store({
      cc_fee: req.body.cc_fee,
      active:true,
      service_agreement: req.body.service_agreement,
      tax_fee: req.body.tax_fee,
      slug: req.body.slug,
      contact: req.body.contact,
      website: req.body.website,
      name: req.body.name,
      twilio_number: req.body.twilio_number,
      locations: req.body.locations
    });
    newStore.save(function (err, store) {
      if(!err) {
        console.log("created new store");
        return res.send(201, store);
      } else {
        console.log(err);
        return next(err);
      }
    })
  }
};