var _ = require('underscore'),
    ObjectId = require('mongoose').Types.ObjectId;;

exports.getItems = function getItems(Inventory) {
  return function (req,res, next) {
    Inventory.apiQuery(req.query, function (err, items) {
      if(!err) {
        return res.send(200, items);
      } else {
        return next(err);
      }
    });
  };
};

exports.getItem = function getItem(Inventory) {
  return function (req,res, next) {
    Inventory.findOne({'_id':req.params.itemId, 'store.storeId':req.prams.storeId}).populate('invoice').exec(function (err, item) {
      if(err) {
        return next(err);
      } else if(item) {
        return res.send(200,item);
      } else {
        return res.send(404,"Couldnt find inventory item:  " + req.params.id);
      }
    });
  };
};

exports.newItem = function newItem(Inventory) {
  return function (req,res, next) {
    var inventory = new Inventory({
      amount: req.body.amount,
      description: req.body.description,
      imei: req.body.imei,
      fixed_cost: req.body.fixed_cost,
      location: req.body.location,
      store: {
        storeId: new ObjectId(req.params.storeId),
        locationId: req.body.store.locationId
      }
    });
    inventory.save(function (err, item){
      if(!err) {
        return res.send(201, item);
      } else {
        return next(err);
      }
    });
  };
};

exports.deleteItem = function deleteItem(Inventory) {
  return function (req, res, next) {
    Inventory.findOne({'_id':req.params.itemId, 'store.storeId':req.params.storeId}, function (err, item) {
      if(!err && item) {
        item.remove(function (err) {
          if(err) {
            return next(err);
          } else {
            console.log('Removed:  '+req.params.itemId);
            return res.send(200,req.params.itemId);  
          }
        });
      } else {
        return next(err);
      }
    });
  };
};

exports.updateItem = function updateItem(Inventory) {
  return function (req, res, next) {
    Inventory.findOne({'_id':req.params.itemId, 'store.storeId':req.params.storeId}, function (err, item) {
      if(!err && item) {
        item.amount = req.body.amount;
        item.description = req.body.description;
        item.imei = req.body.imei;
        item.fixed_cost = req.body.fixed_cost;
        item.store.locationId = req.body.store.locationId;
        item.save(function (err, item) {
          if(!err) {
            return res.send(200, item);
          } else {
            return next(err);
          }
        });
      } else {
        return next(err);
      }
    });
  };
};
