var _ = require('underscore'),
  async = require('async'),
  ObjectId = require('mongoose').Types.ObjectId;

exports.getMessages = function getMessages(Message) {
  return function (req, res, next) {
    Message.apiQuery(req.query, function (err, messages) {
      if(!err) {
        return res.send(200, messages);
      } else {
        return next(err);
      }
    });
  }
};

