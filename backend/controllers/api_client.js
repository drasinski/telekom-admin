var _ = require('underscore'),
  ObjectId = require('mongoose').Types.ObjectId,
  async = require('async');

exports.newClient = function newClient(Client) {
  return function (req, res, next) {
    console.log("new Client")
    var client = new Client({
      store: {
        storeId: new ObjectId(req.params.storeId),
        locationId: req.body.store.locationId
      },
      notes:req.body.notes,
      paymenttype: req.body.paymenttype,
      firstname:req.body.firstname,
      lastname: req.body.lastname,
      telephone: req.body.telephone,
      carrier:req.body.carrier,
      address: req.body.address,
      plan:req.body.plan,
      language:req.body.language,
      paiduntil: req.body.paiduntil,
      messages_sent: []
    });
    client.save(function (err, client) {
      if(!err) {
        res.locals.client = client;
        return next();
      } else {
        console.log(err);
        return next(err);
      }
    });
  }
}

exports.getClients = function getClients(Client) {
  return function (req, res, next) {
    Client.apiQuery(req.query, function (err, clients) {
      if(err) { 
        return next(err) 
      } else if(clients) {
        return res.send(200, clients);
      } else {
        return res.send(404,"Couldn't find any clients");
      }
    });
  }
}


exports.deleteClient = function deleteClient(Client) {
  return function (req, res, next){
    Client.findOne({'_id':req.params.clientId, 'store.storeId':req.params.storeId}, function (err, client) {
      if(client && !err) {
        res.locals.client = client;
        client.remove(function (err) {
          if (err) {
            return next(err);
          } else {
            return next();
          }
        });
      } else {
        return next(err);
      }
    });
  }
}

// PUT /clients/:clientId
exports.updateClient = function updateClient(Client) {
  return function (req,res, next) {
    Client.findOne({'_id':req.params.clientId, 'store.storeId':req.params.storeId}, function (err, client) {
      if(err) {
          return next(err);
      } else if(client) {
          client.firstname = req.body.firstname || client.firstname;
          client.lastname = req.body.lastname || client.lastname;
          client.telephone = req.body.telephone || client.telephone;
          client.carrier = req.body.carrier || client.carrier;
          client.plan = req.body.plan || client.plan;
          client.paiduntil = req.body.paiduntil || client.paiduntil;
          client.notes = req.body.notes || client.notes;
          client.language = req.body.language || client.language;
          client.address = req.body.address || client.address;
          client.receive_sms = _.isUndefined(req.body.receive_sms) ? client.receive_sms :  req.body.receive_sms;
          client.paymenttype = req.body.paymenttype || client.paymenttype;
          client.save(function (err) {
            if (!err) {
              res.locals.client = client;
              return next();
            } else {
              return next(err);
            }
          });
      } else {
          return res.send(404,"Couldn't find:  " + req.params.telephone);
      }
    });
  }
}

exports.getClientInvoicesCount = function getClientInvoicesCount(Invoice) {
  return function (req, res, next) {
    Invoice.count({'client.clientId':req.params.clientId}, function (err, count) {
      if(err) {
        return next(err);
      } else if(count) {
        return res.send({invoice_count:count});
      } else {
        return res.send(404,"Couldn't find:  " + req.params.clientId);
      }
    });
  }
}

// GET /clients/:clientId
exports.getClient = function getClient(Client) {
  return function (req,res, next) {
    Client.findOne({'_id':req.params.clientId, 'store.storeId':req.params.storeId}, function (err, client) {
      if(err) {
        return next(err);
      } else if(client) {
        return res.send(client);
      } else {
        return res.send(404,"Couldn't find:  " + req.params.clientId);
      }
    });
  }
};

exports.sendSMS = function sendSMS(Client, Store) {
  return function (req, res, next) {
    async.parallel([
      function (cb) {
        Client.findOne({'_id':req.params.clientId, 'store.storeId':req.params.storeId}, cb);
      },
      function (cb) {
        Store.findById(req.params.storeId, cb);
      }
    ], function (err, results) {
      if(!err) {
        req.body.from = results[1].twilio_number;
        results[0].sendSMS(req.body, function (err, response) {
          if(!err) {
            return res.send(200, response);
          } else {
            return res.send(404,"Couldn't send message.");
          }
        });
      } else {
        return next(err);
      }
    });
  }
};

exports.sendBulkSMS = function sendBulkSMS(Client, Store) {
  return function (req, res, next) {
    Store.findById(req.params.storeId, function (err, store) {
      if(err) {
        return next(err);
      } else if(!err && store) {
        req.body.from = store.twilio_number;
        Client.sendBulkSMS(req.body, function (err, response) {
          if(!err) {
            return res.send(200, response);
          } else {
            return next(err);
          }
        });
      } else {
        return res.send(404, "Couldn't find store:  " + req.params.storeId);
      }
    });
  }
};





