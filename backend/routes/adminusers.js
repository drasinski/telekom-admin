var express = require('express');
var routerAdminUsers = express.Router({mergeParams: true});
var passportConf = require('../config/passport');
var AdminUser = require('../models/adminusers');
var adminRouter = express.Router();
var api_adminuser = require('../controllers/api_adminuser');

routerAdminUsers.get('/', passportConf.ensureAdminUserAdminRole, api_adminuser.getUsers(AdminUser));
routerAdminUsers.post('/', passportConf.ensureAdminUserAdminRole, api_adminuser.newUser(AdminUser));
routerAdminUsers.delete('/:adminUserId', passportConf.ensureAdminUserAdminRole, api_adminuser.deleteUser(AdminUser));
routerAdminUsers.put('/:adminUserId', api_adminuser.updateUser(AdminUser));
adminRouter.post('/login', api_adminuser.login );
adminRouter.post('/logout', api_adminuser.logout );
adminRouter.get('/current-user', passportConf.ensureAdminUser, api_adminuser.currentUser );


module.exports = adminRouter;