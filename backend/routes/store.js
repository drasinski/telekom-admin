var express = require('express');
var _ = require('underscore');
var storeRouter = express.Router();
var Store = require('../models/store');
var passportConf = require('../config/passport');
var api_stores = require('../controllers/api_stores');
var api_user = require('../controllers/api_user');
var routerInvoices = require('./store_routes/invoices'),
    routerUsers = require('./store_routes/users'),
    routerClients = require('./store_routes/clients'),
    routerInventory = require('./store_routes/inventory'),
    routerIncome = require('./store_routes/income'),
    routerTypes = require('./store_routes/type'),
    routerMessages = require('./store_routes/message'),
    routerNotes = require('./store_routes/note');

module.exports = function (io) {

  /* Middleware use for mongoose-api-query on routes suchas :storeId/invoices or :storeId/clients */
  storeRouter.get('/', passportConf.ensureAdminUser, api_stores.getStores(Store));
  storeRouter.post('/', passportConf.ensureAdminUser, api_stores.newStore(Store));
  storeRouter.use('/:storeId', function (req, res, next) {
    console.log("On store:  " + req.params.storeId);
    if(!_.isUndefined(req.body.store)) {
      req.body.store.storeId = req.params.storeId; // handles all post requests
    }
    if(!_.isUndefined(req.query) && !_.isUndefined(req.params.storeId)) {
      req.query['store.storeId'] = req.params.storeId; // handles all get requests
    } else if(_.isUndefined(req.params.storeId)) {
      return res.send(404);
    }
    next();
  });
  storeRouter.get('/:storeId', api_stores.getStore(Store));
  storeRouter.post('/:storeId/login', api_user.login);
  storeRouter.post('/:storeId/logout', api_user.logout);
  storeRouter.use('/:storeId/*', passportConf.ensureAuthenticated);
  // ALL ROUTES NEED AUTHENTICATION FROM HERE
  storeRouter.put('/:storeId/', api_stores.updateStore(Store));
  storeRouter.get('/:storeId/current-user', api_user.currentUser);
  storeRouter.use('/:storeId/invoices', routerInvoices(io));
  storeRouter.use('/:storeId/users', routerUsers);
  storeRouter.use('/:storeId/clients', routerClients(io));
  storeRouter.use('/:storeId/inventory', routerInventory);
  storeRouter.use('/:storeId/incomes', routerIncome);
  storeRouter.use('/:storeId/types', routerTypes);
  storeRouter.use('/:storeId/messages', routerMessages);
  storeRouter.use('/:storeId/notes', routerNotes);

  return storeRouter;
}
