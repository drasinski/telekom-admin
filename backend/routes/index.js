var express = require('express');
var mainRouter = express.Router();
var path = require('path');
var secrets = require('../config/secrets');
var storeRouter = require('./store'),
    adminRouter = require('./adminusers');

var folder = (process.env.NODE_ENV === 'production') ? secrets.server.publicStore.binFolder : secrets.server.publicStore.buildFolder; 


module.exports = function (io) {

  mainRouter.use('/stores', storeRouter(io) );
  mainRouter.use('/admin', adminRouter );
  mainRouter.post('/epay-callback', function (req, res, next) {
    res.locals.csrf = encodeURIComponent(req.csrfToken());
    res.sendFile('epay-callback.html', { root: path.resolve(folder, 'assets') });
  });

  return mainRouter;

}
