var express = require('express');
var api_user = require('../../controllers/api_user');
var Client = require('../../models/clients');
var passportConf = require('../../config/passport');
var routerClients = express.Router({mergeParams: true});
var User = require('../../models/users');

/* used to store profile pictures */
var AWS = require('aws-sdk');
var secrets = require('../../config/secrets');
AWS.config.region = secrets.aws.region;
AWS.config.accessKeyId = secrets.aws.accessKeyId;
AWS.config.secretAccessKey = secrets.aws.secretAccessKey;

var routerUsers = express.Router({mergeParams: true});
routerUsers.delete('/:userId', passportConf.adminRequired, api_user.deleteUser(User, AWS));
routerUsers.put('/:userId', api_user.updateUser(User));
routerUsers.post('/:userId/upload',api_user.uploadImage(User, AWS));
routerUsers.get('/:username', api_user.getUser(User));
routerUsers.get('/', passportConf.adminRequired, api_user.getUsers(User));
routerUsers.post('/', passportConf.adminRequired, api_user.newUser(User));

module.exports = routerUsers;
