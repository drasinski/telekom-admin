var express = require('express');
var routerInventory = express.Router({mergeParams: true});
var api_inventory = require('../../controllers/api_inventory');
var Inventory = require('../../models/inventory');
var passportConf = require('../../config/passport');

routerInventory.post('/', api_inventory.newItem(Inventory));
routerInventory.delete('/:itemId', passportConf.adminRequired, api_inventory.deleteItem(Inventory));
routerInventory.get('/', api_inventory.getItems(Inventory));
routerInventory.put('/:itemId', passportConf.adminRequired, api_inventory.updateItem(Inventory));
routerInventory.get('/:itemId', api_inventory.getItem(Inventory));

module.exports = routerInventory;