var express = require('express');
var routerIncome = express.Router({mergeParams: true});
var passportConf = require('../../config/passport');
var Income = require('../../models/income');
var api_income = require('../../controllers/api_income');


routerIncome.get('/', passportConf.adminRequired, api_income.getIncomes(Income));
routerIncome.get('/:incomeId', passportConf.adminRequired, api_income.getIncome(Income));
routerIncome.put('/:incomeId', passportConf.adminRequired, api_income.updateIncome(Income));
routerIncome.post('/', passportConf.adminRequired, api_income.newIncome(Income));


module.exports = routerIncome;