var express = require('express');
var Message = require('../../models/message');
var api_message = require('../../controllers/api_messages');
var routerMessages = express.Router({mergeParams: true});


routerMessages.get('/', api_message.getMessages(Message));


module.exports = routerMessages;