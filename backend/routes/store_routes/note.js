var express = require('express');
var Note = require('../../models/note');
var api_note = require('../../controllers/api_notes');
var routerNotes = express.Router({mergeParams: true});


routerNotes.get('/', api_note.getNotes(Note));
routerNotes.post('/', api_note.newNote(Note));
routerNotes.put('/:noteId', api_note.updateNote(Note));
routerNotes.delete('/:noteId', api_note.deleteNote(Note));

module.exports = routerNotes;