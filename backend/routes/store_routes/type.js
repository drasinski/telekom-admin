var express = require('express');
var Type = require('../../models/type');
var passportConf = require('../../config/passport');
var api_type = require('../../controllers/api_types');
var routerTypes = express.Router({mergeParams: true});


routerTypes.get('/', api_type.getTypes(Type));
routerTypes.post('/', passportConf.adminRequired, api_type.newType(Type));
routerTypes.delete('/:typeId', passportConf.adminRequired, api_type.deleteType(Type));
routerTypes.put('/:typeId', passportConf.adminRequired, api_type.updateType(Type));


module.exports = routerTypes;