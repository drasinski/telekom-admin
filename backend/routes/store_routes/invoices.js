var express = require('express');
var routerInvoices = express.Router({mergeParams: true});
var api_invoice = require('../../controllers/api_invoices');
var Invoice = require('../../models/invoice');
var Inventory = require('../../models/inventory');
var Client = require('../../models/clients');
var passportConf = require('../../config/passport');
var ObjectId = require('mongoose').Types.ObjectId;
var userHelper = require('../../lib/user-helpers');

module.exports = function (io) {

  routerInvoices.post('/', api_invoice.newInvoice(Invoice), function (req, res, next) {
    io.sockets.connected[req.get('Socket-Id')].broadcast.to(req.params.storeId+'-admin').emit('new:invoice', {user: userHelper.filterUser(req.user), invoice: res.locals.invoice});
    res.send(201, res.locals.invoice);
  });
  routerInvoices.get('/', api_invoice.getInvoices(Invoice));
  routerInvoices.delete('/:invoiceId', passportConf.adminRequired, api_invoice.deleteInvoice(Invoice), function (req, res, next) {
    io.sockets.connected[req.get('Socket-Id')].broadcast.to(req.params.storeId+'-admin').emit('delete:invoice', {user: userHelper.filterUser(req.user), invoice: res.locals.invoice});
    res.send(200, req.params.invoiceId);
  });
  routerInvoices.put('/:invoiceId', api_invoice.updateInvoice(Invoice, Inventory, Client));
  routerInvoices.get('/:invoiceId', api_invoice.getInvoice(Invoice));

  return routerInvoices;

}
