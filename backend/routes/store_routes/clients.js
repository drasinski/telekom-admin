var express = require('express');
var api_client = require('../../controllers/api_client');
var Client = require('../../models/clients');
var Invoice = require('../../models/invoice');
var Store = require('../../models/store');
var passportConf = require('../../config/passport');
var routerClients = express.Router({mergeParams: true});
var userHelpers = require('../../lib/user-helpers');

module.exports = function(io) {

  routerClients.post('/', api_client.newClient(Client), function (req, res, next) {
    io.sockets.connected[req.get('Socket-Id')].broadcast.to(req.params.storeId).emit('new:client', {user: userHelpers.filterUser(req.user), client: res.locals.client});
    return res.send(201, res.locals.client);
  });
  routerClients.get('/', api_client.getClients(Client));
  routerClients.post('/sms', passportConf.adminRequired, api_client.sendBulkSMS(Client, Store));
  routerClients.delete('/:clientId', passportConf.adminRequired, api_client.deleteClient(Client), function (req, res, next) {
    io.sockets.connected[req.get('Socket-Id')].broadcast.to(req.params.storeId).emit('delete:client', {user: userHelpers.filterUser(req.user), client: res.locals.client});
    return res.send(200, req.params.clientId);
  });
  routerClients.put('/:clientId', api_client.updateClient(Client), function (req, res, next) {
    io.sockets.connected[req.get('Socket-Id')].broadcast.to(req.params.storeId).emit('update:client', {client: res.locals.client});
    return res.send(200, res.locals.client)
  });
  routerClients.get('/:clientId/invoices/count', api_client.getClientInvoicesCount(Invoice));
  routerClients.get('/:clientId', api_client.getClient(Client));
  routerClients.post('/:clientId/sms', api_client.sendSMS(Client, Store));
  
  return routerClients;
}
