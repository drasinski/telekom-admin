var mongoose = require('mongoose'),
    mongooseApiQuery = require('mongoose-api-query'),
    userFunctions = require('../lib/user-helpers');


//******* Database schema TODO add more validation
var Schema = mongoose.Schema;

// User schema
var adminUserSchema = new Schema({
  firstname: { 
    type: String,
    required: true
  },
  lastname: { 
    type: String,
    required: true
  },
  username: { 
    type: String,
    required: true,
    unique: true,
    validate: [userFunctions.validateUniqueUsername, 'Username is already in-use']
  },
  email: { 
    type: String,
    required: true
  },
  password: { 
    type: String,
    required: true,
    validate: [userFunctions.validatePresenceOf, 'Password cannot be blank']
  },
  admin_user: {
    type: Boolean,
    required: true,
    default:true
  },
  admin: { 
    type: Boolean,
    required: true,
    default:false
  }
});

adminUserSchema.virtual('name').get(function() {
  return this.firstname + ' ' + this.lastname;
});

adminUserSchema.plugin(mongooseApiQuery);

/* password middleware */
adminUserSchema.pre('save', userFunctions.hashPassword );

// Password verification
adminUserSchema.methods.comparePassword = userFunctions.comparePassword;


module.exports = mongoose.model( 'AdminUser', adminUserSchema );
