var mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema,
    _ = require('underscore'),
    mongooseApiQuery = require('mongoose-api-query'),
    secrets = require('../config/secrets'),
    twilioclient = require('twilio')(secrets.twilio.sid, secrets.twilio.token),
    async = require('async');

var payment_types = ['none','creditcard','prepaid','mail']

var clientSchema = new mongoose.Schema({
    store: {
      storeId:{type: Schema.ObjectId, ref: 'Store', required: true},
      locationId: {type:String, trim: true}
    },
    language: {type: String, default: 'english', trim:true},
    firstname: {type: String, trim:true},
    lastname: {type: String, trim:true},
    notes: {type: String, trim:true},
    telephone: {type: String, required: true, match: /^\d{10}$/},
    carrier: {type: String, trim:true},
    plan: {type: String, trim:true},
    address: {
      formatted_address: {type: String, trim:true},
      location: {
        type: { type: String, default: "Point" },
        coordinates: [{type: Number}]
      }
    },
    paiduntil: {type: Date},
    paymenttype: {type:String, enum:payment_types, default:payment_types[0] },
    receive_sms: {type: Boolean, default: true},
    messages_sent: [{
      message_type: {type: String},
      date_sent: {type:Date, default: new Date()}, // when was the message sent?
      for_date: {type:Date, default: new Date()} // message was sent for which date?
    }]
});

clientSchema.plugin(mongooseApiQuery);

clientSchema.methods.sendSMS = function sendSMS(body, cb) {
  var self = this;
  twilioclient.sendSms({
    body: body.message,
    to: self.telephone,
    from: body.from
  }, function (err, resp) {
    if(!err) {
      console.log(resp);
      self.messages_sent.push({for_date:body.for_date, message_type: body.message_type});
      self.save(cb);
    } else {
      cb(err, resp);
    }
  });
};

clientSchema.statics.sendBulkSMS = function sendBulkSMS(body, cb) {
  var tasks = _.map(body.to, function (telephone) {
    return function (callback) {
      twilioclient.sendSms({
        body: body.message,
        to: telephone,
        from: body.from
      }, function (err, resp) {
        if(!err) {
          callback(null, {sent:true, telephone: telephone});
        } else {
          callback(null, {sent:false, telephone: telephone});
        }
      });
    }
  });

  async.parallelLimit(tasks, 500, function (err, results) {
    var failure = _.chain(results).filter(function (res) {
      return !res.sent;
    }).pluck('telephone').value();
    var response = {
      sent_messages: (results.length-failure.length),
      failed_messages: failure.length,
      failed_numbers: failure
    }
    cb(null, response);
  });

};

// Update all of the clients invoices with the approriate address/firstname/lastname on save
// Shared data among invoice/client schema
clientSchema.pre('save', true, function (next, done) {
  var self = this;
  var address = {
    formatted_address: self.address.formatted_address || '',
    location: {
      coordinates: self.address.location.coordinates || []
    }
  };
  mongoose.model('Invoice').update({'client.clientId':self._id, 'store.storeId':self.store.storeId},{$set:{'client.address':address,'client.firstname':(self.firstname||''),'client.lastname':(self.lastname||''), 'client.language':(self.language||'english')}},{multi:true},done);
  next();
});

/* when removing a client take all those client's invoices and remove all information that links to the client in our database */
/* for now lets leave the invoice information relating to the client */
clientSchema.pre('remove', true, function (next, done) {
  var self = this;
  mongoose.model('Invoice').update({'client.clientId':self._id, 'store.storeId':self.store.storeId},{$unset:{'client.clientId':''}},{multi:true},done);
  next();
});

module.exports = mongoose.model( 'Client', clientSchema );


