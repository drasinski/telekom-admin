var mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema,
    FormatDate = mongoose.Schema.Types.FormatDate = require('mongoose-schema-formatdate'),
    mongooseApiQuery = require('mongoose-api-query');

var incomeSchema = new mongoose.Schema({
  date: {type: FormatDate, format:'MMM-YYYY', required:true},
  store: {
    storeId:{type: Schema.ObjectId, ref: 'Store', required: true}
  },
  expenses: [{
    locationId: {type:String, required:true, trim:true},
    description: {type:String, required:true, trim: true},
    amount: {type: Number,set:setPrice, required: true}
  }],
  other_income: [{
    locationId: {type:String, required:true, trim:true},
    description: {type:String, required:true, trim: true},
    amount: {type: Number,set:setPrice, required: true}
  }],
  income: [{
    locationId: {type:String, required:true, trim:true},
    description: {type:String, required:true, trim: true},
    amount: {type: Number,set:setPrice, required: true}
  }],
  summary: [{
    locationId: {type:String, required:true, trim:true},
    expenses: {type: Number,set:setPrice, required: true},
    net: {type: Number,set:setPrice, required: true},
    income: {type: Number,set:setPrice, required: true}
  }]
});

function setPrice(num) {
  return Math.ceil(num * 100) / 100;
};

incomeSchema.plugin(mongooseApiQuery);


module.exports = mongoose.model( 'Income', incomeSchema );