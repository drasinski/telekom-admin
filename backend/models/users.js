var mongoose = require('mongoose'),
    mongooseApiQuery = require('mongoose-api-query'),
    userFunctions = require('../lib/user-helpers'),
    _ = require('underscore');


//******* Database schema TODO add more validation
var Schema = mongoose.Schema;

// var validateUniqueUsername = function(value, callback) {
//   var User = mongoose.model('User');
//   User.find({
//     $and: [{
//       username: value
//     }, {
//       _id: {
//         $ne: this._id
//       }
//     }]
//   }, function (err, user) {
//     callback(err || user.length === 0);
//   });
// };


// User schema
var userSchema = new Schema({
  firstname: { 
    type: String,
    required: true
  },
  lastname: { 
    type: String,
    required: true
  },
  username: { 
    type: String,
    required: true,
    unique: true
  },
  email: { 
    type: String
  },
  password: { 
    type: String,
    required: true,
    validate: [userFunctions.validatePresenceOf, 'Password cannot be blank']
  },
  admin: { 
    type: Boolean,
    required: true,
    default:false
  },
  thumbnail: { 
    type:String
  },
  store: {
    locationId: { type:String, required:false },
    storeId: {type: Schema.ObjectId, ref: 'Store', required: true}
  }
});

userSchema.virtual('name').get(function() {
  return this.firstname + ' ' + this.lastname;
});

userSchema.plugin(mongooseApiQuery);

/* password middleware */
userSchema.pre('save', userFunctions.hashPassword );

// Password verification
userSchema.methods.comparePassword = userFunctions.comparePassword;

module.exports = mongoose.model( 'User', userSchema );
