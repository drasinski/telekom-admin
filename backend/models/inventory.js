var mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema,
    async = require('async'),
    _ = require('underscore'),
    mongooseApiQuery = require('mongoose-api-query'),
    generateSlug = require('mongoose-slugs');

var inventorySchema = new mongoose.Schema({
  store: {
    storeId:{
      type: Schema.ObjectId,
      ref: 'Store',
      required: true
    },
    locationId: {
      type:String, 
      required:true, 
      trim: true, 
      match: /^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/
    }
  },
  invoice: {
    invoiceId: {
      type: Schema.ObjectId,
      ref: 'Invoice', 
      'default':null
    }
  },
  category: {
    type: String, 
    required:true, 
    'default': 'Phone Sale'
  },
  category_slug: {
    type:String,
    trim: true,
    'default': 'phone-sale'
  },
  sold: {
    type: Boolean,
    required: true,
    'default':false
  },
  // profit: {
  fixed_cost: {
    type:Number,
    set:setPrice,
    min:0
  },
  // },
  description: {
    type:String,
    required:true,
    trim:true
  },
  imei: {
    type:String,
    required:true,
    trim:true
  },
  amount: {
    type:Number,
    set:setPrice,
    min:0
  }
});

inventorySchema.pre('validate', generateSlug('Inventory', 'category', 'category_slug', {allowDuplication:true}));


inventorySchema.plugin(mongooseApiQuery);

function setPrice(num) {
  return Math.ceil(num * 100) / 100;
};

// imeis : [] (array of imeis)
inventorySchema.statics.returnPhonesToInventory = function returnPhonesToInventory(inventoryIds, storeId, cb) {
  this.update({_id:{$in:inventoryIds}, 'store.storeId':storeId},{$set:{'invoice.invoiceId':null,'sold':false}},{multi:true},cb);
}

inventorySchema.statics.addPhonesToSoldInventory = function addPhonesToSoldInventory(inventoryIds, invoiceId, storeId, cb) {
  this.find({_id:{$in:inventoryIds}, 'store.storeId':storeId}, function (err, phones) {
    if(!err && phones) {
      var tasks = _.map(phones, function (phone, key) {
        return function (callback) {
          phone.invoice.invoiceId = invoiceId;
          phone.sold = true;
          phone.save(callback);
        }
      });
      async.parallel(tasks,cb);
    } else if (err) {
      cb(err);
    } else {
      cb();
    }
  });
}

module.exports = mongoose.model( 'Inventory', inventorySchema );