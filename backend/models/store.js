var mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema,
    generateSlug = require('mongoose-slugs'),
    mongooseApiQuery = require('mongoose-api-query');

var validateUniqueStorename = function(value, callback) {
  var Store = mongoose.model('Store');
  Store.find({ slug: value }, function (err, slug) {
    callback(err || slug.length === 0);
  });
};


var storeSchema = new mongoose.Schema({
  service_agreement: {
    type:String,
    trim:true
  },
  tax_fee: {
    type: Number,
    required: true
  },
  active: {
    type: Boolean,
    default: true
  },
  slug: {
    type:String,
    required:true,
    unique: true,
    trim: true,
    match: /^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/
  },
  website: {
    type:String,
    required:false,
    trim: true
  },
  name: {
    type:String,
    required:true,
    trim: true
  },
  twilio_number: {
    type: String,
    required: true,
    match: /^\d{10}$/
  },
  contact: {
    firstname: {
      type:String,
      required:true,
      trim: true
    },
    lastname: {
      type:String,
      required:true,
      trim: true
    },
    phone_number: {
      type: String,
      required: true,
      match: /^\d{10}$/
    },
    email: {
      type: String
    }
  },
  locations: [{
    name: {
      type:String,
      required:true,
      trim: true
    },
    id: {
      type:String,
      trim: true,
      match: /^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/,
      required:true
    },
    phone_number: {
      type:String,
      required:true,
      trim: true,
      match: /^\d{10}$/
    },
    address: {
      formatted_address: {
        type:String, trim:true
      },
      location: {
          type: { 
            type: String,
            default: "Point"
          },
          coordinates: [{
            type: Number
          }]
      }
    }
  }]
});

storeSchema.plugin(mongooseApiQuery);


// storeSchema.pre('validate', generateSlug('Store', 'name', 'slug'));

module.exports = mongoose.model( 'Store', storeSchema );