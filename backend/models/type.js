var mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema,
    generateSlug = require('mongoose-slugs'),
    mongooseApiQuery = require('mongoose-api-query'),
    ObjectId = require('mongoose').Types.ObjectId;

var typeSchema = new mongoose.Schema({
  category: {
    type: String,
    required: true,
    trim:true
  },
  category_slug: {
    type:String,
    required:true,
    trim: true,
    match: /^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/
  },
  active: {
    type: Boolean,
    required: true,
    'default':true
  },
  description: {
    type: String,
    required: true,
    trim: true
  },
  icon: {
    type: String
  },
  system: {
    type: String
  },
  system_slug: {
    type:String,
    required:true,
    trim: true,
    match: /^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/
  },
  profit: [{
    start_date: {type: Date},
    end_date: {type: Date},
    amount_comm: {type: Number, default:100, min:0},
    amount_fixed: {type: Number, default:0},
    has_amount: {type: Boolean, default:true},
    servicecharge_comm: {type: Number, default:100, min:0},
    servicecharge_fixed: {type: Number, default:0},
    has_servicecharge: {type: Boolean, default:false},
    tax_comm: {type: Number, default:100, min:0},
    tax_fixed: {type: Number, default:0},
    has_tax: {type: Boolean, default:false},
    fixed_cost: {type: Number, default:0}
  }],
  prediction:[{
    amount: {type: Number, default:0},
    servicecharge: {type: Number, default:0},
    tax: {type: Boolean, required: true, default:false},
    default_pred: {type: Boolean, required: true, default:false}
  }],
  store: {
    storeId: {type: Schema.ObjectId, ref: 'Store', required: true}
    // locationId: {type:String, trim: true}
  }
});

typeSchema.pre('validate', generateSlug('Type', 'category', 'category_slug', {allowDuplication:true}));
typeSchema.pre('validate', generateSlug('Type', 'system', 'system_slug', {allowDuplication:true}));


typeSchema.plugin(mongooseApiQuery);

module.exports = mongoose.model( 'Type', typeSchema );
