var mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema,
    _ = require('underscore'),
    mongooseApiQuery = require('mongoose-api-query'),
    async = require('async');

var messageSchema = new mongoose.Schema({
  store: {
    storeId:{
      type: Schema.ObjectId,
      ref: 'Store',
      required: true
    }
  },
  content: {type: String, required: true, trim: true},
  user: {
    firstname: {type: String},
    lastname: {type: String},
    thumbnail: {type: String},
    userId: {type: Schema.ObjectId, ref:'User', required:true }
  },
  type:{ type: String, default:'message'}
});

messageSchema.plugin(mongooseApiQuery);

module.exports = mongoose.model( 'Message', messageSchema );


