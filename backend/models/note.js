var mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema,
    mongooseApiQuery = require('mongoose-api-query'),
    FormatDate = mongoose.Schema.Types.FormatDate = require('mongoose-schema-formatdate');

var noteSchema = new mongoose.Schema({
    store: {
      storeId:{type: Schema.ObjectId, ref: 'Store', required: true},
      locationId: {type:String, trim: true}
    },
    content: {type: String, trim:true},
    date: {type:Date, required: true}
    // date: {type: FormatDate, format:'YYYY-MM-DD', required:true},
});

noteSchema.plugin(mongooseApiQuery);

module.exports = mongoose.model( 'Note', noteSchema );


