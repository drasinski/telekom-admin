var mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema,
    ObjectId = require('mongoose').Types.ObjectId;

var _ = require('underscore');
var mongooseApiQuery = require('mongoose-api-query');

/* GOAL */
var invoiceSchema = new mongoose.Schema({
  subtotal: {
    type: Number,
    required: true
  },
  taxtotal: {
    type: Number,
    'default':0,
    min:0
  },
  grandtotal: {
    type: Number,
    min:0,
    required: true
  },
  cash_total: {
    type: Number,
    min:0,
    'default':0,
    required:true
  },
  credit_total:{
    type: Number, 
    min:0, 
    'default':0, 
    required:true
  },
  check_total:{
    type: Number,
    min:0,
    'default':0,
    required:true
  },
  invoice_number: {
    type: String,
    required: true
  },
  servicecharge: {
    type: Number,
    'default':0,
    min:0
  },
  saletotal: {
    type: Number,
    'default':0,
    min:0
  },
  user: {
    username: {type:String, required:true },
    userId: {type: Schema.ObjectId, ref: 'User' }
  },
  client: {
    clientId: {
      type: Schema.ObjectId,
      ref: 'Client'
    },
    firstname: {
      type:String,
      trim:true
    },
    lastname: {
      type:String,
      trim:true
    },
    telephone: {
      type:String,
      match: /^\d{10}$/
    },
    language: {
      type: String,
      trim:true
    },
    address: {
      formatted_address: {type:String, trim:true},
      location: {
        type: { type: String, default: "Point" },
        coordinates: [{type: Number}]
      }
    }
  },
  store: {
    storeId: {
      type: Schema.ObjectId,
      ref: 'Store',
      required: true
    },
    locationId: {
      type:String,
      required:true,
      trim: true
    }
  },
  transactions: [{
    total: {type:Number, required: true, min:0},
    amount: {type:Number, required: true, min:0},
    amtsale: {type:Number, default:0, min:0},
    servicecharge: {type:Number, default:0, min:0},
    taxtotal: {type:Number, default:0, min:0},
    otherId: {
      category: {type: String},
      category_slug: {type:String, trim: true},
      fixed_cost: {type:Number},
      description: {type: String, trim:true}
    },
    typeId: {type: Schema.ObjectId, ref: 'Type'},
    inventoryId: {type: Schema.ObjectId, ref: 'Inventory'}
  }]
});

invoiceSchema.plugin(mongooseApiQuery);

invoiceSchema.methods.getPhoneTransactions = function getPhoneTransactions() {
  return _.chain(this.transactions).filter(function (val) {
    return !_.isUndefined(val.inventoryId);
  }).map(function (val) {
    return val.inventoryId;
  }).value();
}

invoiceSchema.pre('remove', true, function (next, done) {
  var self = this;
  mongoose.model('Inventory').returnPhonesToInventory(self.getPhoneTransactions(), self.store.storeId, done);
  next();
});

/* Udpate Inventory middleware */
invoiceSchema.pre('save', true, function (next,done) {
  var self = this;
  mongoose.model('Inventory').addPhonesToSoldInventory(self.getPhoneTransactions(), self._id, self.store.storeId, done);
  next();
});

module.exports = mongoose.model( 'Invoice', invoiceSchema );


