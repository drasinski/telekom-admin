var express = require('express'), 
    mongoose = require('mongoose'),
    passport = require('passport'),
    mongodb = require('mongodb'),
    secrets = require('./backend/config/secrets'),
    protectJSON = require('./backend/lib/protectJSON'),
    fs = require('fs'),
    http = require('http'),
    https = require('https'),
    privateKey  = fs.readFileSync(secrets.server.privateKey).toString(),
    certificate = fs.readFileSync(secrets.server.certificate).toString(),
    credentials = {key: privateKey, cert: certificate},
    timestamps = require('mongoose-timestamp');

mongoose.plugin(timestamps,  {
  createdAt: 'created_date', 
  updatedAt: 'last_modified_date'
});
require('mongoose-currency').loadType(mongoose);


var bodyParser = require('body-parser'),
    compress = require('compression'),
    cookieParser = require('cookie-parser'),
    logger = require('morgan'),
    serveStatic = require('serve-static'),
    csrf = require('csurf'),
    methodOverride = require('method-override'),
    errorHandler = require('errorhandler'),
    session = require('express-session');
    // multer = require('multer');


var app = express();

var csrfValue = function(req) {
  var token = (req.body && req.body._csrf)
    || (req.query && req.query._csrf)
    || (req.headers['x-csrf-token'])
    || (req.headers['x-xsrf-token']);
  return token;
};

var folderStore;
var folderAdmin;
var db_url;
if(process.env.NODE_ENV === 'development') {
  folderStore = secrets.server.publicStore.buildFolder;
  folderAdmin = secrets.server.publicAdmin.buildFolder;
  db_url = secrets.db.local;
} else if(process.env.NODE_ENV === 'production') {
  folderStore = secrets.server.publicStore.binFolder;
  folderAdmin = secrets.server.publicAdmin.binFolder;
  db_url = secrets.db.remote;
}

mongoose.connect(db_url,{server: {auto_reconnect: true}});
var MongooseStore = require('express-mongoose-store')(session, mongoose);
var mongooseStore = new MongooseStore({ttl: 43200000});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Connected to DB');
});


app.use('/public_admin', serveStatic(folderAdmin));
app.use('/public_store', serveStatic(folderStore));
app.use(protectJSON);
app.use(bodyParser.json());
app.use(methodOverride());
app.use(logger('combined'));
app.use(compress());
app.use(cookieParser());  // 3600000*2 Hash cookies with this secret
app.use(session({
  secret: secrets.server.cookieSecret,
  key: secrets.server.cookieKey,
  resave: false,
  saveUninitialized: false,
  cookie: { expires:false },
  store: mongooseStore
}));
app.use(csrf({value: csrfValue}));
app.use(function (req, res, next) {
  res.cookie('XSRF-TOKEN', req.csrfToken());
  next();
});
// Initialize Passport!  Also use passport.session() middleware, to support
// persistent login sessions (recommended).
app.use(passport.initialize());
app.use(passport.session());

//error middleware
if(process.env.NODE_ENV == 'development') {
  app.use(errorHandler({ dumpExceptions: true, showStack: true }));
}
app.use(function (err, req, res, next) {
  console.error(err.stack);
  return res.send(500, { message: err.message });
});

var server = http.createServer(app);

//initialize socket.io
var io = require('./io')(require("socket.io").listen(server), mongooseStore);

app.use('/api/v1', require('./backend/routes/index')(io));

app.use('/admin', function (req, res, next) {
  res.sendFile('index.html', { root:folderAdmin });
});

app.all(['/*/*'], function (req,res,next) {
  res.sendFile('index.html', { root: folderStore });
});

// app.listen(secrets.server.listenPort, function() {
//   console.log("Listening on " + secrets.server.listenPort);
// });


server.listen(secrets.server.listenPort, function() {
  console.log("Listening on " + secrets.server.listenPort);
});



// http.createServer(app).listen(secrets.server.listenPort, function() {
//   console.log("Listening on " + secrets.server.listenPort);
// });

// https.createServer(credentials, app).listen(secrets.server.securePort, function() {
//   console.log("Listening on " + secrets.server.securePort);
// });


