'use strict';

var pkg = require('./package.json');

module.exports = {
    assetFolder: 'assetFiles/',
    source: {
        folder: 'public_admin',
        index: 'src/index.html',
        files: {
            app: {
                js: [
                    'src/**/*.js'
                ],
                jade: [
                    'src/app/**/*.jade',
                    'src/common/**/*.jade'
                ],
                less: [
                    'src/less/main.less'
                ],
                assets: [
                    'src/assets/**/*.*'
                ]
            },
            vendor: {
                js: [
                  'vendor/angular/angular.js',
                  'vendor/jquery/jquery.js',
                  'vendor/angular-animate/angular-animate.js',
                  'vendor/angular-bootstrap/ui-bootstrap-tpls.js',
                  'vendor/angular-ui-router/release/angular-ui-router.js',
                  'vendor/angular-ui-utils/ui-utils.js',
                  'vendor/fastclick/lib/fastclick.js',
                  'vendor/lodash/dist/lodash.js',
                  'vendor/moment/moment.js',
                  'vendor/angular-touch/angular-touch.js',
                  'vendor/snapjs/snap.js',
                  'vendor/angular-notify/dist/angular-notify.js',
                  'vendor/angular-promise-tracker/promise-tracker.js',
                  'vendor/angular-snap/angular-snap.js'
                ],
                css: [
                  'vendor/angular-snap/angular-snap-only.css',
                ],
                assets: [
                ]
            }
        }
    },
    targets: {
        buildFolder: 'public_admin/build/',
        distFolder: 'public_admin/bin/',
        tempFolder: '.temp/',
        cordovaFolder: 'cordova/',
        assetsFolder: 'src/assets',
        nwjsFolder: 'nwjs/',
        resourcesFolder: 'resources/',
        minified: {
            js: pkg.name+'-'+pkg.version+'-public_admin.js',
            css: pkg.name+'-'+pkg.version+'-public_admin.css',
            templateCache: 'templates.js'
        }
    },
    angularModuleName: 'templates'
};
