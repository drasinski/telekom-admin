var users = require('../users');
var _ = require('underscore');
var fs = require('fs');
// var file = require('./newclients');
var ObjectId = require('mongoose').Types.ObjectId;
var path = require('path');
var file = path.resolve(__dirname, '../users.json');


_.map(users, function (user) {
  user.store = {
        "storeIds": [
            {
                "$oid": "542adf68e4b0e67da1edd6b7"
            }
        ],
        "locationId": user.location
    };
  return user;
})

fs.writeFile(file, JSON.stringify(users, null, 4), function (err) {
  if(err) {
    console.log("Error writing new users");
  } else {
    console.log("Write users")
  }
})