var clients = require('../clients');
var _ = require('underscore');
var fs = require('fs');
// var file = require('./newclients');
var ObjectId = require('mongoose').Types.ObjectId;
var path = require('path');
var moment = require('moment');
var file = path.resolve(__dirname, '../newclients.json');

clients = _.map(clients, function (client) {
  delete client.sms_history;
  delete client.invoices;
  client.messages_sent = [];
  client.telephone = String(client.telephone);
  client.store = {
    storeId: {
      $oid:new ObjectId("542adf68e4b0e67da1edd6b7")
    },
    locationId: 'telekom1'
  }
  client.created_date = {
    $date: new Date(ObjectId(client._id.$oid).getTimestamp()).getTime()
  }
  client.last_modified_date = {
    $date: new Date(ObjectId(client._id.$oid).getTimestamp()).getTime()
  }
  client.language = client.language || 'polish';
  // client.created_date = {
  //   $date:new Date(ObjectId(client._id.$oid).getTimestamp())
  // }
  // client.last_modified_date = {
  //   $date:new Date(ObjectId(client._id.$oid).getTimestamp())
  // };
  return client;
});


fs.writeFile(file, JSON.stringify(clients, null, 4), function (err) {
  if(err) {
    console.log("Error writing new clients");
  } else {
    console.log("Write clients")
  }
})