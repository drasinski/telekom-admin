var inventories = require('../inventories');
var _ = require('underscore');
var fs = require('fs');
var moment = require("moment");
// var file = require('./newclients');
var ObjectId = require('mongoose').Types.ObjectId;
var path = require('path');
var file = path.resolve(__dirname, '../newinventories.json');

inventories = _.map(inventories, function (item) {
  return {
    created_date: {
      $date: new Date(ObjectId(item._id.$oid).getTimestamp()).getTime()
    },
    last_modified_date: {
      $date: new Date(ObjectId(item._id.$oid).getTimestamp()).getTime()
    },
    _id: item._id,
    __v: item.__v,
    store: {
      storeId: {
        $oid:new ObjectId("542adf68e4b0e67da1edd6b7")
      },
      locationId: item.location
    },
    invoice: {
      invoiceId: item.invoice
    },
    category: 'Phone Sale',
    category_slug: 'phone-sale',
    sold: item.sold,
    fixed_cost: item.cost,
    description: item.name,
    imei: item.imei,
    amount: item.price
  };
});

fs.writeFile(file, JSON.stringify(inventories, null, 4), function (err) {
  if(err) {
    console.log("Error writing new inventory");
  } else {
    console.log("Write inventory")
  }
})