var incomes = require('../incomes');
var _ = require('underscore');
var fs = require('fs');
var moment = require('moment');
// var file = require('./newclients');
var ObjectId = require('mongoose').Types.ObjectId;
var path = require('path');
var file = path.resolve(__dirname, '../newincomes.json');

incomes = _.map(incomes, function (income) {
  return {
    date: {
      $date: new Date(moment(income.label, 'MMM-YYYY')).getTime()
    },
    created_date: {
      $date: new Date(ObjectId(income._id.$oid).getTimestamp()).getTime()
    },
    last_modified_date: {
      $date: new Date(ObjectId(income._id.$oid).getTimestamp()).getTime()
    },
    expenses: transformEle(income.expenses),
    other_income: transformEle(income.other_income),
    store: {
      storeId: {
        $oid:new ObjectId("542adf68e4b0e67da1edd6b7")
      }
    },
    _id: income._id,
    __v: income.__v
  }
});


fs.writeFile(file, JSON.stringify(incomes, null, 4), function (err) {
  if(err) {
    console.log("Error writing new incomes");
  } else {
    console.log("Write incomes")
  }
})

function transformEle(input) {
  return _.map(input, function (x) {
    return {
      locationId: x.location,
      description: x.Type || x.description,
      amount: x.amount
    }
  });
}