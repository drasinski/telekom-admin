var clients = require('./clients');
var _ = require('underscore');
var secrets = require('../backend/config/secrets');
var twilioclient = require('twilio')(secrets.twilio.sid, secrets.twilio.token);
var async = require('async');

console.log(clients.length)
var x = clients.splice(2639);
console.log("clients left:  " + x.length)


var telephoneRegex = /^\d{10}$/;

async.series(_.chain(x).filter(function (client) {
  return telephoneRegex.test(String(client.telephone));
}).map(function (client) {

  var message;

  var name = (client.firstname ||'');


  if(client.language === 'spanish') {
    message = "Felis Dia De Pascas " + name +', de TELEKOM!';
  } else if(client.language === 'ukraine') {
    message = "Bitayu z Velikodnimu cviatamu.Xay bog vam dopomagaie Bazhayu shchastia i zdoroviya uspixiv zavzhdu i u v somy. - TELEKOM";
  } else if(client.language === 'czech') {
    if(name === '') {
      message = "Stastne a vesele velikonocni svatki - a bohatou pomlazku preje Telekom."
    } else {
      message = name + ', Stastne a vesele velikonocni svatki - a bohatou pomlazku preje Telekom.'
    }
  } else if(client.language === 'english') {
    message = "Have a Happy Easter " + name + ", from TELEKOM!";
  } else {
    message = "Wesolych Swiat Wielkanocnych oraz Zdrowia i Pomyslnosci "+ name + ", Zyczy Telekom!";
  }

  return function (callback) {
    twilioclient.sendSms({
      body: message,
      to: client.telephone,
      from: '7732506660'
    }, function (err, resp) {
      if(err) {
        client.error = err;
        callback(client);
      } else {
        callback(null, client);
      }
    });
  }

}).value(), function (err, results) {

  if(err) {
    console.log(err);
    console.log(err.telephone);
    console.log((x.indexOf(_.findWhere(x, {'telephone':err.telephone}))) );
    var idx = 2639 + parseInt(x.indexOf(_.findWhere(x, {'telephone':err.telephone})),10) + 1;
    console.log("index is:  " + idx );
  } else {
    console.log(results);
  }

});

