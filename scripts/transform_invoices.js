var _ = require('lodash');
var fs = require('fs');
// var file = require('./newclients');
var ObjectId = require('mongoose').Types.ObjectId;
var path = require('path');
var file = path.resolve(__dirname, '../newinvoices_mapped.json');
var moment = require("moment");

var invoices = require('../invoices');
// var users = require('../users');

var types = require('../types');

// var items_no_inv = require('../items_not_in_inventory')
// var inventories = require('../newinventories');

var invoices = require('../invoices')
var ObjectId = require('mongoose').Types.ObjectId;


var mongodbRegex = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;

_.each(invoices, function (val) {
 if(!mongodbRegex.test(val._id.$oid)) {
   console.log(val._id);
 }
})


// invoices = _.first(invoices, 200);

// invoices = _.map(invoices, function (invoice) {

//   var new_invoice = {
//     _id: invoice._id,
//     __v: invoice.__v,
//     created_date: {
//       $date: new Date(ObjectId(invoice._id.$oid).getTimestamp()).getTime()
//     },
//     last_modified_date: {
//       $date: new Date(ObjectId(invoice._id.$oid).getTimestamp()).getTime()
//     },
//     subtotal: invoice.subtotal,
//     taxtotal: invoice.taxtotal,
//     grandtotal: invoice.grandtotal,
//     typeofpayment: invoice.typeofpayment,
//     invoice_number: String(invoice.invoice_number),
//     servicecharge: invoice.servicecharge,
//     user: {
//       username: invoice.processed_by
//     },
//     store: {
//       storeId: {
//         $oid:new ObjectId("542adf68e4b0e67da1edd6b7")
//       },
//       locationId: invoice.location
//     }
//   };

//   if(_.isUndefined(_.findWhere(users, {username:invoice.processed_by}))) {
//     new_invoice.user.userId = null;
//   } else {
//     new_invoice.user.userId = _.findWhere(users, {username:invoice.processed_by})._id;
//   }

//   new_invoice.client = {};
//   if(invoice.firstname) {
//     new_invoice.client.firstname = invoice.firstname;
//   }
//   if(invoice.lastname) {
//     new_invoice.client.lastname = invoice.lastname;
//   }
//   if(invoice.telephone) {
//     new_invoice.client.telephone = String(invoice.telephone);
//   }
//   if(invoice.address) {
//     new_invoice.client.address = invoice.address
//   } else {
//     new_invoice.client.address = {
//       location: {
//           coordinates: [],
//           type: "Point"
//       }
//     }
//   }
//   if(invoice.client) {
//     new_invoice.client.clientId = invoice.client;
//   }

//   var z = transformTrans.call(invoice);
//   new_invoice.transactions = z.transactions;

//   return new_invoice;

// });

// var international = [];
// _.each(invoices, function (invoice) {
//   international = _.chain(invoice.transactions).filter(function (trans) {
//     return trans.Type === 'Utilities';
//   }).reduce(function (memo, trans) {
//     if(_.contains(memo, trans.Description)) {
//       return memo;
//     } else {
//       memo.push(trans.Description);
//       return memo;
//     }
//   }, international).value();
// });

// console.log(international)

// fs.writeFile(file, JSON.stringify(invoices, null, -1), function (err) {
//   if(err) {
//     console.log("Error writing new invoices");
//   } else {
//     console.log("Write invoices")
//   }
// })

// /* Private Helper Functions */
// function money_round(num) {
//   return Math.round(num*100)/100;
// }


// function transformTrans() {

//   var att = ['AT@T','PreCash - AT&T','33.85','','23.07','AT&T Fax','AT@T res','AT@ T wireless','AT@T Residential service','AT@T residential','att','AT&T','AT&T Residential','at@t','At@t','AT@t','AT&T Wireless','AT@T Residential','AT@T Wireless']
//   var comed = ['Comm Ed','ComE','Commonwealth edisson','ComeEd','ComEd','comed','Commonweath Edison','Commonwealth Edison','Comed','Commonwealth edison','Commonwealth Edisson','Commonwaelth Edison','Comonwealth Edison','Commonwealt Edison']
//   var pgas = ['Peoples Gas','peoples gas','People Gas','People  Gas','People Gasd']
//   var dish = ['Dish Network','dish']
//   var comcast = ['comcast','Comcast','Comcast Internet']
//   var usc = ['us cellular','US Cellular']
//   var attuverse = ['att u verse','ATT U Verse','at&T Uverse','U Verse','AT&T UVERSE','att uverse','AT&T uverse','AT&T U-Verse','AT@t U verse','At@t U-Verse','AT@t U Verse','AT&T U-verse','AT@T U verse','AT@T U Verse','AT&T Uverse']
//   var tmobile = ['T-Mobile Post Paid','T Mobile Pre-paid','tmobile postpaid','T-Mobile (post-paid)','T-Mobile Postpaid','tmobile','T Mobile Postpaid','T Mobile','T -Mobile Postpaid','T-Mobile Post-Paid','t-mobile','T-Mobile','TMobile Postpay','T Mobile Wireless Postpaid']
//   var vw = ['Volkswagen Credit']
//   var statefarm = ['state farm','State Farm Ins','State Farm','State Farm Auto Ins','State Farm Insurance','State Farm Insuranse']
//   var sprint = ['SPRINT','sprint','sprint postpaid']
//   var toyota = ['Toyota','Toyota Financial Service','Toyota Financial Sevice']
//   var macys = ['macy\'s','Macy\'s','Macys','macys']
//   var nicor = ['nicor gas']
//   var verizon = ['verizon','Verizon Postpaid','Verizon Wireless','verizon wireless']
//   var sears = ['Sears']
//   var directtv =  ['Direct TV']
//   var khols = ['Kohls']
//   var poltel = ['Poltel']
//   var clear = ['clear internet','clear','Clear Internet','Clear - ACC#: 2202477','Clear payment','Clear','Clear - ACC#','Clear Payment']
//   var other = ['Charger','charger','punch']

//   var self = this;
//   self.transactions = _.map(self.transactions, function (trans) {
//     var new_trans = {};
//     if(trans.Type === 'Phone Bill') {
//       var item;
//       if(trans.Description !== 'h20 Bolt' && trans.Description !== 'Clear') {
//         item = _.findWhere(types, {category_slug:'phone-bill', description: trans.Description});
//         new_trans.typeId = item._id;
//         new_trans.amount = trans.amount;
//         new_trans.amtsale = trans.amtsale || 0;
//         new_trans.servicecharge = trans.servicecharge || 0;
//         new_trans.taxtotal = 0;
//         new_trans.total = trans.amount - new_trans.amtsale + new_trans.servicecharge;
//       } else {
//         item = _.findWhere(types, {category_slug:'internet-bill', description: trans.Description});
//         new_trans.typeId = item._id;
//         new_trans.amount = trans.amount;
//         new_trans.amtsale = trans.amtsale || 0;
//         new_trans.servicecharge = trans.servicecharge || 0;
//         new_trans.taxtotal = 0;
//         new_trans.total = trans.amount - new_trans.amtsale + new_trans.servicecharge;
//       }
//     } else if(trans.Type === 'Phone Sale') {
//       var item;
//       if(_.isUndefined(trans.imei)) {
//         _.each(items_no_inv, function (l_item) {
//           if(l_item.invoice.invoiceId.$oid === self._id.$oid) {
//             item = _.cloneDeep(l_item);
//             if(self._id.$oid === "52a3be7819038d020000001e") {
//               console.log(trans)
//               console.log(self._id)
//             }
//           }
//         });
//       } else {
//         item = _.findWhere(inventories, {imei: String(trans.imei)});
//       }
//       if(_.isUndefined(item)) {
//         console.log(trans)
//         console.log(self.invoice_number)
//       }
//       new_trans.amount = trans.amount;
//       new_trans.inventoryId = item._id;
//       new_trans.servicecharge = trans.servicecharge || 0;
//       new_trans.amtsale = trans.amtsale || 0;
//       if(self.taxtotal > 0) {
//         new_trans.taxtotal = money_round((trans.amount-new_trans.amtsale)*0.095);
//       } else {
//         new_trans.taxtotal = 0;
//       }
//       new_trans.total = trans.amount - new_trans.amtsale + new_trans.servicecharge + new_trans.taxtotal;
//     } else if(trans.Type === 'International') {
//       var item = _.findWhere(types,{category_slug:'international',description: trans.Description});
//       new_trans.typeId = item._id;
//       new_trans.amount = trans.amount;
//       new_trans.amtsale = trans.amtsale || 0;
//       new_trans.servicecharge = trans.servicecharge || 0;
//       new_trans.taxtotal = 0;
//       new_trans.total = trans.amount - new_trans.amtsale + new_trans.servicecharge;
//     } else if(trans.Type === 'Activation') {
//       if(trans.Description === 'Red Pocket' || trans.Description === 'h20 Bolt') {
//         var item = _.findWhere(types,{category_slug:'internet-activation',description: trans.Description});
//         new_trans.typeId = item._id;
//         new_trans.amount = trans.amount;
//         new_trans.amtsale = trans.amtsale || 0;
//         new_trans.servicecharge = trans.servicecharge || 0;
//         new_trans.taxtotal = 0;
//         new_trans.total = trans.amount - new_trans.amtsale + new_trans.servicecharge;
//       } else {
//         var item = _.findWhere(types,{category_slug:'activation',description: trans.Description});
//         new_trans.typeId = item._id;
//         new_trans.amount = trans.amount;
//         new_trans.amtsale = trans.amtsale || 0;
//         new_trans.servicecharge = trans.servicecharge || 0;
//         new_trans.taxtotal = 0;
//         new_trans.total = trans.amount - new_trans.amtsale + new_trans.servicecharge;
//       } 
//     } else if(trans.Type === 'Utilities' && !_.contains(other, trans.Description)) {
//       var item;
//       if(_.contains(att,trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'AT&T'});
//       } else if(_.contains(comed,trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'ComEd'});
//       } else if(_.contains(pgas,trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'Peoples Gas'});
//       } else if(_.contains(dish,trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'Dish Network'});
//       } else if(_.contains(comcast, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'Comcast'});
//       } else if(_.contains(usc, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'US Cellular'});
//       } else if(_.contains(attuverse, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'AT&T UVerse'});
//       } else if(_.contains(tmobile, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'T-Mobile'});
//       } else if(_.contains(vw, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'Volkswagon'});
//       } else if(_.contains(statefarm, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'State Farm'});
//       } else if( _.contains(sprint, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'Sprint'});
//       } else if(_.contains(toyota, trans.Description)) {
//         item =  _.findWhere(types, {category_slug:'utilities', description:'Toyota'});
//       } else if(_.contains(macys, trans.Description)) {
//         item =  _.findWhere(types, {category_slug:'utilities', description:'Macys'});
//       } else if(_.contains(nicor, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'Nicor Gas'});
//       } else if(_.contains(verizon, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'Verizon Wireless'});
//       } else if( _.contains(sears, trans.Description)) {
//         item =  _.findWhere(types, {category_slug:'utilities', description:'Sears'});
//       } else if(_.contains(directtv, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'DirectTV'});
//       } else if(_.contains(khols, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'Khols'});
//       } else if(_.contains(poltel, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'utilities', description:'PolTel'});
//       } else if(_.contains(clear, trans.Description)) {
//         item = _.findWhere(types, {category_slug:'internet-bill', description: 'Clear'});
//       } else {
//         console.log(trans.Description)
//         console.log("NOT FOUND");
//       }
//       new_trans.typeId = item._id;
//       new_trans.amount = trans.amount;
//       new_trans.amtsale = trans.amtsale || 0;
//       new_trans.servicecharge = trans.servicecharge || 0;
//       new_trans.taxtotal = 0;
//       new_trans.total = trans.amount - new_trans.amtsale + new_trans.servicecharge;
//     } else if(trans.Type === 'Other' || (trans.Type === 'Utilities' && _.contains(other, trans.Description))) {
//       var item = {
//         category_slug: 'other',
//         category: 'Other',
//         fixed_cost: 0,
//         description: trans.Description
//       };
//       new_trans.amount = trans.amount;
//       new_trans.amtsale = trans.amtsale || 0;
//       new_trans.servicecharge = trans.servicecharge || 0;
//       if(self.taxtotal > 0) {
//         new_trans.taxtotal = money_round((trans.amount-new_trans.amtsale) * 0.095);
//       } else {
//         new_trans.taxtotal = 0;
//       }
//       new_trans.total = trans.amount - new_trans.amtsale + new_trans.servicecharge;
//       new_trans.otherId = item;
//     }
//     return new_trans;
//   });
//   var tax = _.reduce(self.transactions, function (memo, trans) {
//     return memo + trans.taxtotal;
//   },0);
//   if(tax !== self.taxtotal) {
//     _.map(self.transactions, function (trans) {
//       trans.taxtotal = money_round((trans.amount-trans.amtsale) * 0.095);
//       return trans;
//     });
//   }
//   return self;
// }

// var x = _.chain(types).filter(function (type) {
//   return type.category_slug === 'internet-activation';
// }).map(function (type) {
//   return type.description;
// }).value();

// console.log(x)

// var y = ['Ultra Mobile','Simple Mobile','T-Mobile','Page Plus','h20 Wireless','Verizon Wireless','GO Smart','Lyca Mobile','AT&T','Red Pocket','Boost Mobile','Spot Mobile','h20 Bolt','Net10'];

// console.log(international)

// ['Ultra Mobile','Simple Mobile','T-Mobile','Page Plus','h20 Wireless','Verizon Wireless','GO Smart','Lyca Mobile','AT&T','Red Pocket','Boost Mobile','Spot Mobile','h20 Bolt','Net10']

// [ 'AT@T','PreCash - AT&T','33.85','','23.07','AT&T Fax','AT@T res','AT@ T wireless','AT@T Residential service','AT@T residential','att','AT&T','AT&T Residential','at@t','At@t','AT@t','AT&T Wireless','AT@T Residential','AT@T Wireless']
// ['Comm Ed','ComE','Commonwealth edisson','ComeEd','ComEd','comed','Commonweath Edison','Commonwealth Edison','Comed','Commonwealth edison','Commonwealth Edisson','Commonwaelth Edison','Comonwealth Edison','Commonwealt Edison']
// ['Peoples Gas']
// ['Dish Network','dish']
// ['comcast','Comcast','Comcast Internet']
// ['peoples gas','People Gas','People  Gas','People Gasd']
// ['us cellular','US Cellular']
// ['att u verse','ATT U Verse','at&T Uverse','U Verse','AT&T UVERSE','att uverse','AT&T uverse','AT&T U-Verse','AT@t U verse','At@t U-Verse','AT@t U Verse','AT&T U-verse','AT@T U verse','AT@T U Verse','AT&T Uverse']
// ['T-Mobile Post Paid','T Mobile Pre-paid','tmobile postpaid','T-Mobile (post-paid)','T-Mobile Postpaid','tmobile','T Mobile Postpaid','T Mobile','T -Mobile Postpaid','T-Mobile Post-Paid','t-mobile','T-Mobile','TMobile Postpay','T Mobile Wireless Postpaid']
// ['Volkswagen Credit']
// ['state farm','State Farm Ins''State Farm','State Farm Auto Ins','State Farm Insurance','State Farm Insuranse']
// ['SPRINT','sprint','sprint postpaid']
// ['Toyota','Toyota Financial Service','Toyota Financial Sevice']
// ['macy\'s','Macy\'s','Macys','macys']
// ['nicor gas']
// ['verizon','Verizon Postpaid','Verizon Wireless','verizon wireless']
// ['Sears']
// ['Direct TV']
// ['Kohls']
// ['Poltel']

// ['Charger','charger','punch']

