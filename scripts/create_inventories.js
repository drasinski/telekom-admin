var clients = require('../clientstest');
var _ = require('underscore');
var fs = require('fs');
// var file = require('./newclients');
var ObjectId = require('mongoose').Types.ObjectId;
var path = require('path');
var file = path.resolve(__dirname, '../items_not_in_inventory.json');
var moment = require("moment");

var invoices = require('../invoices');

var international = [];
_.each(invoices, function (invoice) {
  international = _.chain(invoice.transactions).filter(function (trans) {
    return trans.Type === 'Phone Sale';
  }).reduce(function (memo, trans) {
    if(_.contains(memo, trans.Description)) {
      return memo;
    } else {
      if(_.isUndefined(trans.imei)){
        invoice.phone_name = trans.Description;
        invoice.phone_cost = trans.amount;
        memo.push(invoice);
      }
      return memo;
    }
  }, international).value();
});

var key ={'Apple iPhone 3G 8GB RB Unlck':109,'Apple iPhone 3Gs - 8GB RB Unlck':129,'iPhone 5C':550,'Apple iPhone 4G 8GB Unlck Quad Band':295,'Samsung Galaxy 2':239,'Samsung Galaxy 3 T999, T-Mobile RB':369,
          'Samsung Galaxy 4':175,'Alcatel 606A T-Mobile RB':24,'Alcatel 665, Flip T-Mobile RB':26,'Samsung T139 (Gray)':33,'Samsung T259 (Blue)':65,
          'Samsung GT-C3520 (PINK)':69,'Samsung T459 White':60,'T-Mobile ZTE V768 Concord':59,'T-Mobile Garmin A50 Asus':50,'Sony Ericsson Z550i':39,'Blackberry 8320':60,
          'Alcatel 0T510A AT&T':26,'Samsung i727 Skyrocket AT&T':249,'Samsung SGH-C3520 GSM Flip':69,'T-Mobile - LG GS170':32,'T-Mobile - Nokia X2':49,'BLU - Dash 3.5 D172a':89,'BLU - Jenny T172':18,'LG GT405':69,
          'Samsung T159':60,'Kindle Fire HDX':179,'HTC One':215,'Samsung S4 Mini':110,'Samsung Note 3':175, 'Nexus 5':380, 'Nokia Lumia 521':80,'Nokia Lumia 510':65,'Bolt Flash':35,'Bolt Spider':65, 'iPhone 5S White':639, 'iPhone 5S Black':710
        }

international = _.map(international, function (invoice) {
  return {
    _id: {
      $oid: new ObjectId()
    },
    created_date: {
      $date: new Date(ObjectId(invoice._id.$oid).getTimestamp()).getTime()
    },
    last_modified_date: {
      $date: new Date(ObjectId(invoice._id.$oid).getTimestamp()).getTime()
    },
    store: {
      storeId: {
        $oid:new ObjectId("542adf68e4b0e67da1edd6b7")
      },
      locationId: invoice.location
    },
    invoice: {
      invoiceId: invoice._id
    },
    category: 'Phone Sale',
    category_slug: 'phone-sale',
    sold: true,
    fixed_cost: key[invoice.phone_name],
    description: invoice.phone_name,
    imei: "No IMEI",
    amount: invoice.phone_cost
  };
});

fs.writeFile(file, JSON.stringify(international, null, 4), function (err) {
  if(err) {
    console.log("Error writing new inventory");
  } else {
    console.log("Write inventory")
  }
})

