var passportSocketIo = require('passport.socketio');
var secrets = require('./backend/config/secrets');
var _ = require('underscore');
var async = require('async');
var Message = require('./backend/models/message');

module.exports = function (io, sessionStore) {

  function serializeUser() {
    var self = this;
    return {
      _id: self._id,
      username: self.username,
      thumbnail: self.thumbnail,
      firstname: self.firstname,
      lastname: self.lastname,
      userId: self._id
    };
  }

  function getOnJoinContent(roomId, userId) {
    var names = _.chain(io.sockets.adapter.rooms[roomId]).keys().map(function (clientId) {
      return io.sockets.connected[clientId];
    }).reject(function (socket) {
      return socket.request.user._id.equals(userId);
    }).groupBy(function (socket) {
      return socket.request.user._id;
    }).map(function (grouped) {
      return grouped[0];
    }).map(function (socket) {
      return socket.request.user.name;
    }).uniq().value();
    console.log(names);
    if(names.length === 1) {
      return names[0] + ' is online.';
    } else if(names.length === 2) {
      return names.join(' and ') + ' are online.';
    } else if(names.length === 0) {
      return "You are the only one online";
    } else {
      return _.initial(names).join(', ') + ' and ' + _.last(names) + ' are online.';
    }
  }

  function userAlreadyInRoom(roomId, userId) {
    return (_.chain(io.sockets.adapter.rooms[roomId]).keys().map(function (clientId) {
      return io.sockets.connected[clientId];
    }).filter(function (socket) {
      return socket.request.user._id.equals(userId);
    }).value().length > 1);
  }

  io.use(passportSocketIo.authorize({
      store: sessionStore,
      key: secrets.server.cookieKey,
      secret: secrets.server.cookieSecret,
      success: function onAuthorizeSuccess(data, accept){
        // if this is an admin User prepend the store ID
        if(data.user.admin_user) {
          data.user.store = {
            storeId: data._query.storeId
          }
        }
        accept();
      },
      fail: function onAuthorizeFailure(data, message, err, accept) {
        if(err) {
          throw new Error(message);
        }
        return accept(new Error(message));
      } 
  }));

  io.sockets.on('connection', function (socket) {
    var user = socket.request.user || data.session.user;
    var adminRoom = user.store.storeId + '-admin';
    if(user.admin) {
      socket.join(adminRoom, function (err) {
        if(!err) {
          console.log(user.name + " has joined room:  " + adminRoom);
        } else {
          console.log("Failed to join room " + adminRoom + ' because ' + err);
        }
      });
    }

    var storeRoom = user.store.storeId;
    socket.join(storeRoom, function (err) {
      if(!err) {
        if(!userAlreadyInRoom(storeRoom, user._id)) {
          socket.broadcast.to(storeRoom).emit('user joined', {
            content: user.name + ' has joined.',
            type: 'notification',
            created_date: _.now()
          });
        }
        io.to(socket.id).emit('on join', {
          type:'notification',
          created_date: _.now(),
          content: getOnJoinContent(storeRoom, user._id)
        });
        console.log(user.name + " has joined room:  " + storeRoom);
      } else {
        console.log("Failed to join room " + storeRoom + " because " + err);
      }
    });

    socket.on('disconnect', function () {
      console.log(user.name + " has disconnected.");
      if(!userAlreadyInRoom(storeRoom, user._id)) {
        socket.broadcast.to(storeRoom).emit('user left', {
          content: user.name + ' has left.',
          type: 'notification',
          created_date: _.now()
        });
      }
    });

    socket.on('new message', function (data) {
      var message = {
        content: data.content,
        type:data.type,
        store: {
          storeId: user.store.storeId
        },
        user: serializeUser.call(user)
      }
      message = new Message(message);
      message.save(function (err, messageDb) {
        if(!err) {
          socket.broadcast.to(storeRoom).emit('new message', messageDb);
        } else {
          console.log(err);
        }
      })
    });

    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', function () {
      socket.broadcast.to(storeRoom).emit('typing', serializeUser.call(user));
    });
  
    // when the client emits 'stop typing', we broadcast it to others
    socket.on('stop typing', function () {
      socket.broadcast.to(storeRoom).emit('stop typing', serializeUser.call(user));
    });

    socket.on('read messages', function () {
      var sockets = passportSocketIo.filterSocketsByUser(io, function (user) {
        return user._id.equals(user._id);
      });
      var emitSocks = _.chain(sockets).filter(function (sock) {
        return sock.id !== socket.id;
      }).map(function (sock) {
        return function (cb) {
          io.to(sock.id).emit('read messages');
          cb();
        }
      }).value();
      async.parallel(emitSocks, function (err, msg) {
        console.log("Succesfully emitted:  " + msg);
      });
    });

  });

  return io;

};