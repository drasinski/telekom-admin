'use strict';

var pkg = require('./package.json');

module.exports = {
    source: {
        folder: 'public_store',
        index: 'src/index.html',
        files: {
            app: {
                js: [
                    'src/**/*.js'
                ],
                jade: [
                    'src/app/**/*.jade',
                    'src/common/**/*.jade'
                ],
                less: [
                    'src/less/main.less'
                ],
                assets: [
                    'src/assets/**/*.*'
                ]
            },
            vendor: {
                js: [
                  'vendor/jquery/jquery.js',
                  'vendor/ng-file-upload/angular-file-upload-shim.js',
                  'vendor/angular/angular.js',
                  'vendor/angular-animate/angular-animate.js',
                  'vendor/angular-bootstrap/ui-bootstrap-tpls.js',
                  'vendor/angular-ui-router/release/angular-ui-router.js',
                  'vendor/angular-ui-utils/ui-utils.js',
                  'vendor/fastclick/lib/fastclick.js',
                  'vendor/d3/d3.js',
                  'vendor/socket.io-client/socket.io.js',
                  'vendor/angular-socket-io/socket.js',
                  'vendor/angular-socket-io/socket-deferred.js',
                  'vendor/lodash/dist/lodash.js',
                  'vendor/angular-deferred-bootstrap/angular-deferred-bootstrap.js',
                  'vendor/moment/moment.js',
                  'vendor/SHA-1/sha1.js', // used by angulartics
                  'vendor/waypoints/waypoints.js', // used by angulartics
                  'vendor/angulartics/src/angulartics.js',
                  'vendor/angulartics/src/angulartics-ga.js',
                  'vendor/ngImgCrop/compile/unminified/ng-img-crop.js',
                  'vendor/ui-iconpicker/dist/scripts/ui-iconpicker.js',
                  'vendor/hammerjs/hammer.js',
                  'vendor/angular-gestures/gestures.js',
                  'vendor/nvd3/nv.d3.js',
                  'vendor/angularjs-nvd3-directives/dist/angularjs-nvd3-directives.js',
                  'vendor/ng-file-upload/angular-file-upload.js',
                  'vendor/angular-popover-toggle/popover-toggle.js',
                  'vendor/snapjs/snap.js',
                  'vendor/angular-cache/dist/angular-cache.js',
                  'vendor/angular-cookies/angular-cookies.js',
                  'vendor/angular-snap/angular-snap.js',
                  'vendor/angular-moment/angular-moment.js',
                  'vendor/angular-sanitize/angular-sanitize.js',
                  'vendor/angular-scroll-glue/src/scrollglue.js',
                  'vendor/angularjs-slider/rzslider.js',
                  'vendor/angular-promise-tracker/promise-tracker.js',
                  'vendor/angular-notify/dist/angular-notify.js'
                ],
                css: [
                  'vendor/nvd3/nv.d3.css',
                  'vendor/angular-snap/angular-snap-only.css',
                  'vendor/ui-iconpicker/dist/styles/ui-iconpicker.css',
                  'vendor/ngImgCrop/compile/unminified/ng-img-crop.css'
                ],
                assets: [
                ]
            }
        }
    },
    targets: {
        buildFolder: 'public_store/build/',
        distFolder: 'public_store/bin/',
        tempFolder: '.temp/',
        cordovaFolder: 'cordova/',
        assetsFolder: 'assets',
        nwjsFolder: 'nwjs/',
        resourcesFolder: 'resources/',
        minified: {
            js: pkg.name+'-'+pkg.version+'-public_store.js',
            css: pkg.name+'-'+pkg.version+'-public_store.css',
            templateCache: 'templates.js'
        }
    },
    angularModuleName: 'templates'
};
