// Contains everything to develop BoardZ! using a live reload server

'use strict';

module.exports = function (key) {

  var gulp = require('gulp'),
      del = require('del'),
      path = require('path'),
      inject = require('gulp-inject'),
      watch = require('gulp-watch'),
      server = require('gulp-server-livereload'),
      runSequence = require('run-sequence'),
      utils,
      less = require('gulp-less'),
      jade = require('gulp-jade'),
      ngTemplate = require('gulp-ng-template'),
      buildConfig;

    if(key === 'public_store') {
      buildConfig = require('../gulp.config.public_store');
    } else {
      buildConfig = require('../gulp.config.public_admin');
    }

    utils = require('./utils.js')(buildConfig);

    gulp.task('dev:clean'+':'+key, function (done) {
        del([
            buildConfig.targets.buildFolder
        ]).then(function () {
            done();
        });
    });

    gulp.task('dev:copy-assets'+':'+key, function () {
        return utils.getAssets(buildConfig.source.folder, true, true)
            .pipe(gulp.dest(buildConfig.targets.buildFolder));
    });

    gulp.task('dev:build-css'+':'+key, function() {
        return gulp.src(utils.getLessFiles(buildConfig.source.folder))
            .pipe(less({
                paths: []
            }))
            .pipe(gulp.dest(buildConfig.targets.buildFolder));
    });

    gulp.task('dev:build-jade'+':'+key, function() {
        return gulp.src(utils.getJadeFiles(buildConfig.source.folder))
            .pipe(jade({}))
            // .pipe(gulp.dest(buildConfig.targets.buildFolder))
            .pipe(ngTemplate({
                moduleName: buildConfig.angularModuleName,
                filePath: buildConfig.targets.minified.templateCache,
                standalone:true
            }))
            .pipe(gulp.dest(buildConfig.targets.buildFolder+'/src/app'));
    });

    gulp.task('dev:copy-source'+':'+key, function () {
        return utils.getSources(buildConfig.source.folder)
            .pipe(gulp.dest(buildConfig.targets.buildFolder));
    });

    gulp.task('dev:inject'+':'+key, function () {
        var injectables = utils.getSources(buildConfig.targets.buildFolder, false);

        return gulp.src(path.join(buildConfig.source.folder, buildConfig.source.index))
            .pipe(inject(injectables, {
                ignorePath: buildConfig.targets.buildFolder,
                addRootSlash: false,
                addPrefix:buildConfig.source.folder
            }))
            .pipe(gulp.dest(buildConfig.targets.buildFolder));
    });

    gulp.task('dev:default'+':'+key, function (done) {
        runSequence('dev:clean'+':'+key,
            'dev:build-css'+':'+key,
            'dev:build-jade'+':'+key,
            'dev:copy-source'+':'+key,
            'dev:copy-assets'+':'+key,
            'dev:inject'+':'+key,
            done);
    });

    // gulp.task('dev:start-live-server', function () {
    //     gulp.src(buildConfig.targets.buildFolder)
    //         .pipe(server({
    //             livereload: true,
    //             open: false
    //         }));
    // });

    // gulp.task('dev:livereload', function () {
    //     runSequence('dev:default', 'dev:start-live-server', function () {
    //         deltaWatch();
    //     });
    // });

    // gulp.task('dev:watch', function () {
    //     runSequence('dev:default', function () {
    //         deltaWatch();
    //     });
    // });

    // function deltaWatch() {
    //     watch(buildConfig.source.folder, { base: buildConfig.source.folder }, function (vinyl) {
    //         if (vinyl.event && (vinyl.event === 'add' || vinyl.event === 'unlink' || vinyl.path.indexOf('index.html') > -1)) {
    //             gulp.start('dev:inject');
    //         }
    //     })
    //         .pipe(gulp.dest(buildConfig.targets.buildFolder));
    // }


}
