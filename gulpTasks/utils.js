// Utility functions
'use strict';

var gulp = require('gulp'),
    path = require('path');
    // buildConfig = require('../gulp.config');

module.exports = function(buildConfig) {

    var get_mapped_files = function getMappedSourceFiles(files, baseFolder) {
        return files.map(function (file) {
            return path.join(baseFolder, file);
        });
    }
    
    var get_jade_files = function getJadeFiles(baseFolder) {
        var self = this;
        return [].concat(
            self.getMappedSourceFiles(buildConfig.source.files.app.jade, baseFolder)
        );    
    }
    
    var get_less_files = function getLessFiles(baseFolder) {
        var self = this;
        return [].concat(
            self.getMappedSourceFiles(buildConfig.source.files.app.less, baseFolder)
        );
    }
    
    var get_css_html_files = function getCssHtmlFiles(baseFolder) {
        var self = this;
        return [].concat(
            self.getMappedSourceFiles(['main.css'], baseFolder),
            self.getMappedSourceFiles([path.join('src','app', buildConfig.targets.minified.templateCache)], baseFolder)
        );
    }
    
    var get_source_files = function getSourceFiles(baseFolder) {
        var self = this;
        return [].concat(
            self.getMappedSourceFiles(buildConfig.source.files.vendor.js, baseFolder),
            self.getMappedSourceFiles(buildConfig.source.files.vendor.css, baseFolder),
            self.getMappedSourceFiles(buildConfig.source.files.app.js, baseFolder)
        );
    }
    
    var get_sources = function getSources(baseFolder, read) {
        var self = this;
        if (read === undefined) {
            read = true;
        }
    
        return gulp.src(self.getSourceFiles(baseFolder).concat(self.getCssHtmlFiles(baseFolder)), { read: !!read, base: baseFolder });
    }
    
    var get_assets = function getAssets(baseFolder, read, copyWithoutStructure) {
        var self = this;
        if (read === undefined) {
            read = true;
        }
    
        var config = {
            read: read,
            base: path.join(baseFolder, 'src')
        };
    
        if (!copyWithoutStructure) {
            config.base = baseFolder;
        }
    
        return gulp.src([].concat(
                self.getMappedSourceFiles(buildConfig.source.files.vendor.assets, baseFolder),
                self.getMappedSourceFiles(buildConfig.source.files.app.assets, baseFolder)
            )
            , config);
    }

    return {
      getCssHtmlFiles: get_css_html_files,
      getMappedSourceFiles: get_mapped_files,
      getLessFiles: get_less_files,
      getJadeFiles: get_jade_files,
      getSourceFiles: get_source_files,
      getSources: get_sources,
      getAssets: get_assets
    }
};