angular.module( 'cellbetter-admin', [
  'templates',
  'admin',
  'profile',
  'stores',
  'store',
  'ui.router',
  'security',
  'ui.bootstrap',
  'ui.bootstrap.tpls',
  'hdirectives',
  'snap',
  'constants',
  'ngTouch',
  'pageLoadSpinner'
])

.config(['$urlRouterProvider', '$locationProvider','snapRemoteProvider', function ($urlRouterProvider,$locationProvider,snapRemoteProvider) {
  snapRemoteProvider.globalOptions.disable = 'right';
  snapRemoteProvider.globalOptions.tapToClose = true;
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/admin/stores');
}])
.run(['$rootScope', '$state', '$stateParams', 'security','$http', function ($rootScope, $state, $stateParams, security, $http) {
  $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, fromState, fromParams) {
    if(angular.isDefined(fromState.name)) {
      $state.previous = angular.copy(fromState.name);
    } else {
      $state.previous = 'payments';
    }
    $state.prevParams = angular.copy(fromParams);
  });
  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    console.log(error);
    console.log(error.stack);
  });

  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
  // Get the current user when the application) starts
  // (in case they are still logged in from a previous session) 
  // security.requestCurrentUser();
  FastClick.attach(document.body);

}])

.controller('AppCtrl', ['$scope', 'security','pageLoad','snapRemote', function ($scope, security, pageLoad, snapRemote) {

  $scope.isAuthenticated = security.isAuthenticated;
  $scope.isAdmin = security.isAdmin;
  $scope.logout = security.logout;  
  $scope.login = security.showLogin;
  $scope.pageLoad = pageLoad;

  $scope.year = new Date().getFullYear();
  $scope.month = moment().format('MMM');

  $scope.snapToggle = function snapToggle(side) {
    snapRemote.toggle(side);
  };

  // Clear the search bar if there is an error and we succesfully change state
  $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | Cell Better' ;
    }
  });

  $scope.$watch(function() {
    return security.currentUser;
  }, function (currentUser) {
    $scope.currentUser = currentUser;
  });

  snapRemote.getSnapper().then(function (snapper) {
    snapper.on('open', function() {
      $scope.snapActive = true;
    });
    
    snapper.on('close', function() {
      $scope.snapActive = false;
    });
  });
  
}]);

