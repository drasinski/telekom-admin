angular.module( 'admin.users', [
  'ui.router',
  'security.authorization',
  'resource.users',
  'user.srvcs',
  'service.confirm',
  'constants'
])
.config([ '$stateProvider','securityAuthorizationProvider', function config( $stateProvider, securityAuthorizationProvider) {
  $stateProvider.state( 'users', {
    url: '/admin/users',
    views: {
      "main": {
        controller: 'UsersCtrl',
        templateUrl: 'admin/users/templates/users.html'
      }
    },
    resolve: {
      authUser: securityAuthorizationProvider.requireAdminUser,
      users: ['UsersService', 'User', function (UsersService, User) {
        return UsersService.get({}).then(function (users) {
          return _.map(users, User.build);
        });
      }]
    },
    data: {
      pageTitle: 'Users'
    }
  })
  .state('new', {
    url:'/admin/newuser',
    views: {
      "main": {
        controller:'NewUserCtrl',
        templateUrl: 'admin/users/templates/new-user.html'
      }
    },
    resolve: {
      authUser: securityAuthorizationProvider.requireAdminUser,
      user: ['authUser','User', function (authUser, User) {
        return User.build({
          changepassword:true
        });
      }]
    },
    data: {
      pageTitle: 'New User'
    }
  });
}])
.controller('UsersCtrl', ['$scope','users','ConfirmService', function ($scope, users, ConfirmService) {

  $scope.users = users;

  $scope.removeUser = function removeUser(user) {
    ConfirmService('Are you sure you want to delete '+ user.username + ' ?').then(function () {
      user.$remove().then(function () {
        var i = $scope.users.indexOf(user);
        if(i > -1) {
          $scope.users.splice(i,1);
        }
      });
    });
  };

}])
.controller('NewUserCtrl', ['$scope','user','ConfirmService', function ($scope, user, ConfirmService) {
  $scope.user = user;

  $scope.saveUser = function saveUser(user) {
    ConfirmService('Are you want to save new user ' + user.username +' ?').then(function() {
      user.$save().then(function (newUser) {
        $scope.$state.go('users', null, {reload:true});
      });
    });
  };

}])
;
