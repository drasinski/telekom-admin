angular.module( 'store', [
  'ui.router',
  'security.authorization',
  'resource.store',
  'type.srvcs',
  'constants'
])
.config([ '$stateProvider','securityAuthorizationProvider', function config( $stateProvider, securityAuthorizationProvider) {
  $stateProvider.state('store', {
      url:'/admin/stores/{id}',
      views: {
        "main": {
          templateUrl:'store/templates/store.html',
          controller:'StoreCtrl'
        }
      },
      resolve: {
        authUser: securityAuthorizationProvider.requireAuthenticatedUser,
        store: ['StoreService', '$stateParams','Store', function (StoreService, $stateParams, Store) {
          return StoreService.get({_id:$stateParams.id}).then(function (stores) {
            return Store.build(stores[0]);
          });
        }],
        types: ['$stateParams', 'TypeService', 'Types', function ($stateParams, TypeService, Types) {
          return TypeService.get($stateParams.id).then(Types.build);
        }]
      },
      data: {
        pageTitle: 'Store Details'
      }
    })
  .state('newstore', {
    url:'/admin/store/new',
    views: {
      "main": {
        templateUrl:'store/templates/store.html',
        controller:'StoreCtrl'
      }
    },
    resolve: {
      authUser: securityAuthorizationProvider.requireAuthenticatedUser,
      store: ['Store', function (Store) {
        return Store.build({});
      }],
      types:['defaultTypes', 'Types', function (defaultTypes, Types) {
        return Types.build(defaultTypes);
      }]
    },
    data: {
      pageTitle: 'New Store'
    }
  })
    ;
}])
.controller('StoreCtrl', ['$scope', 'store','ConfirmService', 'types', function ($scope, store, ConfirmService, types) {
  $scope.store = store;

  $scope.types = types;

  $scope.saveStore = function saveStore(store, types) {
    ConfirmService("Save your changes to " + store.name + "?").then(function (result) {
      store.$save().then(function (store) {
        types.$save(store);
      });
    })
  };

}]);
