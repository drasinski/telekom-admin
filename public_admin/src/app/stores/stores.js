angular.module( 'stores', [
  'ui.router',
  'security.authorization',
  'resource.store',
  'constants'
])
.config([ '$stateProvider','securityAuthorizationProvider', function config( $stateProvider, securityAuthorizationProvider) {
  $stateProvider.state('stores', {
    url:'/admin/stores',
    views: {
      "main": {
        controller:'StoresCtrl',
        templateUrl:'stores/templates/stores.html'
      }
    },
    resolve: {
      authUser: securityAuthorizationProvider.requireAuthenticatedUser,
      stores:['StoreService', function (StoreService) {
        return StoreService.get({});
      }]
    },
    data: {
      pageTitle: 'Stores'
    }
  })
  ;
}])
.controller('StoresCtrl', ['$scope', 'stores', function ($scope, stores) {
  $scope.stores = stores;
}])
