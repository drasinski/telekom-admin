angular.module( 'profile', [
  'ui.router',
  'security.authorization',
  'service.confirm',
  'constants'
])
.config([ '$stateProvider','securityAuthorizationProvider', function config( $stateProvider, securityAuthorizationProvider) {
  $stateProvider.state('admin', {
    url:'/admin/profile',
    views: {
      "main": {
        controller:'AdminCtrl',
        templateUrl:'profile/templates/admin.html'
      }
    },
    resolve: {
      authUser: securityAuthorizationProvider.requireAuthenticatedSameUser
    },
    data: {
      pageTitle: 'User'
    }
  });
}])
.controller('AdminCtrl', ['$scope','ConfirmService', function ($scope, ConfirmService) {

  $scope.test = 'Hello'

}]);
