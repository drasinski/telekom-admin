angular.module('resource.store', [
  'constants',
  'service.xhr'
])
.factory('StoreService', ['xhr','apiPrefix', function (xhr, apiPrefix) {
  return {
    put: function() {
      return xhr('put',[apiPrefix+'stores/'+this._id, this]);
    },
    get: function(query) {
      var x = angular.extend({per_page:'0',sort_by:'created_date'},query);
      return xhr([apiPrefix+'stores',{params:x}]);
    },
    post: function () {
      return xhr('post',[apiPrefix+'stores',this]);
    }
  }
}])
