angular.module('store.srvcs',[
  'constants',
  'service.address',
  'notify',
  'resource.store'
])
.factory('Store', ['getAddress','notifyService','StoreService', function (getAddress, notifyService, StoreService) {

  function Store(options) {
    angular.extend(this, {contact:{}}, options);
  }

  function deserialize(store) {
    store = angular.extend(store || {})
    if(angular.isDefined(store._id)) {
      store.storeId = store._id;
    } else if(angular.isDefined(store.storeId)) {
      store._id = store.storeId;
    }
    if(angular.isDefined(store.storeIds)) {
      store.storeId = '';
    }
    store.locations = _.map(store.locations, Location.build);
    return store;
  }

  var telephoneRegex = /^\d{10}$/;
  Store.telephoneRegex = angular.copy(telephoneRegex);
  var slugRegex = /^[a-z0-9-]+$/;
  Store.slugRegex = angular.copy(slugRegex); 

  function Location(location) {
    angular.extend(this,{address:{}},location);
  }

  function deserializeLocation(location) {
    if(angular.isDefined(location.address) && angular.isDefined(location.address.location)) {
      delete location.address.location
    }
    if(angular.isDefined(location.id)) {
      delete location.id;
    }
    return location;
  }

  function convertToSlug(Text) {
    return Text.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
  }

  Location.prototype = {
    get id() {
      return convertToSlug(this.name);
    },
    onSelectAddress: function(address) {
      angular.extend(this.address, address);
    },
    isValid: function() {
      return angular.isString(this.address.formatted_address) &&
             angular.isString(this.phone_number) && 
             telephoneRegex.test(this.phone_number) &&
             angular.isString(this.id) &&
             angular.isString(this.name) && 
             !_.isEmpty(this.id) &&
             !_.isEmpty(this.address.formatted_address) &&
             !_.isEmpty(this.name);
    },
    serialize: function() {
      var self = this;
      return {
        id: self.id,
        name: self.name,
        address: self.address,
        phone_number: self.phone_number
      };
    }
  }

  Location.build = function (location) {
    return new Location(deserializeLocation(location));
  }

  // Do we need a store.clear function here?
  Store.prototype = {
    get locationInfo() {
      return _.findWhere(this.locations, {id:this.locationId});
    },
    get telephoneRegex() {
      return telephoneRegex;
    },
    get slugRegex() {
      return slugRegex;
    },
    addLocation: function() {
      var loc = Location.build({});
      this.locations.push(loc);
    },
    removeLocation: function(loc) {
      var i = this.locations.indexOf(loc);
      if(i > -1) {
        this.locations.splice(i,1);
      }
    },
    uniqueLocationNames: function() {
      return (_.chain(this.locations).map(function (loc) {
        return loc.name;
      }).uniq().value().length === this.locations.length);
    },
    isNew: function() {
      return angular.isUndefined(this._id);
    },
    validContactInfo: function() {
      return !_.isEmpty(this.contact.firstname) &&
             angular.isString(this.contact.firstname) &&
             !_.isEmpty(this.contact.lastname) &&
             angular.isString(this.contact.lastname) &&
             !_.isEmpty(this.contact.email) &&
             angular.isString(this.contact.email) &&
             !_.isEmpty(this.contact.phone_number) &&
             telephoneRegex.test(this.contact.phone_number);
    },
    isValid: function() {
      var validLocations = _.reduce(this.locations, function (memo, loc) {
        return memo && loc.isValid();
      }, true);
      return angular.isNumber(this.tax_fee) &&
             this.locations.length >= 1 &&
             this.uniqueLocationNames() &&
             this.tax_fee >= 0 && 
             this.tax_fee <= 100 &&
             !_.isEmpty(this.slug) &&             
             slugRegex.test(this.slug) &&
             !_.isEmpty(this.service_agreement) &&
             angular.isString(this.service_agreement) &&
             !_.isEmpty(this.name) &&
             angular.isString(this.name) && 
             this.validContactInfo() &&
             validLocations;
    },
    serialize: function() {
      var self = this;
      return {
        _id: self._id,
        service_agreement: self.service_agreement,
        tax_fee: self.tax_fee,
        slug: self.slug,
        website: self.website,
        name: self.name,
        twilio_number: self.twilio_number,
        contact: self.contact,
        active: self.active,
        locations: _.map(self.locations, function (loc) {
          return loc.serialize();
        })
      };
    },
    $predictAddress: function(address) {
      return getAddress(address);
    },
    onSelectAddress: function (address, locationId) {
      angular.extend(this.address, address);
    },
    $save: function() {
      var self = this;
      if(self.isNew()) {
        return StoreService.post.call(self.serialize()).then(function (store) {
          self._id = store._id;
          notifyService("Successfully created new store.", true);
          return store;
        }, function (err) {
          notifyService("Could not create new store, please try again");
          return err;
        });
      } else {
        return StoreService.put.call(self.serialize()).then(function (store) {
          notifyService("Successfully updated store information.", true);
          return store;
        }, function (err) {
          notifyService("Could not update store information, please try again");
          return err;
        });
      }
    }
  }

  Store.build = function (options) {
    return new Store(deserialize(options));
  }

  return Store;

}]);
