angular.module('service.hasher',[])
.factory('hasher', [function () {
  function hashCode(str){
    var hash = 0, i, chac;
    if (str.length === 0) { return hash; }
    for (i = 0; i < str.length; i++) {
      cha = str.charCodeAt(i);
      hash = ((hash<<5)-hash)+cha;
      hash = hash & hash;
    }
    return hash;
  }

  return function() {
    return function hasher() {
      return hashCode(JSON.stringify(_.toArray(arguments)));
    };
  };
}]);
