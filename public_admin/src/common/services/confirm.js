angular.module('service.confirm', [
  'ui.bootstrap.modal',
  'ui.keypress',
  'hdirectives'
])
.service('ConfirmService', [ '$modal','$q', function ($modal, $q) {

  return function (header) {
    var modalInstance = $modal.open({
      templateUrl: 'services/templates/confirm.html',
      controller: 'ConfirmServiceCtrl',
      resolve: {
        header: [function() {
          return header;
        }]
      }
    });
    var defer = $q.defer();
    modalInstance.result.then(function (response) {
      defer.resolve();
    }, function() {
      defer.reject();
    });
    return defer.promise;
  };

}])

.controller('ConfirmServiceCtrl', ['$scope', '$modalInstance','header', function ($scope,$modalInstance,header) {

  $scope.header = header;

  $scope.submit = function submit() {
    $modalInstance.close();
  };

  $scope.cancel = function cancel() {
    $modalInstance.dismiss('cancel');
  };


}])
;