/* Directive to capitalize first letter of input */
angular.module('hdirectives',[
  'ui.bootstrap',
  'custom-templates'
])
.directive('capitalizeFirst', [function() {
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if(inputValue) {
             var capitalized = inputValue.charAt(0).toUpperCase() + inputValue.substring(1);
             if(capitalized !== inputValue) {
                modelCtrl.$setViewValue(capitalized);
                modelCtrl.$render();
              }         
              return capitalized;
            }
         };
         modelCtrl.$parsers.push(capitalize);
         capitalize(scope[attrs.ngModel]);  // capitalize initial value
     }
   };
}])
.directive('ngEnter', [function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
}])
.directive('dropdownMultiselect', [function(){
   return {
       restrict: 'E',
       scope:{           
            model: '=ngModel',
            options: '=',
            pre_selected: '=preSelected',
            id:'@',
            text: '@',
            icon: '@',
            size: '@' // bs3 button classes
       },
       templateUrl: 'helperDirectives/templates/dropdown-tpls/dropdown.html', 
       link: function($scope){

            $scope.o = false;
            $scope.size = $scope.size || 'xs';

            $scope.openDropdown = function openDropdown(){
              $scope.o = !$scope.o;
              $scope.selected_items = [];
              for(var i=0; i<$scope.pre_selected.length; i++){
                $scope.selected_items.push($scope.pre_selected[i].id);
              }
            };
           
            $scope.selectAll = function selectAll() {
              $scope.model = _.pluck($scope.options, 'id');
            };

            $scope.setSelectedItem = function setSelectedItem(){
              var id = this.option.id;
              if (_.contains($scope.model, id)) {
                $scope.model = _.without($scope.model, id);
              } else {
                $scope.model.push(id);
              }
              return false;
            };

            $scope.isChecked = function isChecked(id) {                 
              return _.contains($scope.model, id);
            };                                 
       }
   };
}])
.directive('dropdownSingleselect', [function() {
   return {
       restrict: 'E',
       scope:{           
            model: '=ngModel',
            options: '=',
            text: '@',
            icon: '@',
            size: '@'
       },
       templateUrl: 'helperDirectives/templates/dropdown-tpls/dropdownsingle.html', 
       link: function($scope){

            $scope.label = function label() {
              var x = _.findWhere($scope.options, {id:$scope.model});
              if(angular.isDefined(x)) {
                return x.name
              } else if(angular.isDefined($scope.text)) {
                return $scope.text;
              } else if(angular.isDefined($scope.options)) {
                return $scope.options[0].name;
              } else {
                return '';
              }
            }

            $scope.size = $scope.size || 'xs';
            $scope.o = false;

            $scope.openDropdown = function openDropdown() {
              $scope.o = !$scope.o;
            };
           
            $scope.setSelectedItem = function setSelectedItem(option) {
              if(!angular.equals(_.findWhere($scope.options, {id:option.id}).name,$scope.text)) {
                $scope.text = option.name;
              }
              $scope.model = option.id;
            };

            $scope.isChecked = function isChecked(option) {
              return $scope.model === option;
            };
       }
   };
}])
.directive('money', [function () {
  'use strict';

  var NUMBER_REGEXP = /^\s*(\-|\+)?(\d+|(\d*(\.\d*)))\s*$/;
  function isUndefined(value) {
    return typeof value == 'undefined';
  }
  function isEmpty(value) {
    return isUndefined(value) || value === '' || value === null || value !== value;
  }

  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, el, attr, ctrl) {
      function round(num) { 
        return Math.round(num * 100) / 100;
      }

      var min = parseFloat(attr.min) || -1;

      // Returning NaN so that the formatter won't render invalid chars
      ctrl.$parsers.push(function(value) {
        // Handle leading decimal point, like ".5"
        if (value === '.') {
          ctrl.$setValidity('number', true);
          return 0;
        }

        // Allow "-" inputs only when min < 0
        if (value === '-') {
          ctrl.$setValidity('number', false);
          return (min < 0) ? -0 : NaN;
        }

        var empty = isEmpty(value);
        if (empty || NUMBER_REGEXP.test(value)) {
          ctrl.$setValidity('number', true);
          return value === '' ? null : (empty ? value : parseFloat(value));
        } else {
          ctrl.$setValidity('number', false);
          return NaN;
        }
      });
      ctrl.$formatters.push(function(value) {
        return isEmpty(value) ? '' : '' + value;
      });

      var minValidator = function(value) {
        if (!isEmpty(value) && value < min) {
          ctrl.$setValidity('min', false);
          return undefined;
        } else {
          ctrl.$setValidity('min', true);
          return value;
        }
      };
      ctrl.$parsers.push(minValidator);
      ctrl.$formatters.push(minValidator);

      if (attr.max) {
        var max = parseFloat(attr.max);
        var maxValidator = function(value) {
          if (!isEmpty(value) && value > max) {
            ctrl.$setValidity('max', false);
            return undefined;
          } else {
            ctrl.$setValidity('max', true);
            return value;
          }
        };

        ctrl.$parsers.push(maxValidator);
        ctrl.$formatters.push(maxValidator);
      }

      // Round off to 2 decimal places
      ctrl.$parsers.push(function (value) {
        return value ? round(value) : value;
      });
      ctrl.$formatters.push(function (value) {
        return value ? parseFloat(value).toFixed(2) : value;
      });

      el.bind('blur', function () {
        var value = ctrl.$modelValue;
        if (value) {
          ctrl.$viewValue = round(value).toFixed(2);
          ctrl.$render();
        }
      });
    }
  };
}])
.directive('ngFocus', [function () {
  var FOCUS_CLASS = "ng-focused";
  return {
    restrict: 'A',
    require: "ngModel",
    link: function (scope, iElement, iAttrs, ctrl) {
      ctrl.$focused = false;
      iElement.bind('focus',function(evt){
        iElement.addClass(FOCUS_CLASS);
        scope.$apply(function(){
          ctrl.$focused = true;
        });
      }).bind('blur',function(evt){
        iElement.removeClass(FOCUS_CLASS);
        scope.$apply(function(){
          ctrl.$focused = false;
        });
      });
    }
  };
}])
.directive('aDisabled', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        priority: -99999,
        link: function (scope, element, attrs) {
          var oldNgClick = attrs.ngClick;
          if (oldNgClick) {
            scope.$watch(attrs.aDisabled, function (val, oldval) {
              if ( !! val) {
                  element.unbind('click');
              } else if (oldval) {
                  attrs.$set('ngClick', oldNgClick);
                  element.bind('click', function () {
                      scope.$apply(attrs.ngClick);
                  });
              }
            });
          }
        }
    };
}])
.directive('focusMe', ['$timeout', '$parse', function ($timeout, $parse) {
  return {
    //scope: true,   // optionally create a child scope
    link: function(scope, element, attrs) {
      var model = $parse(attrs.focusMe);
      scope.$watch(model, function(value) {
        if(value === true) { 
          $timeout(function() {
            element[0].focus(); 
          });
        }
      });
      // to address @blesh's comment, set attribute value to 'false'
      // on blur event:
      element.bind('blur', function() {
         scope.$apply(model.assign(scope, false));
      });
    }
  };
}])
.directive('dateRangePicker', [ '$filter','STORE', function ($filter, STORE) {
  return {
      templateUrl: 'helperDirectives/templates/date-tpls/daterangepicker.html',
      restrict:'E',
      replace: true,
      scope: {
        dt:'=',
        spin:'&',
        onDownloadCb:'&'
      },
      link: function($scope) {
        // $scope.dt.pendingRequest = $scope.dt.pendingRequest || true;
        $scope.open_end_date = false;
        $scope.open_start_date = false;
        
        var initDate = {};

        function shortDate(date) {
          return $filter('date')(date,'dd-MMM-yyyy');
        }

        $scope.$watch('dt.endDate', function (nV,oV) {
          if(angular.equals(nV, oV)) {
            initDate.endDate = angular.copy(moment($scope.dt.endDate).startOf('day'));
            initDate.startDate = angular.copy(moment($scope.dt.startDate).startOf('day'));            
            $scope.dt.endDate = shortDate(moment($scope.dt.endDate).startOf('day').valueOf());
            $scope.dt.startDate = shortDate(moment($scope.dt.startDate).startOf('day').valueOf());
          }
          // $scope.maxDate = moment(nV).subtract('days',1);
          if(moment(nV).isBefore($scope.dt.startDate) || moment(nV).isSame($scope.dt.startDate)) {
            $scope.dt.startDate = shortDate(moment(nV).subtract(1,'days').valueOf());
          }
        });

        $scope.$watch('dt.startDate', function (nV, oV) {
          if(!angular.equals(nV,oV) && (moment(nV).isAfter($scope.dt.endDate) || moment(nV).isSame($scope.dt.endDate))) {
            $scope.dt.endDate = shortDate(moment(nV).add(1,'days').valueOf());
          }          
        });

        $scope.executeCb = function executeCb(dt) {
          $scope.onDownloadCb({nV:dt});
          initDate.endDate = angular.copy(moment(dt.endDate));
          initDate.startDate = angular.copy(moment(dt.startDate));
        };

        $scope.isChanged = function isChanged(nV) {
          return initDate && (!moment(nV.startDate).isSame(initDate.startDate) || !moment(nV.endDate).isSame(initDate.endDate));
        }

        $scope.today = shortDate(moment().add(1,'days').valueOf());
        $scope.maxDate = shortDate(moment($scope.today).subtract(1,'days').valueOf());
        $scope.minDate = shortDate(moment(STORE.created_date).format());
        $scope.minDate_two = shortDate(moment(STORE.created_date).add(1,'days').format());

      }
  };
}])
.directive('stopEvent', [function () {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      element.bind(attr.stopEvent, function (e) {
        e.stopPropagation();
      });
    }
  };
}])
.directive('focusThis', ['$timeout', function ($timeout) {
  return {
    scope: { trigger: '@focusMe' },
    link: function(scope, element) {
      scope.$watch('trigger', function(value) {
        if(value === "true") { 
          $timeout(function() {
            element[0].focus(); 
          });
        }
      });
    }
  };
}])
.directive('datepickerInput', ['$filter', function ($filter) {
  return {
      templateUrl: 'helperDirectives/templates/invoice-tpls/datepickerinput.html',
      restrict:'E',
      replace:true,
      scope: {
        date:"=ngModel"
      },
      link: function($scope) {
        $scope.$watch('date', function (nV, oV) {
          if(nV === oV) {
            $scope.date = new Date(nV);
          }
        });
      }
  };
}])
.directive( 'editInPlace', ['$window','$compile', function ($window,$compile) {
  return {
    restrict: 'A',
    scope: { 
      value: '=',
      inputPlaceholder: '@',
      preventClick:"="
    },
    link: function ( $scope, element, attrs ) {
      var templateStr = '<input class="form-control edit-inline" ng-model="value" placeholder="{{inputPlaceholder}}" edit-inline></input>';
      var inputElement = angular.element(templateStr);
      var initialValue;
      $scope.preventClick = $scope.preventClick || false;
      $compile(inputElement)($scope);
      // var elemStyle = $window.document.defaultView.getComputedStyle(element.children()[1], '');
      if(parseInt(element.css('font-size'),10) >= 14*1.25) {
        inputElement.addClass('input-lg');
      } else if(parseInt(element.css('font-size'),10) <= 14*0.85) {
        inputElement.addClass('input-sm');
      }
      inputElement.css({
        'display':'none',
        'font-size': element.css('font-size'),
        'font-family': element.css('font-family'),
        'font-weight': element.css('font-weight'),
        'line-height': element.css('line-height'),
        'margin': element.css('margin')
      });
      inputElement.keyup(function(e) {
        if(angular.isDefined(e) && e.keyCode == 13) {
          inputElement.css('display','none');
          element.css('display','inline-block');
          element.focus();
          initialValue = inputElement.val();
        } else if(angular.isDefined(e) && e.keyCode == 27 && inputElement.val() != '') {
          if(initialValue != '') {
            inputElement.css('display','none');
            element.css('display','inline-block');
            $scope.$apply(function() {
              $scope.value = initialValue;
            });
            inputElement.focus();
          } else {
            $scope.$apply(function () {
              $scope.value = '';
            });
          }
        } else if(angular.isDefined(e) && e.keyCode == 27 && inputElement.val() == '' ) {
          inputElement.css('display','none');
          element.css('display','inline-block');
          element.css('opacity',0.65);
          element.text($scope.inputPlaceholder.replace(/\D/g, "_"));
        }
      });
      inputElement.blur(function () {
        if(!_.isEmpty(inputElement.val()) && (initialValue !== inputElement.val())) {
          inputElement.css('display','none');
          element.css('display','inline-block');
          element.focus();
          initialValue = inputElement.val();
        } else if(!_.isEmpty(inputElement.val()) && (initialValue === inputElement.val())) {
          inputElement.css('display','none');
          element.css('display','inline-block');
          element.focus();
        }
      });
      element.after(inputElement);
      element.css({
        'padding':6.5+'px',
        'cursor':'pointer',
        'display':'inline-block',
        'opacity':1
      });
      /* Show the placeholder if there is no initial value */
      $scope.$watch('value', function (nV, oV) {
        if(angular.equals(nV,oV)) {
          initialValue = angular.copy(nV || '');
          if(nV == '' || angular.isUndefined(nV)) {
            element.text($scope.inputPlaceholder.replace(/\D/g, "_"));
            element.css('opacity',0.65);
            // element.css('display','none');
            // inputElement.css('display','inline-block');
          }
        }
      });
      inputElement.click(function (event) {
        event.stopPropagation();
      });
      element.click(function (event) {
        event.stopPropagation();
        if(!$scope.preventClick) {
          element.css('display','none');
          inputElement.css('display','inline-block');
          inputElement.focus();
        }
      });
    }
  };
}])
//http://jsfiddle.net/NDFHg/382/
.directive("editInline", ['$window', function editInline( $window ){
  return {
    require: 'ngModel',
    link: function(scope, element, attr, ngModel){
        scope.$watch(attr.ngModel,function(nv,ov){
          var tester = angular.element('<span>');
          var elemStyle = $window.document.defaultView.getComputedStyle(element[0], '');
          tester.css({
            'font-family': elemStyle.fontFamily,
            'line-height': elemStyle.lineHeight,
            'font-size': elemStyle.fontSize,
            'font-weight': elemStyle.fontWeight
          });
          tester.text(nv||element.attr('placeholder'));
          element.parent().append(tester);
          var r = tester[0].getBoundingClientRect();
          var w = r.width+parseInt(elemStyle.borderLeftWidth,10)+parseInt(elemStyle.borderRightWidth,10)+parseInt(elemStyle.paddingLeft,10)+parseInt(elemStyle.paddingRight,10);
          element.css('width',w+'px');            
          tester.remove();
        });
    }
  };
}])

