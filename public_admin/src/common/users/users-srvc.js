angular.module('resource.users', [
  'constants',
  'service.xhr'
])
.factory('UsersService', ['xhr', 'apiPrefix', function (xhr, apiPrefix) {
  return {
    get: function(query) {
      var x;
      var id;
      if(angular.isObject(query)) {
        x = angular.extend({per_page:'0',sort_by:'created_date'},query);
        id = '';
      } else if (angular.isString(query)) {
        x = {};
        id = '/'+query;
      }
      return xhr([apiPrefix+'/admin/users'+id,{params:x, cache: false}]);
    },
    post: function () {
      return xhr('post',[apiPrefix+'/admin/users',this]);
    },
    delete: function () {
      return xhr('delete',[apiPrefix+'/admin/users/'+this._id]);
    },
    put: function () {
      return xhr('put',[apiPrefix+'/admin/users/'+this._id, this]);
    }
  };
}]);
