angular.module('user.srvcs',[
  'constants',
  'resource.users',
  'store.srvcs',
  'notify'
])
.factory('User', [ '$q', 'Store', 'UsersService','notifyService', function ($q, Store, UsersService, notifyService) {

  function User(options) {
    angular.extend(this, options);
    angular.extend(this.store, options.store);
  }

  function deserialize(user) {
    user.changepassword = user.changepassword || false;
    return user;
  }

  function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
  }

  User.prototype = {

    serialize: function(options) {
      var serializeUser = {
        password: ((!this.changepassword) ? '' : this.password),
        firstname: this.firstname,
        lastname: this.lastname,
        username: this.username,
        email: this.email,
        admin: this.admin,
        _id: this._id,
        store: this.store.serialize()
      }
      return serializeUser;
    },

    hasNewThumbnail: function() {
      return angular.isDefined(this.thumbnail) && (this.thumbnail.indexOf('base64') > -1);
    },

    // $saveThumbnail: function() {
    //   var self = this;
    //   return UsersService.upload.call(self.serialize(), dataURItoBlob(self.thumbnail)).then(function (response) {
    //     var random = (new Date()).toString();
    //     self.thumbnail = response.data + "?cb=" + random;
    //   }, null, angular.noop);
    // },

    $save: function() {
      var self = this;
      if(self.isExistingUser()) {
        var promises = [UsersService.put.call(this.serialize())];
        if(self.hasNewThumbnail()) {
          promises.push(self.$saveThumbnail());
        }
        return $q.all(promises).then(function (user) {
          notifyService(self.username + " was succesfully updated.", true);
          return user[0];
        }, function (err) {
          notifyService("There was an error updating user, please try again.", false);
          return err;
        });
      } else {
        return UsersService.post.call(this.serialize()).then(function (user) {
          self._id = user._id;
          notifyService("Successfully created " + self.username, true);
          if(self.hasNewThumbnail()) {
            return self.$saveThumbnail().then(function () {
              return user;
            });
          } else {
            return user;
          }
        }, function (err) {
          notifyService("Could not create " + self.username + ", please try again.");
          return err;
        }); 
      }
    },

    $remove: function () {
      var self = this;
      return UsersService['delete'].call(self.serialize()).then(function (user) {
        notifyService("Successfully removed " + self.username, true);
        return user;
      }, function (err) {
        notifyService("Could not remove " + self.username+", please try again.");
        return err;
      });
    },

    isExistingUser: function() {
      return angular.isDefined(this._id);
    },

    isNewUser: function() {
      return angular.isUndefined(this._id);
    }

  };

  User.build = function (options) {
    options.store = Store.build(options.store);
    return new User(deserialize(options));
  }

  return User;

}]);
