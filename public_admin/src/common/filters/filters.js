angular.module('filters', [
  'service.hasher'
])
.filter('tel',[function() {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            case 11: // +CPPP####### -> CCC (PP) ###-####
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;

            case 12: // +CCCPP####### -> CCC (PP) ###-####
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;

            default:
                return tel;
        }

        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (country + " (" + city + ") " + number).trim();
    };
}])
.filter('address', [function() {
    return function (address) {
        if(angular.isDefined(address)) {
          address = address.split(',');
          if(address.length === 4) {
            return (address[0]||'') + '\n' + (address[1]||'') + (address[2]||'') + '\n' + (address[3]||'');
          } else if(address.length === 5) {
            return (address[0]||'') + '\n' + (address[1]||'') + '\n' + (address[2]||'') + (address[3]||'') + '\n' + (address[4]||'');
          }
        } else {
          return '';
        }
    };
}])
.filter('reverse', [function() {
  return function (invoices) {
    return invoices.slice().reverse();
  };
}])
.filter('capitalizeFirst', [function () {
    "use strict";
    return function (input) {
        if(input) {
          return input.charAt(0).toUpperCase() + input.slice(1).toLowerCase(); 
        } else {
          return '';
        }
    };
}])
.filter('percentage', ['$window', function ($window) {
    return function (input, decimals, suffix) {
        decimals = decimals || 3;
        suffix = suffix || '%';
        if ($window.isNaN(input)) {
            return '';
        }
        return Math.round(input * Math.pow(10, decimals + 2))/Math.pow(10, decimals) + suffix;
    };
}])
.filter('noFractionCurrency', [ '$filter', '$locale', function (filter, locale) {
  var currencyFilter = filter('currency');
  var formats = locale.NUMBER_FORMATS;
  return function(amount, currencySymbol) {
    var value = currencyFilter(amount, currencySymbol);
    var sep = value.indexOf(formats.DECIMAL_SEP);
    if(amount >= 0) { 
      return value.substring(0, sep);
    }
    return value.substring(0, sep) + ')';
  };
}])
.filter('searchField', ['$parse', function ($parse) {
  function toArray(object) {
    return angular.isArray(object) ? object :
      Object.keys(object).map(function (key) {
        return object[key];
      });
  }
  return function (collection) {
    var get, field;
    collection = (angular.isObject(collection)) ? toArray(collection) : collection;
    var args;
    if(angular.isArray(arguments[1])) {
      args = arguments[1];
    } else {
      args = Array.prototype.slice.call(arguments, 1)
    }
    if(!angular.isArray(collection) || !args.length) {
      return collection;
    }
    return collection.map(function (member) {
      field = args.map(function (field) {
        get = $parse(field);
        return get(member);
      }).join(' ');
      //TODO(ariel):we don't wanna change the source, but I'm not sure about the performance.
      return angular.extend(member, { searchField: field });
    });
  }
}])
;

