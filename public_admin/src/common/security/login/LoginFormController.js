angular.module('security.login.form', ['ui.bootstrap.buttons'])
// 'services.localizedMessages'
// The LoginFormController provides the behaviour behind a reusable form to allow users to authenticate.
// This controller and its template (login/form.tpl.html) are used in a modal dialog box by the security service.
// , 'localizedMessages'
.controller('LoginFormController', ['$scope', 'security', function ($scope, security) {
  // The model for this form 
  $scope.user = {};

  // Any error message from failing to login
  $scope.authError = null;

  // The reason that we are being asked to login - for instance because we tried to access something to which we are not authorized
  // We could do something diffent for each reason here but to keep it simple...
  $scope.authReason = null;
  if ( security.getLoginReason() ) {
    $scope.authReason = ( security.isAuthenticated() ) ?
      "Not authorized" : "Not authenticated";
  }

  // Attempt to authenticate the user specified in the form's model
  $scope.login = function login(user) {
    // Clear any previous security errors
    $scope.authError = null;

    // Try to login
    security.login(user.username, user.password, user.rememberme).then(function (loggedIn) {
      if ( !loggedIn ) {
        // If we get here then the login failed due to bad credentials
        $scope.authError = "Invalid Credentials"; // 'localizedMessages'
      }
    }, function (x) {
      // If we get here then there was a problem with the login request to the server
      $scope.authError = "Error from server"; // 'localizedMessages'
    });
  };

  $scope.clearForm = function() {

  };

  $scope.cancelLogin = function cancelLogin() {
    security.cancelLogin();
  };
}]);