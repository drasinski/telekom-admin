angular.module('notify', [
  'cgNotify'
])
.factory('notifyService', ['notify', function (notify) {

  notify.config({
    position:'right',
    templateUrl: 'notify/notify-template.html',
    duration: 2250,
    startTop: 85
  });

  // success: true/false
  return function (message, success) {
    notify({
      // position:'right',
      message: message,
      classes: success ? 'alert-success' : 'alert-danger'
    });
  }

}]);