angular.module('constants', [])
.constant('apiPrefix','api/v1/')
.constant('defaultTypes', 
[
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "system": "E Pay",
    "system_slug": "e-pay",
    "description": "AT&T",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 3,
        "amount": 30
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 15
      }
    ],
    "active": true
  },
  {
    "system_slug": "e-pay",
    "category_slug": "activation",
    "category": "Activation",
    "description": "AT@T",
    "icon": "fa-signal",
    "system": "E Pay",
    "store": {
    },
    "prediction": [],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "system": "E Pay",
    "system_slug": "e-pay",
    "description": "Boost Mobile",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 3,
        "amount": 30
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 0
      }
    ],
    "active": true
  },
  {
    "category_slug": "international",
    "category": "International",
    "system": "Boss Revolution",
    "system_slug": "boss-revolution",
    "description": "Boss Revolution",
    "icon": "fa-globe",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 10
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 20
      }
    ],
    "active": true
  },
  {
    "system_slug": "services",
    "category_slug": "services",
    "category": "Services",
    "icon": "fa-wrench",
    "system": "Services",
    "description": "Computer Repair",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 50
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "system_slug": "e-pay",
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "icon": "fa-refresh",
    "system": "E Pay",
    "description": "Expo Mobile",
    "store": {
    },
    "prediction": [],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "system": "E Pay",
    "system_slug": "e-pay",
    "description": "GO Smart",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 4,
        "amount": 35
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5,
        "amount": 40
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 3,
        "amount": 25
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 5
      }
    ],
    "active": true
  },
  {
    "system_slug": "e-pay",
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "system": "E Pay",
    "description": "Go  GO Mobile",
    "store": {
    },
    "prediction": [],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 50,
        "amount_comm": 10
      }
    ],
    "active": true
  },
  {
    "system_slug": "services",
    "category_slug": "services",
    "category": "Services",
    "description": "Internet",
    "icon": "fa-wrench",
    "system": "Services",
    "store": {
    },
    "prediction": [],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "activation",
    "category": "Activation",
    "system": "Activations",
    "system_slug": "activations",
    "description": "Lyca Mobile",
    "icon": "fa-signal",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 35
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "system": "E Pay",
    "system_slug": "e-pay",
    "description": "Lyca Mobile",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 4,
        "amount": 23
      },
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 4,
        "amount": 29
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5,
        "amount": 35
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5,
        "amount": 39
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 6,
        "amount": 45
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 6,
        "amount": 49
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 7,
        "amount": 55
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 7,
        "amount": 59
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 8
      }
    ],
    "active": true
  },
  {
    "system_slug": "e-pay",
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "description": "Metro PCS",
    "icon": "fa-refresh",
    "system": "E Pay",
    "store": {
    },
    "prediction": [],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "international",
    "category": "International",
    "description": "Net10",
    "system": "E Pay",
    "system_slug": "e-pay",
    "icon": "fa-globe",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 10
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 25
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "description": "Net10",
    "system": "E Pay",
    "system_slug": "e-pay",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 4,
        "amount": 35
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 4,
        "amount": 40
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5,
        "amount": 45
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5,
        "amount": 50
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 6,
        "amount": 60
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 8,
        "amount": 75
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 6
      }
    ],
    "active": true
  },
  {
    "system_slug": "services",
    "category_slug": "services",
    "category": "Services",
    "icon": "fa-wrench",
    "system": "Services",
    "description": "Phone Repair",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 100
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "system_slug": "services",
    "category_slug": "services",
    "category": "Services",
    "icon": "fa-wrench",
    "system": "Services",
    "description": "Phone Unlock",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 100
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "description": "Page Plus",
    "system": "E Pay",
    "system_slug": "e-pay",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 4.05,
        "amount": 29.95
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5.05,
        "amount": 39.95
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 6,
        "amount": 55
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 7.05,
        "amount": 69.95
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 10
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "description": "Red Pocket",
    "system": "E Pay",
    "system_slug": "e-pay",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 4.01,
        "amount": 29.99
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5.01,
        "amount": 39.99
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 6.01,
        "amount": 49.99
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 7.01,
        "amount": 59.99
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 5
      }
    ],
    "active": true
  },
  {
    "system_slug": "services",
    "category_slug": "services",
    "category": "Services",
    "description": "Scan and print",
    "icon": "fa-wrench",
    "system": "Services",
    "store": {
    },
    "prediction": [],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "international",
    "category": "International",
    "system": "E Pay",
    "system_slug": "e-pay",
    "description": "Simple Mobile",
    "icon": "fa-globe",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 10
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 9.5
      }
    ],
    "active": true
  },
  {
    "category_slug": "activation",
    "category": "Activation",
    "description": "Simple Mobile",
    "system": "Activations",
    "system_slug": "activations",
    "icon": "fa-signal",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 35
      }
    ],
    "profit": [
      {
        "fixed_cost": 0.5,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "description": "Simple Mobile",
    "system": "E Pay",
    "system_slug": "e-pay",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 3,
        "amount": 25
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 4,
        "amount": 40
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5,
        "amount": 50
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 6,
        "amount": 60
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 7,
        "amount": 70
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 1,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 8.25
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "system": "E Pay",
    "system_slug": "e-pay",
    "description": "Spot Mobile",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 4,
        "amount": 30
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 6
      }
    ],
    "active": true
  },
  {
    "system_slug": "e-pay",
    "category_slug": "international",
    "category": "International",
    "description": "T Mobile",
    "icon": "fa-globe",
    "system": "E Pay",
    "store": {
    },
    "prediction": [],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "activation",
    "category": "Activation",
    "system": "Activations",
    "system_slug": "activations",
    "description": "T-Mobile",
    "icon": "fa-signal",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 35
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 1,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "description": "T-Mobile",
    "system": "E Pay",
    "system_slug": "e-pay",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 5,
        "amount": 50
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 6,
        "amount": 60
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 7,
        "amount": 70
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 8,
        "amount": 80
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 3,
        "amount": 30
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 7.25
      }
    ],
    "active": true
  },
  {
    "system_slug": "services",
    "category_slug": "services",
    "category": "Services",
    "description": "TIP",
    "icon": "fa-wrench",
    "system": "Services",
    "store": {
    },
    "prediction": [],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "system": "E Pay",
    "system_slug": "e-pay",
    "description": "Tracfone",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 4,
        "amount": 30
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 5
      }
    ],
    "active": true
  },
  {
    "system_slug": "services",
    "category_slug": "services",
    "category": "Services",
    "description": "Translations",
    "icon": "fa-wrench",
    "system": "Services",
    "store": {
    },
    "prediction": [],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "activation",
    "category": "Activation",
    "system": "Activations",
    "system_slug": "activations",
    "description": "Ultra Mobile",
    "icon": "fa-signal",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 35
      }
    ],
    "profit": [
      {
        "fixed_cost": 2.5,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "system": "E Pay",
    "system_slug": "e-pay",
    "description": "Ultra Mobile",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 4,
        "amount": 29
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5,
        "amount": 39
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 6,
        "amount": 49
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 7,
        "amount": 59
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 4,
        "amount": 19
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5,
        "amount": 44
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 4,
        "amount": 34
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 1,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 10
      }
    ],
    "active": true
  },
  {
    "category_slug": "international",
    "category": "International",
    "description": "Ultra Mobile Wallet",
    "system": "E Pay",
    "system_slug": "e-pay",
    "icon": "fa-globe",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 1,
        "amount": 10
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 1,
        "amount": 5
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 5
      }
    ],
    "active": true
  },
  {
    "system_slug": "e-pay",
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "description": "Univision",
    "icon": "fa-refresh",
    "system": "E Pay",
    "store": {
    },
    "prediction": [],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "system": "E Pay",
    "system_slug": "e-pay",
    "description": "Verizon Wireless",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 5,
        "amount": 45
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 6,
        "amount": 60
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 8,
        "amount": 75
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 50,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 0
      }
    ],
    "active": true
  },
  {
    "category_slug": "activation",
    "category": "Activation",
    "system": "Activations",
    "system_slug": "activations",
    "description": "h20 Wireless",
    "icon": "fa-signal",
    "store": {
    },
    "prediction": [
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 0,
        "amount": 35
      }
    ],
    "profit": [
      {
        "fixed_cost": 0.5,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": false,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 100
      }
    ],
    "active": true
  },
  {
    "category_slug": "phone-bill",
    "category": "Phone Bill",
    "description": "h20 Wireless",
    "system": "E Pay",
    "system_slug": "e-pay",
    "icon": "fa-refresh",
    "store": {
    },
    "prediction": [
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 4,
        "amount": 30
      },
      {
        "default_pred": true,
        "tax": false,
        "servicecharge": 5,
        "amount": 40
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 6,
        "amount": 50
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 7,
        "amount": 60
      },
      {
        "default_pred": false,
        "tax": false,
        "servicecharge": 5,
        "amount": 35
      }
    ],
    "profit": [
      {
        "fixed_cost": 0,
        "has_tax": false,
        "tax_fixed": 0,
        "tax_comm": 100,
        "has_servicecharge": true,
        "servicecharge_fixed": 0,
        "servicecharge_comm": 100,
        "has_amount": true,
        "amount_fixed": 0,
        "amount_comm": 8
      }
    ],
    "active": true
  }
])