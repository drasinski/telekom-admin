angular.module('resource.types', [
  'constants',
  'service.xhr'
])
.factory('TypeService', ['$q','xhr','apiPrefix', function ($q, xhr, apiPrefix) {


  return {
    get: function(storeId, query) {
      var x = angular.extend({per_page:'0',sort_by:'description'},query);
      return xhr([apiPrefix + "stores/" + storeId +'/types',{params:x, cache: false }]).then(function (data) {
        return data;
      });
    },
    post: function () {
      var self = this;
      return xhr('post',[apiPrefix + "stores/" + this.store.storeId + '/types', this]).then(function (resp) {
        return resp;
      });
    },
    delete: function () {
      var self = this;
      return xhr('delete',[apiPrefix + "stores/" + this.store.storeId + '/types/' + this._id]).then(function (resp) {
        return resp;
      });
    },
    put: function () {
      var self = this;
      return xhr('put',[apiPrefix + "stores/" + this.store.storeId + '/types/' + this._id, this]).then(function (resp) {
        return resp;
      });
    }
  }


}])