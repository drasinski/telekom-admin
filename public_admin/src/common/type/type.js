angular.module('type.srvcs',[
  'constants',
  'notify',
  'resource.types'
])
.factory('Type', ['notifyService','TypeService','$q', function (notifyService, TypeService, $q) {

  function Type(options) {
    angular.extend(this, options);
    this.oldType = angular.copy(this.serialize());
  }

  function deserialize(type) {
    if(angular.isDefined(type.last_modified_date)) {
      type.last_modified_date = new Date(type.last_modified_date)
    };
    if(angular.isDefined(type.created_date)) {
      type.created_date = new Date(type.created_date);
    } else {
      type.created_date = new Date();
    }
    if(angular.isUndefined(type._id) && (type.profit.length === 1)) {
      type.start_date = moment().format();
    }
    return type;
  }


  // Do we need a store.clear function here?
  Type.prototype = {
    isNew: function() {
      return angular.isUndefined(this._id);
    },
    hasChanged: function() {
      return !angular.equals(this.serialize(), this.oldType);
    },
    serialize: function() {
      var self = this;
      return {
         _id: self._id,
        category: self.category,
        active: self.active,
        description: self.description,
        icon: self.icon,
        system: self.system,
        profit: self.profit,
        prediction: self.prediction,
        store: {
         storeId: self.store.storeId
        }
      };
    },
    $save: function(store) {
      var self = this;
      if(self.isNew()) {
        self.store.storeId = store._id;
        return TypeService.post.call(self.serialize()).then(function (type) {
          type._id = type._id;
          notifyService("Successfully created " + self.description + ".", true);
          self.oldType = angular.copy(self.serialize());
          return store;
        }, function (err) {
          notifyService("Could not create " + self.description+".");
          return err;
        });
      } else if(!self.isNew() && self.hasChanged()) {
        return TypeService.put.call(self.serialize()).then(function (type) {
          notifyService("Successfully updated " + self.description, true);
          self.oldType = angular.copy(self.serialize());
          return type;
        }, function (err) {
          notifyService("Could not update " + self.description + " please try again.");
          return err;
        });
      } else {
        return $q.when(self);
      }
    }
  }

  Type.build = function (options) {
    return new Type(deserialize(options));
  }

  return Type;

}])
.factory('Types', [ 'Type','$q', function (Type, $q) {

  function Types(options) {
    this.types = options;
  }

  function convertToSlug(Text) {
    return Text.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
  }

  Types.prototype = {
    removeType: function (type) {
      var i = this.types.indexOf(type);
      if(i > -1) {
        this.types.splice(i, 1);
      }
    },
    addType: function (category, icon) {
      this.types.push(Type.build({
        category: category,
        category_slug: convertToSlug(category),
        active: true,
        description:'',
        icon: icon,
        system: '',
        profit:[{
          start_date: moment().format(),
          amount_comm:100,
          amount_fixed:0,
          has_amount:true,
          servicecharge_comm: 100,
          servicecharge_fixed: 0,
          has_servicecharge: false,
          tax_comm: 100,
          tax_fixed:0, 
          has_tax: false,
          fixed_cost: 0
        }],
        prediction:[],
        store:{}
      }));
    },
    $save: function(store) {
      var promises = _.map(this.types, function (type) {
        return type.$save(store);
      })
      return $q.all(promises).then(function (resp) {
        console.log(resp);
        return resp;
      }, function (err) {
        console.log(err);
        return err;
      });
    }
  }

  Types.build = function(types) {
    if(angular.isArray(types)) {
      return new Types(_.map(types, Type.build));
    }
  }

  return Types;

}])
;
