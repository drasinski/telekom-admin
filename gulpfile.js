'use strict';

var Dev = require('./gulpTasks/dev');
var public_store_dev = new Dev('public_store');
var public_admin_dev = new Dev('public_admin');

var Dist = require('./gulpTasks/dist');
var public_store_dist = new Dist('public_store');
var public_admin_dist = new Dist('public_admin');


// var public_store_dev = require('./gulpTasks/dev')('public_store');
// var public_store_dist = require('./gulpTasks/dist')('public_store');
// var public_admin_dist = require('./gulpTasks/dist')('public_admin');

var gulp = require('gulp'),
    runSequence = require('run-sequence');

gulp.task('compile:public_store', function (done) {
    runSequence(
        'dist:release:public_store',
        done
    );
});

gulp.task('compile:public_admin', function (done) {
    runSequence(
        'dist:release:public_admin',
        done
    );
});

gulp.task('compile', function (done) {
    runSequence(
        'compile:public_admin',
        'compile:public_store',
        // 'cordova:clean',
        // 'cordova:copy-source',
        // 'cordova:config-for-default',
        // 'cordova:build:all',
        // 'nwjs:clean',
        // 'nwjs:copy-source',
        // 'nwjs:build',
        done
    );
});

gulp.task('heroku:production', function (done) {
    runSequence(
        'compile',
        'dev:clean:public_store',
        'dev:clean:public_admin',
        done
    );
});


gulp.task('build:public_store', function (done) {
    runSequence(
        'dev:default:public_store',
        done
    );
});

gulp.task('build:public_admin', function (done) {
    runSequence(
        'dev:default:public_admin',
        done
    );
});

gulp.task('build', function (done) {
    runSequence(
      'build:public_store',
      'build:public_admin',
      done
    );
});

gulp.task('clean', function (done) {
    runSequence(
      'dev:clean:public_store',
      'dev:clean:public_admin',
      'dist:clean:public_store',
      'dist:clean:public_admin',
      done
    );
});

gulp.task('heroku:development', function (done) {
    runSequence(
        'build',
        // 'dev:default:public_store',
        // 'cordova:clean',
        // 'cordova:copy-source',
        // 'cordova:config-for-default',
        // 'cordova:build:all',
        // 'nwjs:clean',
        // 'nwjs:copy-source',
        // 'nwjs:build',
        done
    );
});

gulp.task('default', ['build']);

gulp.task('watch', ['dev:watch']);