angular.module('clientinfo.drtv', [
  'filters',
  'hdirectives'
])
.directive('clientInvoiceList', [function () {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'clientinfo/templates/clientinfo-invoice-list.html',
    link: function($scope, element, attrs) {

      $scope.deleteInvoice = function deleteInvoice(viewinvoice) {
        $scope.client.invoices = _.reject($scope.client.invoices, function (value){
          return value._id === viewinvoice;
        });
      };

    }
  };
}])
;