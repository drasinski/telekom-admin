angular.module( 'clientinfo', [
  'ui.router',
  'hdirectives',
  'security.authorization',
  'service.confirm',
  'invoice.srvcs',
  'invoice.directive',
  'resource.clients',
  'clients.srvcs',
  'clients.directive',
  'ui.bootstrap.collapse',
  'filters',
  'clientinfo.drtv'
  // 'ui.map'
])

.config([ '$stateProvider','securityAuthorizationProvider','invoiceInitProvider','clientInitProvider','STORE', function config( $stateProvider, securityAuthorizationProvider, invoiceInitProvider, clientInitProvider, STORE) {
  $stateProvider.state( 'clientinfo', {
    abstract: true,
    url: '/'+STORE.slug+'/clientinfo',
    views: {
      "main": {
        controller: 'ClientInfoCtrl',
        templateUrl: 'clientinfo/templates/clientinfo.html'
      }
    },
    reloadOnSearch: false,
    resolve: {
      currentUser: securityAuthorizationProvider.requireAuthenticatedUser,
      clients: ['ClientService', 'Client', function (ClientService, Client) {
        return ClientService.get().then(function (clients) {
          return _.map(clients, Client.build);
        });
      }]
    },
    data: {
      pageTitle: 'Client Info'
    }
  })
  .state('clientinfo.detail', {
    url: '/{id}',
    templateUrl: 'clientinfo/templates/clientinfo-detail.html',
    controller: 'ClientInfoDetailCtrl',
    reloadOnSearch: true,
    abstract:true,
    resolve: {
      client: ['$stateParams','ClientService','Client', function ($stateParams, ClientService, Client) {
        return ClientService.getClient($stateParams.id).then(Client.build);
      }]
    }
  })
  .state('clientinfo.detail.invoices', {
    url:'/invoices?page',
    templateUrl: 'clientinfo/templates/clientinfo-detail-invoices.html',
    resolve: {
      types: invoiceInitProvider.requireTypesInventory,
      client: ['client','Invoice','types','$stateParams','$q', function (client, Invoice, types, $stateParams, $q) {
        return $q.all([client.$getInvoices({per_page:10, page:$stateParams.page, sort_by:'created_date,desc'}), client.$getInvoicesCount()]).then(function (resp) {
          var data = resp[0];
          client.invoices = _.map(data, Invoice.build);
          client.invoice_page = $stateParams.page;
          return client;
        });
      }]
    }
  })
  .state('clientinfo.detail.info', {
    url:'/info',
    templateUrl: 'clientinfo/templates/clientinfo-detail-info.html',
    resolve: {
      carriers: clientInitProvider.requireCarriers
    }
  })
  ;
}])
.controller('ClientInfoCtrl', ['$scope','clients', '$filter', function ClientInfoCtrl($scope, clients, $filter) {
  $scope.clients = clients;

}])
.controller('ClientInfoDetailCtrl', ['$scope', 'client','ConfirmService', 'Invoice', function ClientInfoDetail($scope, client, ConfirmService) {

  $scope.client = client;
  
  $scope.saveClient = function saveClient(client) {
    ConfirmService('Save your changes to this client?').then(function () {
      client.$save();
    });
  };

  $scope.changeInvoicePage = function changeInvoicePage(client) {
    $scope.$state.go('clientinfo.detail.invoices', {page: client.invoice_page});
  };

  $scope.sendSMS = function sendSMS(client) {
    var x = {
      message: client.message_info,
      message_type: 'Manual',
      for_date: new Date()
    }
    client.$sendSMS(x);
  };

  $scope.delClient = function delClient(client) {
    ConfirmService('Are you sure you want to delete this client?').then(function (result) {
      client.$remove().then(function (id) {
        $scope.$parent.clients = _.reject($scope.$parent.clients, function (value) {
          return value._id === id;
        });
        $scope.$state.go('payments');
      });
    });
  };

  // $scope.mapOptions = {
  //   center: new google.maps.LatLng(35.784, -78.670),
  //   zoom:17,
  //   mapTypeId: google.maps.MapTypeId.ROADMAP
  // };

}]);
