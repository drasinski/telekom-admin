angular.module( 'cellbetter-store', [
  'templates',
  'angular-data.DSCacheFactory',
  'angulartics',
  'angulartics.google.analytics',
  'ngSanitize',
  'dailylog',
  'invoice',
  'clientinfo',
  'payments',
  'admin',
  'profile',
  'ui.router',
  'security',
  'clients.srvcs',
  'resource.clients',
  'invoice.srvcs',
  'ui.bootstrap',
  'ui.bootstrap.tpls',
  'chat',
  'hdirectives',
  'snap',
  'constants',
  'pageLoadSpinner',
  'service.printer'
])

.config(['$urlRouterProvider', '$locationProvider','snapRemoteProvider','STORE', '$analyticsProvider', function ($urlRouterProvider,$locationProvider,snapRemoteProvider, STORE, $analyticsProvider) {
  $analyticsProvider.virtualPageviews(false);
  snapRemoteProvider.globalOptions.disable = 'right';
  snapRemoteProvider.globalOptions.tapToClose = true;
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise(STORE.slug + '/invoice');
  // $urlRouterProvider.otherwise(STORE.slug + '/payments/'+moment().format('YYYY')+'/'+moment().format('MMM'));
}])
.run(['$rootScope', '$state', '$stateParams','$analytics','$location','$http','security','DSCacheFactory', 'ClientService', function ($rootScope, $state, $stateParams, $analytics, $location, $http, security, DSCacheFactory, ClientService) {
  $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, fromState, fromParams) {
    if(angular.isDefined(fromState.name)) {
      $state.previous = angular.copy(fromState.name);
    } else {
      $state.previous = 'payments';
    }
    var x = $location.path().split('/');
    $analytics.pageTrack('/'+x.slice(2).join('/'));
    $state.prevParams = angular.copy(fromParams);
  });
  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    console.log(error);
    console.log(error.stack);
  });
  ClientService.get();
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
  // Get the current user when the application) starts
  // (in case they are still logged in from a previous session) 
  // security.requestCurrentUser();
  FastClick.attach(document.body);
  new DSCacheFactory('defaultCache', {
    maxAge: 3600000, // Items added to this cache expire after 15 minutes.
    cacheFlushInterval: 6000000, // This cache will clear itself every hour.
    deleteOnExpire: 'aggressive' // Items will be deleted from this cache right when they expire.
  });
  $http.defaults.cache = DSCacheFactory.get('defaultCache');
}])
.controller('AppCtrl', ['$scope', 'security','pageLoad','snapRemote','clientsFilter','$timeout','printer','Store', function ($scope, security, pageLoad, snapRemote, clientsFilter, $timeout, printer, Store) {

  $scope.isAuthenticated = security.isAuthenticated;
  $scope.isAdmin = security.isAdmin;
  $scope.logout = security.logout;  
  $scope.login = security.showLogin;
  $scope.pageLoad = pageLoad;

  $scope.year = new Date().getFullYear();
  $scope.month = moment().format('MMM');
  $scope.today = new Date();

  $scope.snapToggle = function snapToggle(side) {
    snapRemote.toggle(side);
  };


  $scope.clientsFilter = clientsFilter;

  // Clear the search bar if there is an error and we succesfully change state
  $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams){
    if($scope.telephoneSearchError) {
      $scope.telephoneSearch = '';
      $scope.telephoneSearchError = false;
    }
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | Cell Better' ;
    }
  });

  $scope.$watch(function() {
    return security.currentUser;
  }, function (currentUser) {
    $scope.currentUser = currentUser;
  });

  $scope.printCCForm = function printCCForm() {
    printer.print("ccform/ccform-print.html", {store:Store.build({locationId:$scope.currentUser.store.locationId})});
  };


  $scope.searchPhone = function searchPhone(item) {
    $scope.$state.go('clientinfo.detail.info', { id: item._id}).then(function (success) {
      $scope.telephoneSearch = '';
      $scope.telephoneSearchError = false;
      $scope.telephoneSuccess = true;
      $timeout(function() {
        $scope.telephoneSuccess = false;
      },1500)
    }, function() {
      $scope.telephoneSearchError = true;
      console.log("Invalid phone Number");
    });
  };

  snapRemote.getSnapper().then(function (snapper) {
    snapper.on('open', function() {
      $scope.snapActive = true;
    });
    
    snapper.on('close', function() {
      $scope.snapActive = false;
    });
  });
  
}]);

