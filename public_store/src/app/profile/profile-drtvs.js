angular.module('profile.drtv', [
  'resource.users'
])

.directive('nameUnique', ['UsersService','$timeout', function (UsersService, $timeout) {
 return {
    restrict: 'A',
    require: 'ngModel',

    link: function(scope, element, attrs, ngModel) {
       var stop_timeout;
       var bool;
       return scope.$watch(function() {
          return ngModel.$modelValue;
       }, function(name) {
          $timeout.cancel(stop_timeout);
          if (name === '') {
             ngModel.$setValidity('unique', true);
          }
          stop_timeout = $timeout(function() {
             UsersService.get({
                username: name
             }).then(function (models) {
                // if(models.length === 1) {
                //   bool = (models[0]._id === scope.init) && (models[0].username === name);
                // } else if( models.length === 0) {
                //   bool = true;
                // } else {
                //   bool = false;
                // }
                return ngModel.$setValidity('unique', models.length === 0  );
             });
          }, 200);
       });
       }
    };
}])
/**
 * A validation directive to ensure that this model has the same value as some other
 */
.directive('validateEquals', [function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {

      function validateEqual(myValue, otherValue) {
        if (myValue === otherValue) {
          ctrl.$setValidity('equal', true);
          return myValue;
        } else {
          ctrl.$setValidity('equal', false);
          return undefined;
        }
      }

      scope.$watch(attrs.validateEquals, function(otherModelValue) {
        ctrl.$setValidity('equal', ctrl.$viewValue === otherModelValue);
      });

      ctrl.$parsers.push(function(viewValue) {
        return validateEqual(viewValue, scope.$eval(attrs.validateEquals));
      });

      ctrl.$formatters.push(function(modelValue) {
        return validateEqual(modelValue, scope.$eval(attrs.validateEquals));
      });
    }
  };
}])
;

