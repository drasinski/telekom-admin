angular.module('fileUpload', [
  'angularFileUpload',
  'ui.bootstrap.modal',
  'constants',
  'ngImgCrop'
])
.directive('fileUpload', ['$modal', function ($modal) {
  return {
    restrict: 'E',
    templateUrl: 'profile/imageUpload/fileUpload.html',
    link: function ($scope, iElement, iAttrs) {

      $scope.onFileSelect = function onFileSelect($files) {
        if(angular.isDefined($files[0])) {
          var $file = $files[0];
          var rFilter = /^(image\/jpeg|image\/png)$/i;
          if (!rFilter.test($file.type)) {
            $scope.error = "Only JPG and PNG are allowed";
          } else if ($file.size > 1048576*2) {
            $scope.error = "Please upload a file less than 2 MB";
          } else if (window.FileReader) {
  
            var modalInstance = $modal.open({
              templateUrl: 'profile/imageUpload/cropImageModal.html',
              controller: 'cropImageController',
              resolve: {
                data: [ function() {
                  return $file;
                }]
              }
            });
  
            modalInstance.result.then(function (myModel) {
              $scope.user.thumbnail = myModel;
            }, function (err) {
              console.log("modal dissmissed");
            });
          }
        }
      };

    }
  };
}])
.controller('cropImageController', ['$scope', '$modalInstance','data', function cropImageController($scope,$modalInstance,data) {
  var fileReader = new FileReader();
  fileReader.readAsDataURL(data);
  var setPreview = function(fileReader) {
    fileReader.onload = function(e) {
      $scope.$apply(function ($scope) {
        $scope.dataUrls = e.target.result;
      })
    };
  };
  setPreview(fileReader);
  $scope.myCroppedImage = '';

  $scope.submit = function submit(myModel) {
    $modalInstance.close(myModel);
  };

  $scope.cancel = function cancel() {
    $modalInstance.dismiss('cancel');
  };

}]);
