angular.module( 'profile', [
  'ui.router',
  'security.authorization',
  'resource.users',
  'profile.drtv',
  'service.confirm',
  'user.srvcs',
  'fileUpload',
  'constants'
])
.config([ '$stateProvider','securityAuthorizationProvider','STORE', function config( $stateProvider, securityAuthorizationProvider, STORE) {
  $stateProvider.state('edit', {
    url:'/' + STORE.slug + '/edit/{username}',
    views: {
      "main": {
        controller:'EditUserCtrl',
        templateUrl:'profile/templates/profile.html'
      }
    },
    resolve: {
      authUser: securityAuthorizationProvider.requireAuthenticatedSameUser,
      user: ['UsersService','$stateParams', 'User','$q','authUser', function (UsersService,$stateParams, User, $q, authUser) {
        if(angular.equals(authUser.username, $stateParams.username)) {
          return $q.when(authUser);
        } else {
          return UsersService.get($stateParams.username).then(User.build);
        }
      }]
    },
    data: {
      pageTitle: 'Edit User'
    }
  });
}])
.controller('EditUserCtrl', ['$scope','user','ConfirmService', function ($scope, user, ConfirmService) {


  $scope.user = user;

  $scope.saveUser = function saveUser(user) {
    ConfirmService("Are you sure you want to save your changes?").then(function () {
      user.$save().then(function (user) {
        $scope.$state.go($scope.$state.previous, $scope.$state.prevParams);
      });
    });
  };

  $scope.goBack = function goBack(state) {
    state.go(state.previous, state.prevParams);
  }

}]);
