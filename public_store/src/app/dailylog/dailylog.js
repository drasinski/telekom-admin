angular.module( 'dailylog', [
  'ui.router',
  'filters',
  'security.authorization',
  'invoice.srvcs',
  'transaction.srvcs',
  'invoice.filters',
  'invoice.directive',
  'resource.invoices',
  'resource.inventory',
  'resource.types',
  'hdirectives',
  'ajoslin.promise-tracker',
  'constants',
  'ngAnimate'
])
.provider('dailyLog', {

  requireInvoices: ['dailyLog','$stateParams','currentUser','types', function (dailyLog, $stateParams, currentUser, types) {
    return dailyLog.requireInvoices($stateParams, currentUser);
  }],

  $get: ['InvoiceService','Invoice','Invoices', function (InvoiceService, Invoice, Invoices) {

    return {
      requireInvoices: function(stateParams, currentUser) {
        stateParams.from = stateParams.from || moment().startOf('day').format('MM-DD-YYYY');
        stateParams.to = stateParams.to || moment().startOf('day').add(1, 'days').format('MM-DD-YYYY');
        var query = {created_date:'{gte}'+stateParams.from+'{lte}'+stateParams.to};
        if(!currentUser.admin) {
          query['store.locationId'] = currentUser.store.locationId;
        };
        return InvoiceService.get(query).then(function (invoices) {
          return Invoices.build(invoices, currentUser, {endDate: moment(stateParams.to,'MM-DD-YYYY').valueOf(), startDate: moment(stateParams.from,'MM-DD-YYYY').valueOf()}); 
        });
      },

    }

  }]

})

.config([ '$stateProvider','securityAuthorizationProvider','dailyLogProvider','invoiceInitProvider','STORE', function config( $stateProvider, securityAuthorizationProvider, dailyLogProvider, invoiceInitProvider, STORE) {
  $stateProvider.state( 'dailylog', {
    url: '/' + STORE.slug + '/dailylog?from&to',
    views: {
      'main': {
        controller: 'DailyLogCtrl',
        templateUrl: 'dailylog/templates/dailylog.html'
      }
    },
    reloadOnSearch:false,
    resolve: {
      currentUser: securityAuthorizationProvider.requireAuthenticatedUser,
      types: invoiceInitProvider.requireTypesInventory,
      data: dailyLogProvider.requireInvoices
    },
    data: {
      pageTitle: 'Log'
    }
  })
  .state('dailylog.search', {
    url:'/invoice/{invoice:[0-9]{13}}',
    views: {
      '': {
        controller: 'searchInvoiceCtrl',
        templateUrl: 'dailylog/templates/dailylog-search-invoice.html'
      }
    },
    resolve: {
      findinvoice: ['Invoices', '$stateParams', function (Invoices, $stateParams) {
        return Invoices.search($stateParams.invoice).then(function (data) {
          return data;
        });
      }]
    }
  })
  ;
}])
.controller('DailyLogCtrl', ['$scope','$window','$location','data', function ($scope ,$window, $location, data) {

  $scope.data = data;
  // $scope.groupedInvoices = $filter('groupByInvoices')(invoices,'day');
  
  $scope.changeDates = function changeDates(data) {
    data.changeDate().then(function () {
      $location.search({'from':moment(data.myDateRange.startDate).format('MM-DD-YYYY'),'to':moment(data.myDateRange.endDate).format('MM-DD-YYYY')});
    });
  };

  /* sloppy way we need to update the $scope.invoices to trigger cache miss */
  // Hacky but works.
  $scope.updateLoc = function updateLoc(inv) {
    $scope.deleteInvoice(inv);
    $scope.invoices.push(inv);
  };


  $scope.searchInvoice = function searchInvoice(inv_number) {
    $scope.$state.go('dailylog.search',{invoice:inv_number}).then(function (state) {
      $scope.afterMsg = {
        message:'Found invoice:  ',
        invoice:'#'+inv_number,
        error:false
      };
    });
  };

  $scope.deleteInvoice = function deleteInvoice(viewinvoice) {
    $scope.data.deleteInvoice(viewinvoice);
  };

  $scope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    if(toState.name === 'dailylog.search') {
      $scope.error = true;
      $scope.afterMsg = {
        message:'Could not find invoice:  ',
        invoice:'#'+toParams.invoice,
        error:true
      };
    }
  });

  $scope.printInfo = function printInfo() {
    $window.print();
  };


}])
.controller('searchInvoiceCtrl', ['$scope','findinvoice', function ($scope,findinvoice) {
  $scope.findinvoice = findinvoice;
  $scope.changeState = function changeState() {
    $scope.$state.go('dailylog');
    $scope.afterMsg = {
      message:'',
      invoice:'',
      error:false
    };
  };
}])
.factory('Invoices', ['Invoice','InvoiceService','promiseTracker','STORE', function (Invoice, InvoiceService, promiseTracker, STORE) {
  

  function Invoices(options) {
    angular.extend(this, options);
    this.promiseTracker = promiseTracker();
    this.filters = {locs:[],users:[]};
    this.getFilterValues();
    this.setDateRange();
  }

  Invoices.prototype = {
    locations: _.map(STORE.locations, function (loc) {
      return {name: loc.name, id: loc.id};
    }),
    setDateRange: function() {
      var self = this;
      self.dateRange = [];
      self.dateRange.push(self.myDateRange.startDate);
      var startDate = moment(self.myDateRange.startDate).add(1,'days');
      var endDate = moment(self.myDateRange.endDate);
      while(!startDate.isSame(endDate, 'day')) {
        self.dateRange.push(startDate.format());
        startDate = startDate.add(1, 'days');
      }
    },
    getFilterValues: function() {
      var self = this;
      self.filters.users = _.chain(self.invoices).map(function (invoice) {
        return invoice.user.username;
      }).uniq().value();
      if((self.filters.locs.length === 0)) {
       // if(self.locations.indexOf(self.currentUser.store.locationId) > -1) {
          self.filters.locs.push(self.currentUser.store.locationId);
        // }
        //  else {
        //   self.filters.locs = _.pluck(self.locations,'id');
        // }
      }
      self.userroles = _.map(self.filters.users, function (user) {
        return {id:user, name: user};
      });
    },
    changeDate: function () {
      var self = this;
      var query = {created_date:('{gte}'+self.myDateRange.startDate+'{lte}'+self.myDateRange.endDate)};
      if(!self.currentUser.admin) {
        query['store.locationId'] = self.currentUser.store.locationId;
      }
      var promise = InvoiceService.get(query);
      self.promiseTracker.addPromise(promise);
      return promise.then(function (invoices) {
        self.invoices = _.map(invoices, Invoice.build);
        self.getFilterValues();
        self.setDateRange();
        return self.invoices;
      });
    },
    deleteInvoice: function (viewinvoice) {
      var self = this;
      self.invoices = _.reject(self.invoices, function (val) {
        return val._id === viewinvoice;
      })
    }
  };

  Invoices.build = function (invoices, user, myDateRange) {
    if(angular.isArray(invoices)) {
      invoices  = _.map(invoices, Invoice.build);
      return new Invoices({invoices:invoices, currentUser:user, myDateRange:myDateRange });
    }
  }

  return Invoices;

}])
.controller('viewInvoiceCtrl', ['$scope', function ($scope) {
  $scope.toggleSelected = function toggleSelected() {
    $scope.viewinvoice = !$scope.viewinvoice;
  };

  $scope.isSelected = function isSelected() {
    return $scope.viewinvoice;
  };

}]);
