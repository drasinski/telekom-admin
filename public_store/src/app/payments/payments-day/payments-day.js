angular.module('payments.day', [
  'ui.router',
  'hdirectives',
  'security.authorization',
  'resource.clients',
  'clients.srvcs',
  'invoice.srvcs',
  'invoice.srvcs.new-modal',
  'invoice.directive',
  'payments-day.directives',
  'ui.bootstrap.tabs',
  'ui.bootstrap.tooltip',
  'service.printer',
  'ui.bootstrap.collapse',
  'filters'
])
.config([ '$stateProvider','securityAuthorizationProvider','invoiceInitProvider','clientInitProvider','STORE', function ( $stateProvider, securityAuthorizationProvider, invoiceInitProvider, clientInitProvider, STORE) {
  $stateProvider.state('payments.day', {
    abstract:true,
    url:'/{day:(?:[0][1-9]|[1-2][0-9]|3[01]|[1-9])}',
    templateUrl:'payments/payments-day/templates/payments-day.html',
    controller:'PaymentsCtrl',
    resolve: {
      currentUser: securityAuthorizationProvider.requireAuthenticatedUser,
      PaymentDay: ['calendar','$stateParams','PaymentDay', function (calendar, $stateParams, PaymentDay) {
        var date = moment($stateParams.year+'-'+$stateParams.month+'-'+$stateParams.day,'YYYY-MMM-D').format();
        var day = _.findWhere(_.flatten(calendar.rows), {date: date});
        return PaymentDay.build(day.clients, day.note, date);
      }]
    }
  })
  .state('payments.day.creditcard', {
    url:'/recurring',
    abstract:true,
    template: "<div ui-view='' ng-show='PaymentDay.setclient.isClient()'></div>",
    data: {
      header: 'Recurring Payments'
    },
    resolve: {
      types: invoiceInitProvider.requireTypesInventory,
      clients: ['PaymentDay', function (PaymentDay) {
        PaymentDay.setClients('creditcard');
      }]
    }
  })
  .state('payments.day.creditcard.client', {
    url:'/{id}',
    controller:'RecurringPaymentsCtrl',
    templateUrl:'payments/payments-day/templates/tabs/recurring.html',
    resolve: {
      setclient: ['PaymentDay','$stateParams', function (PaymentDay, $stateParams) {
        return PaymentDay.setSetClient($stateParams);
      }],
      invoice: ['currentUser','Invoice','setclient','types', function (currentUser, Invoice, setclient, types) {
        var x = Invoice.build({
          paymentInfo: {
            Credit:true,
          },
          invoice_number: new Date().getTime(),
          store: {
            locationId: currentUser.store.locationId,
            storeId: currentUser.store._id
          },
          user: {
            username: currentUser.username,
            userId: currentUser._id
          },
          client: setclient
        });
        x.newPayment();
        return x;
      }]
    }
  })
  .state('payments.day.none', {
    abstract:true,
    url:'/upcoming',
    template: "<div ui-view='' ng-show='PaymentDay.setclient.isClient()'></div>",
    data: {
      header: 'Upcoming Payments'
    },
    resolve: {
      carriers: clientInitProvider.requireCarriers,
      clients: ['PaymentDay', function (PaymentDay) {
        PaymentDay.setClients('none');
      }]
    }
  })
  .state('payments.day.none.client', {
    url:'/{id}',
    templateUrl:'payments/payments-day/templates/tabs/upcoming.html',
    resolve: {
      setclient: ['PaymentDay','$stateParams', function (PaymentDay, $stateParams) {
        return PaymentDay.setSetClient($stateParams);
      }]
    }
  })
  .state('payments.day.prepaid', {
    abstract:true,
    url:'/prepaid',
    data: {
      header:'Prepaid Payments'
    },
    template: "<div ui-view='' ng-show='PaymentDay.setclient.isClient()'></div>",
    resolve: {
      carriers: clientInitProvider.requireCarriers,
      clients: ['PaymentDay', function (PaymentDay) {
        PaymentDay.setClients('prepaid');
      }]
    }
  })
  .state('payments.day.prepaid.client', {
    url:'/{id}',
    resolve: {
      setclient: ['PaymentDay','$stateParams', function (PaymentDay, $stateParams) {
        return PaymentDay.setSetClient($stateParams);
      }]
    },
    templateUrl:'payments/payments-day/templates/tabs/prepaid.html'
  });

}])
.controller('PaymentsCtrl', ['$scope','$filter','PaymentDay', function ($scope, $filter, PaymentDay) {

  $scope.PaymentDay = PaymentDay;

  $scope.sortField = {};
  $scope.reverse = {};
  $scope.sort = function sort(fieldName) {
    if ($scope.sortField[$scope.$state.current.name] === fieldName) {
      $scope.reverse[$scope.$state.current.name] = !$scope.reverse[$scope.$state.current.name];
    } else {
      $scope.sortField[$scope.$state.current.name] = fieldName;
      $scope.reverse[$scope.$state.current.name] = false;
    }
  };

  $scope.$watch('sortField', function (nv, ov) {
    if(angular.equals(nv,ov)) {
      $scope.PaymentDay.clients = $filter('orderBy')($scope.PaymentDay.clients, nv[$scope.$state.current.name],$scope.reverse[$scope.$state.current.name])
    }
  }, true);

  $scope.$watch('reverse', function (nv, ov) {
    if(!angular.equals(nv,ov)) {
      $scope.PaymentDay.clients = $filter('orderBy')($scope.PaymentDay.clients, $scope.sortField[$scope.$state.current.name], nv[$scope.$state.current.name])
    }
  }, true);


  $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    if(angular.isDefined(fromState.name) && (fromState.name.length > 0)) {
      $scope.PaymentDay.prevParams[fromState.name] = fromParams.id;
    }
  });

  $scope.isSortUp = function isSortUp(fieldName) {
    return $scope.sortField[$scope.$state.current.name] === fieldName && !$scope.reverse[$scope.$state.current.name];
  };

  $scope.isSortDown = function isSortDown(fieldName) {
    return $scope.sortField[$scope.$state.current.name] === fieldName && $scope.reverse[$scope.$state.current.name];
  };

  $scope.isCollapsedFunc = function isCollapsedFunc() {
    if(!$scope.PaymentDay.clients.length) {
      return false;
    } else {
      return $scope.isCollapsed;
    }
  }



}])
.controller('RecurringPaymentsCtrl', ['$scope','printer','invoice','invoiceModal', function ($scope, printer, invoice, invoiceModal) {

  $scope.invoice = invoice;
  // $scope.$parent.isCollapsed = (clients.length > 0);

  $scope.submitInvoice = function submitInvoice(invoice) {
    invoiceModal.open(invoice).then(function (response) {
      response.printFlag && $scope.printInvoice();
      console.log("Successfully submitted invoice");
    }, function (err) {
      console.log(err);
    });
  };

  $scope.printInvoice = function printInfo() {
    return printer.printFromScope('invoice/templates/invoice-print.html', $scope);
  };

}])
.factory('PaymentDay',[ '$state','$q', function ($state, $q) {

  function PaymentDay(options) {
    var self = this;
    angular.extend(this, options)
    angular.extend(this.note, options.note);
    this.prevParams = {};
    this.leftTooltip = moment(this.day).add(-1, 'days').format();
    this.rightTooltip = moment(this.day).add(1, 'days').format();
    this.filteredClients = {
      'prepaid': _.filter(self.day_clients, function (client) {
        return (client.paymenttype === 'prepaid');
      }),
      'none': _.filter(self.day_clients, function (client) {
        return (client.paymenttype === 'none');
      }),
      'creditcard': _.filter(self.day_clients, function (client) {
        return (client.paymenttype === 'creditcard');
      })
    }
  }

  PaymentDay.prototype = {
    changeDay: function (direction) {
      var day = moment(this.day).add(direction, 'days');
      $state.go($state.current.name, {day:moment(day).format('D'), year:moment(day).format('YYYY'), month:moment(day).format('MMM'), id:undefined });
    },
    changeClient: function (client) {
      $state.go($state.current.name, {id: client._id});
    },
    nextClient: function (direction) {
      this.changeClient(this.getClientTooltipClient(direction));
    },
    sendSMStoAll: function() {
      angular.forEach(this.clients, function (client) {
        var x = {
          for_date: client.rechargeDate,
          message_type: (client.paymenttype === 'prepaid') ? 'Prepaid':'Upcoming',
          message: client.message
        }
        $q.all([client.$sendSMS(x), client.$save()]);
      });
    },
    setClients: function (key) {
      this.clients = this.filteredClients[key];
    },
    setSetClient: function(stateParams) {
       if(_.isEmpty(stateParams.id) && this.clients.length) {
         stateParams.id = this.clients[0]._id;
         this.setclient = this.clients[0];
       } else {
         this.setclient = _.findWhere(this.clients, {'_id': stateParams.id})||{};
       }
       return this.setclient;
    },
    isClient: function (string) {
      var self = this;
      var i = this.clients.indexOf(_.findWhere(this.clients, {_id: self.setclient._id}));
      if(string === 'first') {
        return (i === 0) || (this.clients.length === 1);
      } else if(string === 'last') {
        return (i === (this.clients.length-1)) || (this.clients.length === 1);
      }
    },
    getClientTooltipClient: function (direction) {
      var self = this;
      var i = this.clients.indexOf(_.findWhere(this.clients, {_id: self.setclient._id}))+direction;
      if(i > -1 && i < this.clients.length) {
        return self.clients[i];
      }
    }
  }


  PaymentDay.build = function(day_clients, note, date) {
    return new PaymentDay({day_clients: day_clients, note:note, day:date});
  }

  return PaymentDay;

}])
