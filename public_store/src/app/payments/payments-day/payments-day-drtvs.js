angular.module('payments-day.directives',[
  'hdirectives'
])
.directive('message', ['$q', function ( $q) {
  return {
    restrict: 'E',
    replace:true,
    templateUrl: 'payments/payments-day/templates/sms.html',
    link: function($scope, el, attrs) {

      $scope.$watch('PaymentDay.setclient.language', function (nv, ov) {
        if(nv !== ov) {
          $scope.PaymentDay.setclient.getSMSText();
        }
      });

      $scope.sendSMS = function sendSMS(client) {
        var x = {
          for_date: client.rechargeDate,
          message_type: (client.paymenttype === 'prepaid') ? 'Prepaid':'Upcoming',
          message: client.message
        }
        var promises = [client.$sendSMS(x),client.$save()];
        $q.all(promises).then(function () {
          console.log("successfully sent SMS")
        }, function (err) {
          console.log(err);
        });
      };
    }
  };
}]);