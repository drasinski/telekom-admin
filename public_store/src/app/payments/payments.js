angular.module('payments', [
  'ui.router',
  'hdirectives',
  'security.authorization',
  'resource.clients',
  'resource.notes',
  'clients.srvcs',
  'payments.day',
  'note.srvcs',
  'service.hasher'
])  
.config([ '$stateProvider','securityAuthorizationProvider','STORE', function ( $stateProvider, securityAuthorizationProvider, STORE) {
  $stateProvider.state( 'payments', {
    url: '/' + STORE.slug + '/payments/{year:(?:19|20)[0-9]{2}}/{month:(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)}',
    views: {
      "main": {
        controller: 'PaymentsListCtrl',
        templateUrl: 'payments/templates/payments.html'
      }
    },
    resolve: {
      currentUser: securityAuthorizationProvider.requireAuthenticatedUser,
      calendar: ['$q','ClientService','NoteService','Calendar','$stateParams', function ($q, ClientService, NoteService, Calendar, $stateParams) {
        $stateParams.year = $stateParams.year || moment().format("YYYY");
        $stateParams.month = $stateParams.month || moment().format("MMM");
        var startDate = moment().year($stateParams.year).month($stateParams.month).startOf('month').subtract(10,'days');
        var endDate = moment().year($stateParams.year).month($stateParams.month).endOf('month').add(14,'days');
        var noteQuery = {date:'{gte}'+startDate.format()+'{lte}'+endDate.format()};
        return $q.all([ClientService.get(),NoteService.get(noteQuery)]).then(function (resp) {
          return Calendar.build(resp[0], resp[1], $stateParams.year, $stateParams.month);
        });
      }]
    },
    data: {
      pageTitle: 'Payments'
    }
  });
}])
.controller('PaymentsListCtrl', ['$scope','calendar', function ClientInfoCtrl ($scope, calendar) {

  $scope.calendar = calendar

}])
.factory('Day', ['$state','Note', function ($state, Note) {

  function Day(options) {
    angular.extend(this, options);
    angular.extend(this.note, options.note);
    this.creditcard_payments = 0;
    this.none_payments = 0;
    this.prepaid_payments = 0;
    this.clients = [];
  }

  Day.prototype = {
    isToday: function() {
      return moment().isSame(this.date,'day');
    },
    isPast: function() {
      return moment().isAfter(this.date, 'day');
    },
    goPaymentsDay: function(key) {
      key = key || 'creditcard';
      $state.go('payments.day.' + key + '.client',{day:moment(this.date).format('D'), year:moment(this.date).format('YYYY'), month:moment(this.date).format('MMM') });
    }
  }

  Day.build = function(day, note) {
    return new Day({date:day, note: Note.build(note)});
  }

  return Day;

}])
.factory('calendarCache',['$cacheFactory', function ($cacheFactory) {
  return $cacheFactory('calendar');
}])
.factory('Calendar', ['$state','Client','Day','hasher','calendarCache', function ($state, Client, Day, hasher, calendarCache) {

  function Calendar(options) {
    angular.extend(this,options);
  }

  Calendar.prototype = {
    get popoverTemplateUrl() {
      return 'payments/templates/payments-note-popover.html';
    },
    move: function (direction) {
      var date = moment(this.activeDate).add(direction,'months');
      $state.go('payments',{year: date.format('YYYY'), month: date.format('MMM') },{ reload:true });
    }
  }

  function getDates(startDate, n) {
    var dates = new Array(n), current = moment(startDate).startOf('day'), i = 0;
    while ( i < n ) {
      dates[i++] = current.format();
      current = current.add(1,'days');
    }
    return dates;
  };

  function getFirstDate(year, month) {
    year = parseInt(year);
    month = moment().month(month).month();
    var firstDayOfMonth = new Date(year, month, 1),
        difference = 0 - firstDayOfMonth.getDay(),
        numDisplayedFromPreviousMonth = (difference > 0) ? 7 - difference : - difference,
        firstDate = new Date(firstDayOfMonth);
    if ( numDisplayedFromPreviousMonth > 0 ) {
      firstDate.setDate( - numDisplayedFromPreviousMonth + 1 );
    }
    return firstDate;
  };

  function initDays(days, clients, firstDate) {
    angular.forEach(clients, function (client) {
      var i = moment(client.rechargeDate).diff(moment(firstDate).startOf('day'),'days');
      if(client.rechargeDate && (i >= 0) && (i < 42)) {
        days[i][client.paymenttype+'_payments'] += 1;
        days[i].clients.push(client);
      }
    });
    return days;
  }

  var daysFn = _.memoize(initDays, function (days, clients, firstDate) {
    clients = _.map(clients, function (client) {
      return {
        id: client._id,
        rechargeDate: client.rechargeDate,
        paymenttype: client.paymenttype
      }
    });
    days = _.pluck(days,'date');
    return hasher()(days, clients, firstDate);
  });


  function getHashKey(clients, year, month) {
    return JSON.stringify(_.map(clients, function (client){
      return {
        id: client._id,
        rechargeDate: client.rechargeDate,
        paymenttype: client.paymenttype,
        receive_sms: client.receive_sms,
        messages_sent: client.messages_sent.length
      }      
    }))+'-'+year+'-'+month;
  }

  // NEED TO THINK THIS CACHE FACTORY THROUGH
  Calendar.build = function(clients, notes, year, month) {
    if(angular.isUndefined(calendarCache.get(getHashKey(clients,year,month)))) {
      var activeDate = moment().year(year).month(month).startOf('day').format();
      clients = _.map(clients, Client.build);
      var firstDate = getFirstDate(year, month);
      // 35 is the number of days on a six-month calendar
      var days = _.map(getDates(firstDate, 42), function (day) {
        var note = _.filter(notes, function (note) {
          return moment(note.date).isSame(day,'day');
        });
        return Day.build(day, note[0] || {date:day});
      });
      days = initDays(days, clients, firstDate);
      var options = {
        rows: _.chain(days).groupBy(function (ele,idx) {return Math.floor(idx/7)}).toArray().value(),
        activeDate: activeDate,
        leftTooltip: moment(activeDate).subtract(1,'months').format(),
        rightTooltip: moment(activeDate).add(1,'months').format()
      };
      calendarCache.put(getHashKey(clients,year,month), options);
    } else {
      var options = calendarCache.get(getHashKey(clients,year,month));
    }
    return new Calendar(options);
  }

  return Calendar;

}]);
