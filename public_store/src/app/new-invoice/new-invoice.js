angular.module( 'invoice', [
  'ui.router',
  'security.authorization',
  'hdirectives',
  'constants',
  'filters',
  'service.printer',
  'resource.inventory',
  'resource.clients',
  'resource.types',
  'invoice.srvcs.new-modal',
  'invoice.srvcs',
  'inventory.srvcs',
  'invoice.directive',
  'clients.directive',
  'ngAnimate',
  'angulartics'
])
.provider('newInvoice', {

  requireClient: ['newInvoice','$stateParams','currentUser', function (newInvoice, $stateParams, currentUser) {
    return newInvoice.requireClient($stateParams.id, currentUser);
  }],

  requireInvoice: ['newInvoice','client','currentUser','types', function (newInvoice, client, currentUser, types) {
    return newInvoice.requireInvoice(client, currentUser);
  }],

  $get: ['Client','ClientService','STORE', 'Invoice', function (Client, ClientService, STORE, Invoice) {
    return {

      requireClient: function(stateParam, currentUser) {
        var emptyClient = {
          store: {
            locationId: currentUser.store.locationId,
            storeId: STORE._id
          }
        };
        if(Client.idRegex.test(stateParam)) {
          return ClientService.get({'_id':stateParam}, true).then(function (client) {
            if(angular.isArray(client)) {
              return client[0];
            } else {
              return emptyClient;
            }
          }, function (err) {
            return emptyClient;
          });
        } else {
          return emptyClient;
        }
      },

      requireInvoice: function(client, currentUser) {
        return Invoice.build({
          store: {
            locationId: currentUser.store.locationId,
            storeId: currentUser.store.locationId
          },
          user: {
            username: currentUser.username,
            userId: currentUser._id
          },
          client: client,
          invoice_number: new Date().getTime(),
          typeofpayment: ['Cash']
        });
      }
    }
  }]
})
.config([ '$stateProvider','securityAuthorizationProvider','newInvoiceProvider','invoiceInitProvider','clientInitProvider','STORE', function config( $stateProvider, securityAuthorizationProvider, newInvoiceProvider, invoiceInitProvider, clientInitProvider, STORE) {
  $stateProvider.state( 'invoice', {
    url: '/' + STORE.slug + '/invoice?id',
    views: {
      "main": {
        controller: 'InvoiceCtrl',
        templateUrl: 'new-invoice/templates/new-invoice.html'
      }
    },
    resolve: {
      currentUser: securityAuthorizationProvider.requireAuthenticatedUser,
      types: invoiceInitProvider.requireTypesInventory,
      carriers: clientInitProvider.requireCarriers,
      client: newInvoiceProvider.requireClient,
      invoice: newInvoiceProvider.requireInvoice
    },
    data: {
      pageTitle: 'Invoice'
    }
  });
}])
.controller('InvoiceCtrl', [ '$scope','$window','$analytics','Inventory','inventoryModal','invoiceModal', 'invoice', 'printer',
   function InvoiceCtrl ( $scope, $window, $analytics, Inventory, inventoryModal, invoiceModal, invoice, printer) {

  $scope.invoice = invoice

  $scope.addPhone = function addPhone(store, transaction) {
    inventoryModal.addPhone(Inventory.build({store:store})).then(function (data) {
      $analytics.eventTrack('Add Phone',{category:"New Invoice"})
      transaction.addPhone(data);
    })
  };

  $scope.submitInfo = function submitInfo(invoice) {
    invoice.checkExistingClient().then(function (invoice) {
      invoiceModal.open(invoice).then(function (response) {
        if(response.printFlag) {
          $analytics.eventTrack('Print Modal',{category:'New Invoice'});
          $scope.printInfo().then(function() {
            if(invoice.hasPhones()) {
              $scope.$state.reload(); // need to reset inventory to remove phone
            } else {
              invoice.clear();
            }
          });
        } else {
          if(invoice.hasPhones()) {
            $scope.$state.reload(); // need to reset inventory to remove phone
          } else {
            invoice.clear();
          }
        }
      });
    });
  };

  $scope.printInfo = function printInfo() {
    return printer.printFromScope('invoice/templates/invoice-print.html', $scope);
  };

  // $scope.printInfo = function printInfo() {
  //   return $timeout(function(){
  //     $window.print();
  //   });
  // };

}]);
