angular.module( 'admin.messages', [
  'ui.router',
  'security.authorization',
  'constants',
  'filters',
  'clients.srvcs',
  'resource.types',
  'hdirectives',
  'rzModule',
  'service.notify',
  'resource.clients'
])
.config([ '$stateProvider','securityAuthorizationProvider','clientInitProvider','STORE', function config( $stateProvider, securityAuthorizationProvider, clientInitProvider, STORE) {
  $stateProvider.state( 'messages', {
    url: '/'+STORE.slug+'/messages',
    views: {
      "main": {
        controller: 'MessagesCtrl',
        templateUrl: 'admin/messages/templates/messages.html'
      }
    },
    resolve: {
      authUser: securityAuthorizationProvider.requireAdminUser,
      carriers: clientInitProvider.requireCarriers,
      data: ['ClientService','Messages','carriers', function (ClientService, Messages, carriers) {
        return ClientService.get().then(function (clients) {
          return Messages.build(clients, carriers);
        });
      }]
    },
    data: {
      pageTitle: 'Messages'
    }
  })
}])
.controller('MessagesCtrl', ['$scope','$filter','data', function ($scope, $filter, data) {

  $scope.data = data;


  $scope.translatePlan = function translatePlan(input) {
    return $filter('noFractionCurrency')(input);
  };

  $scope.sort = function sort(fieldName) {
    if ($scope.sortField === fieldName) {
      $scope.reverse = !$scope.reverse;
    } else {
      $scope.sortField = fieldName;
      $scope.reverse = false;
    }
  };

  $scope.isSortUp = function isSortUp(fieldName) {
    return $scope.sortField === fieldName && !$scope.reverse;
  };

  $scope.isSortDown = function isSortDown(fieldName) {
    return $scope.sortField === fieldName && $scope.reverse;
  };

}])
.factory('Messages',['$q','Client','STORE','notifyService','ClientService', function ($q, Client, STORE, notifyService, ClientService) {

  function Messages(options) {
    var self = this;
    angular.extend(this, options);
    self.message = '';
    self.message_list = [];
    this.filters = {
      carriers: _.pluck(self.carriers,'id'),
      languages: _.pluck(Client.languages,'id'),
      paymenttypes: _.pluck(Client.paymenttypes,'id'),
      plans: {
        min: 0,
        max: self.plans.ceil
      },
      dateRange: {startDate:moment(STORE.created_date,'MM-DD-YYYY').valueOf(), endDate: moment().add(30,'days').format('MM-DD-YYYY').valueOf() }
    };
  }

  Messages.prototype = {
    get languages() {
      return Client.languages;
    },
    get paymenttypes() {
      return Client.paymenttypes
    },
    inFilters: function(client) {
      var self = this;
      return _.contains(self.filters.carriers, client.carrier) &&
            _.contains(self.filters.languages, client.language) &&
            _.contains(self.filters.paymenttypes, client.paymenttype) &&
            (moment(client.paiduntil).isBetween(self.filters.dateRange.startDate,self.filters.dateRange.endDate,'day') ||
            moment(client.paiduntil).isSame(self.filters.dateRange.startDate,'day') ||
            moment(client.paiduntil).isSame(self.filters.dateRange.endDate,'day')) &&
            (client.plan >= self.filters.plans.min) &&
            (client.plan <= self.filters.plans.max);
    },
    addToMessageList: function (client) {
      client.dont_send = !client.dont_send;
    },
    getMessageListNumbers: function() {
      return _.chain(this.message_list).filter(function (client) {
        return !client.dont_send;
      }).pluck('telephone').value();
    },
    getMessageList: function() {
      var self = this;
      self.message_list = _.filter(self.clients, function (client) {
        client.dont_send = false;
        return self.inFilters(client);
      })
    },
    sendBulkSMS: function() {
      var self = this;
      var options = {
        message: self.message,
        to: self.getMessageListNumbers()
      }
      return ClientService.smsBulk(options).then(function (response) {
        if(response.failed_messages) {
          notifyService("Could not send message to " + response.failed_messages +' clients.', 'alert-danger');
          console.log(response.failed_numbers);
        }
        notifyService("Successfully sent " + response.sent_messages +' messages.', 'alert-success');
        return response;
      }, function (err) {
        return $q.reject(err);
      });
    }
  }

  Messages.build = function (clients, carriers) {
    if(angular.isArray(clients) && clients.length) {
      clients = _.map(clients, Client.build);
      return new Messages({
        clients: clients,
        message:'',
        plans: {
          floor:0,
          ceil: _.max(clients, function (client) {
            return client.plan
          }).plan
        },
        carriers: carriers
      });
    }
  }

  return Messages;

}])
;
