angular.module( 'admin.inventory', [
  'ui.router',
  'security.authorization',
  'resource.inventory',
  'inventory-group.srvcs',
  'inventory.srvcs',
  'inventory.filters',
  'filters',
  'hdirectives',
  'inventory.drtv'
])
.config([ '$stateProvider','securityAuthorizationProvider','STORE', function config( $stateProvider, securityAuthorizationProvider, STORE) {
  $stateProvider.state( 'inventory', {
    url: '/' + STORE.slug + '/inventory',
    views: {
      "main": {
        controller: 'InventoryCtrl',
        templateUrl: 'admin/inventory/templates/inventory.html'
      }
    },
    resolve: {
      currentUser: securityAuthorizationProvider.requireAdminUser,
      inventory: ['InventoryService', function (InventoryService) {
        return InventoryService.get({sold:'f'});
      }]
    },
    data: {
      pageTitle: 'Inventory'
    }
  });
}])
.controller('InventoryCtrl', ['$scope','Inventory','inventory','inventoryModal','InventoryGroup','$filter','STORE', function ($scope,Inventory,inventory, inventoryModal, InventoryGroup, $filter, STORE) {

  function initView () {
    $scope.inventory = $filter('groupBy')(inventory,'description');
    $scope.inventory = _.map($scope.inventory, InventoryGroup.build);
    $scope.inventory = $filter('partitionBy')($scope.inventory);
  };

  $scope.searchFilter = function searchFilter(term,key) {
    term = term || '';
    return ((key.toLowerCase()).indexOf(term.toLowerCase()) > -1);
  };

  initView();

  $scope.filters = {
    locOptions: _.map(STORE.locations, function (loc) {
      return {name: loc.name, id: loc.id};
    }),
    locs: _.pluck(STORE.locations, 'id')
  };

  $scope.addPhone = function addPhone(store) {
    inventoryModal.addPhone(Inventory.build({store:store})).then(function (data) {
      inventory.push(data);
      initView();
    });
  };

}]);
