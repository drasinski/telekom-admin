angular.module('inventory.drtv', [
  'service.confirm',
  'ui.bootstrap.collapse'
])
.directive('inventoryItem', [ 'ConfirmService','$modal','$q', function (ConfirmService, $modal, $q) {
  return {
    restrict: 'E',
    templateUrl: 'admin/inventory/templates/inventory-group.html',
    link: function ($scope, iElement, iAttrs) {

      $scope.isCollapseditem = true;

      $scope.savePhone = function savePhone(phones) {
        ConfirmService("Are you sure you want to save your changes?").then(function () {
          phones.$save().then(function (phones) {
            console.log("successfully saved the phones");
          }, function (err) {
            console.log(err);
          })
        });
      }

    }
  };
}])
;