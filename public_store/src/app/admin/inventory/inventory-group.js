angular.module('inventory-group.srvcs', [
  'inventory.srvcs'
])
.factory('InventoryGroup', ['Inventory', '$q', function (Inventory, $q) {

  function InventoryGroup(options, phonename) {
    this.phonename = phonename;
    this.sortField = undefined;
    this.reverse;
    this.deletePhones = [];
    this.icons = {};
    this.phones = options;
  }

  InventoryGroup.prototype = {

    isValid: function() {
      return _.reduce(this.phones, function (memo, phone) {
        return memo && phone.isValid();
      }, true);
    },

    cancel: function () {
      var self = this;
      self.phones = _.chain(self.phones.concat(self.deletePhones)).each(function (phone) {
        phone.revert();
      }).filter(function (phone) {
        return !phone.isNewPhone();
      }).value();
      self.deletePhones = [];
    },

    $save: function () {
      var self = this;
      if(self.isValid()) {
        var phones = _.chain(self.phones).filter(function (phone) {
          return phone.hasChanged() || phone.isNewPhone();
        }).map(function (phone) {
          return phone.$save();
        }).value().concat(_.map(self.deletePhones, function (phone) {
          return phone.$remove();
        }));
        return $q.all(phones).then(function (response) {
          self.deletePhones = [];
          return self.phones;
        });
      }
    },

    removePhone: function(phone) {
      var idx = this.phones.indexOf(phone);
      if((this.phones.length > idx) && (idx > -1)) {
        this.phones.splice(idx,1);
        if(!phone.isNewPhone()) {
          this.deletePhones.push(phone);
        }
      }
    },

    addPhone: function() {
      var self = this;
      self.phones.push(Inventory.build({description:self.phonename}));
      self.sortField = false;
    },

    sort: function(fieldname) {
      this.icons[fieldname] = false;
      if(fieldname === this.sortField) {
        this.reverse = !this.reverse;
      } else {
        this.sortField = fieldname;
        this.reverse = false;
      }
    },

    isSortUp: function (fieldname) {
      return (this.sortField === fieldname && !this.reverse) || this.icons[fieldname];
    },

    isSortDown: function (fieldname) {
      return this.sortField === fieldname && this.reverse;
    },

    hasChanged: function() {
      return _.reduce(this.phones, function (memo, val) {
        return memo || val.hasChanged();
      },false) || (this.deletePhones.length > 0);
    },

    containsLoc: function(locations, phone) {
      return _.contains(locations, phone.store.locationId) || phone.isNewPhone();
    },

    phonesLength: function(locations) {
      var self = this;
      return _.filter(self.phones, function (phone) {
        return self.containsLoc(locations, phone);
      }).length;
    }

  }

  InventoryGroup.build = function(options, phonename) {
    if(angular.isArray(options)) {
      options = _.map(options, Inventory.build);
    }
    return new InventoryGroup(options, phonename);
  }


  return InventoryGroup;

}]);