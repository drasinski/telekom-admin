angular.module( 'admin.config', [
  'ui.router',
  'security.authorization',
  'constants',
  'hdirectives',
  'type.srvcs',
  'filters',
  'service.confirm',
  'iconpicker',
  'config.drtv',
  'store.srvcs',
  'service.notify'
])
.config([ '$stateProvider','securityAuthorizationProvider','STORE', function config( $stateProvider, securityAuthorizationProvider, STORE) {
  $stateProvider.state( 'config', {
    url: '/'+STORE.slug+'/config',
    views: {
      "main": {
        controller: 'ConfigCtrl',
        templateUrl: 'admin/config/templates/config.html'
      }
    },
    resolve: {
      authUser: securityAuthorizationProvider.requireAdminUser,
      config:['TypeService','Config', function (TypeService, Config) {
        return TypeService.get({}).then(function (types) {
          return Config.build(types);
        });
      }],
      store:['Store', function (Store) {
        return Store.build();
      }]
    },
    data: {
      pageTitle: 'Config'
    }
  })
  .state('config.newtype', {
    url:'/type/{category_slug}',
    templateUrl:'admin/config/templates/type.html',
    controller: 'TypeCtrl',
    resolve: {
      type: ['config','$stateParams','$q', function (config, $stateParams, $q) {
        if(config.hasCategory($stateParams.category_slug)) {
          var z = _.findWhere(config.categories, {category_slug: $stateParams.category_slug});
          if(config.isThereNewType(z.category)) {
            return config.getNewType(z.category);
          } else {
            return config.addType(z.category, z.category_slug, z.icon);
          }
        } else {
          return $q.reject('Has no such category');
        }
      }]
    }
  })
  .state('config.edittype', {
    url:'/type/{category_slug}/{id}',
    controller:'TypeCtrl',
    templateUrl:'admin/config/templates/type.html',
    resolve: {
      type: ['config','$stateParams','$q', function (config, $stateParams, $q) {
        if(config.hasType($stateParams.category_slug, $stateParams.id)) {
          return config.getType($stateParams.category_slug, $stateParams.id);
        } else {
          console.log($stateParams);
          return $q.reject('Has no type:  ' + $stateParams.id);
        }
      }]
    }
  })
  .state('config.store', {
    url: '/store',
    controller: 'StoreCtrl',
    templateUrl: 'admin/config/templates/store.html',
    resolve: {
      store: ['Store', function (Store) {
        return Store.build();
      }]
    }
  })
  ;

}])
.controller('ConfigCtrl', ['$scope','config','store', function ($scope, config, store) {

  $scope.store = store;

  $scope.config = config;

  $scope.goStore = function goStore() {
    if($scope.$state.is('config.store')) {
      $scope.$state.go('config');
    } else {
      $scope.$state.go('config.store');
    }
  };

  $scope.addCategory = function addCategory(config, category, icon) {
    $scope.addType(config, config.addCategory(category, icon));
    $scope.newcategory_name = '';
    $scope.newicon = 'fa fa-info';
  }

  $scope.removeCategory = function removeCategory(config, category) {
    config.removeCategory(category);
    $scope.$state.go('config');
  };

  $scope.addType = function addType(config, category) {
    $scope.$state.go('config.newtype', {category_slug: config.slugify(category)});
  };

  $scope.isSelected = function isSelected(type) {
    return (angular.isUndefined(type._id) && $scope.$state.is('config.newtype',{category_slug:type.category_slug})) || $scope.$state.is('config.edittype',{category_slug:type.category_slug, id:type._id});
  };

  $scope.selectType = function selectType(type) {
    if($scope.isSelected(type)) {
      $scope.$state.go('config');
    } else {
      if(type.isNewType()) {
        $scope.$state.go('config.newtype', {category_slug: type.category_slug });
      } else {
        $scope.$state.go('config.edittype',{category_slug:type.category_slug, id:type._id});
      }
    }
  };

}])
.controller('CategoriesCtrl',['$scope', function ($scope) {

  $scope.initIcon = 'fa ' + $scope.types[0].icon;

  $scope.$watch('icon', function (nv, ov) {
    if(angular.isDefined(nv) && !angular.isUndefined(ov)) {
      $scope.config.updateIcon($scope.category, _.last(nv.split(' ')));
    }
  });

  $scope.isNewCategory = function isNewCategory() {
    return ($scope.types.length === 0) || angular.isUndefined($scope.types[0]._id);
  }
  $scope.isCollapsed = $scope.isNewCategory();

  $scope.isSortUp = function isSortUp(fieldName) {
    return $scope.sortField === fieldName && !$scope.reverse;
  };

  $scope.isSortDown = function isSortDown(fieldName) {
    return $scope.sortField === fieldName && $scope.reverse;
  };

  $scope.triggerCollapse = function triggerCollapse() {
    if(!$scope.isNewCategory()) {
      $scope.isCollapsed = !$scope.isCollapsed;
    }
  }

  $scope.sort = function sort(fieldName) {
    if ($scope.sortField === fieldName) {
      $scope.reverse = !$scope.reverse;
    } else {
      $scope.sortField = fieldName;
      $scope.reverse = false;
    }
  };

}])
.controller('TypeCtrl', ['$scope','ConfirmService','type', function ($scope, ConfirmService, type) {

  $scope.date = new Date();

  $scope.editType = type;

  $scope.cancel = function cancel(config, type) {
    config.removeType(type);
    $scope.$state.go('config');
  }

  $scope.saveType = function saveType(type) {
    ConfirmService("Save your changes to " + type.description + "?").then(function (result) {
      type.$save().then(function (newType) {
        $scope.$state.go('config.edittype',{ id: newType._id, category_slug: newType.category_slug });
      });
    });
  }

  var x = $scope.editType.getDefaultPred();
  if(angular.isDefined(x)) {
    $scope.reference_amount = x.amount || 100;
    $scope.reference_servicecharge = x.servicecharge || 4;
    $scope.reference_tax = $scope.reference_amount * $scope.editType.store.tax_fee;
  } else {
    $scope.reference_amount = 100;
    $scope.reference_servicecharge = 4;
    $scope.reference_tax = $scope.reference_amount * $scope.editType.store.tax_fee;
  }

}])
.controller('StoreCtrl', ['$scope', 'store','ConfirmService', function ($scope, store, ConfirmService) {

  $scope.store = store;

  $scope.cancel = function cancel(store) {
    store.revert();
    $scope.$state.go('config');
  }

  $scope.saveStore = function saveStore(store) {
    ConfirmService("Save your changes to " + store.name + "?").then(function (result) {
      store.$save();
    })
  };

}])
.factory('Config', ['Type','$q','notifyService', function (Type, $q, notifyService) {

  function Config(options) {
    angular.extend(this, options);
  }

  function slugify(text)
  {
    return text.toString().toLowerCase()
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '');            // Trim - from end of text
  }

  Config.prototype = {
    get systems() {
      return _.chain(this.types).toArray().flatten().filter(function (type) {
        return !type.isNewType();
      }).pluck('system').uniq().value();
    },
    slugify: function (text) {
      return slugify.call(null, text);
    },
    updateIcon: function(category, icon) {
      var self = this;
      $q.all(_.chain(self.types[category]).filter(function (type) {
        return !type.isNewType();
      }).map(function (val) {
        var copy = angular.copy(val);
        copy.icon = icon;
        copy.category = copy.oldType.category;
        copy.active = copy.oldType.active;
        copy.description = copy.oldType.description;
        copy.system = copy.oldType.system;
        copy.profit = copy.oldType.profit;
        copy.prediction = copy.oldType.prediction;
        return copy.$update();
      }).value()).then(function (resp) {
        notifyService("Updated icon for " + category+'.', 'alert-success' );
        self.types[category] = _.map(self.types[category], function (val) {
          val.icon = icon;
          return val;
        });
      }, function (err) {
        notifyService("Could not update icon for " + category+'.', 'alert-danger' );        
      });
    },
    addCategory: function(category, icon) {
      var category_slug = slugify(category);
      this.categories.push({category:category, category_slug:category_slug, icon:icon});
      this.types[category] = [];
      return category_slug;
    },
    removeCategory: function (category) {
      var index = this.categories.indexOf(_.findWhere(this.categories, {category: category}));
      if(index > -1) {
        this.categories.splice(index, 1);
        delete this.types[category];
      }
    },
    isThereNewType: function (category) {
      return (_.filter(this.types[category], function (type) {
        return type.isNewType();
      }).length > 0);
    },
    getNewType: function (category) {
      return _.find(this.types[category], function (type) {
        return type.isNewType();
      });
    },
    addType: function (category, category_slug, icon) {
      var x = Type.build({
        category: category,
        category_slug: category_slug,
        active:true,
        icon: icon
      });
      this.types[category].push(x);
      return x;
    },
    removeType: function (type) {
      var self = this;
      var i = self.types[type.category].indexOf(type);
      if(i > -1 && type.isNewType()) {
        self.types[type.category].splice(i,1);
      } else if(i > -1) {
        type.revert();
      }
    },
    getType: function (category_slug, id) {
      var category = _.findWhere(this.categories, {category_slug: category_slug}).category;
      return _.findWhere(this.types[category], {_id: id});
    },
    isInvalidCategory: function(category) {
      return angular.isDefined(_.findWhere(this.categories,{category: category})) || !angular.isString(category) || category.length <= 0;
    },
    hasCategory: function (category_slug) {
      var self = this;
      return angular.isDefined(_.findWhere(self.categories, {category_slug: category_slug}));
    },
    hasType: function (category_slug, id) {
      var self = this;
      if(self.hasCategory(category_slug)) {
        var cat = _.findWhere(self.categories, {category_slug: category_slug});
        return angular.isDefined(_.findWhere(self.types[cat.category], { _id: id }));
      } else {
        return false;
      }
    },
    isValidType: function (type) {
      return (_.chain(this.types[type.category]).pluck('description').filter(function (description) {
        return description === type.description;
      }).value().length <= 1);
    }
  }

  Config.build = function(types) {
    if(angular.isArray(types)) {
      var categories = _.chain(types).map(function (type) {
        return {
          icon: type.icon,
          category_slug: type.category_slug,
          category: type.category
        }
      }).uniq(false, 'category_slug').value();
      types = _.chain(types).map(Type.build).groupBy('category').value();
      return new Config({types:types, categories: categories});
    }
  }

  return Config;

}]);
