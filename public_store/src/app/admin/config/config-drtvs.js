angular.module('config.drtv', [
  'filters',
  'hdirectives'
])
.directive('typeProfit', [function () {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      cost:'=',
      active:'=',
      commission:'=',
      label:'@',
      reference:'@',
      bold:'=',
      result:'='
    },
    templateUrl: 'admin/config/templates/type-profit.html',
    link: function($scope, element, attrs) {

      $scope.reference = parseInt($scope.reference,10);

    }
  };
}])
.directive('typeTotalProfit', [function () {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'admin/config/templates/type-total-profit.html',
    scope: {
      profit:'=',
      referenceTax:'=',
      referenceAmount:'=',
      referenceServicecharge:'='
    },
    link: function($scope, element, attrs) {

      $scope.getProfit = function getProfit(profit) {
        var x = 0;
        if(profit.has_amount) {
          x = x + (($scope.referenceAmount-profit.amount_fixed)*(profit.amount_comm/100));
        }
        if(profit.has_servicecharge) {
          x = x + ($scope.referenceServicecharge-profit.servicecharge_fixed)*(profit.servicecharge_comm/100);
        }
        if(profit.has_tax) {
          x = x + ($scope.referenceTax-profit.tax_fixed)*(profit.tax_comm/100);
        }
        return x - profit.fixed_cost;
      }


    }
  };
}])
.directive('typePrediction', [function() {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'admin/config/templates/type-prediction.html',
    link: function($scope, element, attrs) {

      $scope.sortField = 'amount';

      $scope.sort = function sort(fieldName) {
        if ($scope.sortField === fieldName) {
          $scope.reverse = !$scope.reverse;
        } else {
          $scope.sortField = fieldName;
          $scope.reverse = false;
        }
      };

      $scope.isSortUp = function isSortUp(fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
      };
    
      $scope.isSortDown = function isSortDown(fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
      };      

    }
  };
}])

;