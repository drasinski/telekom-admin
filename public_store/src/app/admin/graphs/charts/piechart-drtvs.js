angular.module('piechart.directive',[
  'nvd3ChartDirectives',
  'hdirectives'
])
.directive('pieChart', [ function () {
  return {
    restrict: 'E',
    scope: {
      piechart:'=',
      opens:'@',
      colorfunction:'&'
    },
    templateUrl: 'admin/graphs/charts/templates/piechart.html',
    link: function ($scope, iElement, iAttrs) {

      $scope.xFunction = function xFunction(){
        return function (d){
          return d.key;
        };
      };

      $scope.yFunction = function yFunction(){
        return function (d){
          return d.profit;
        };
      };

      $scope.toolTipContentFunction = function toolTipContentFunction(){
        return function (key, x, y, e, graph) {
          return  '<div class="nvtooltip xy-tooltip"><h3>' + key + '</h3>' +'<p>' +'$' + x + '</p></div>';
        };
      };

      $scope.$watch('piechart.filters', function (nv, ov) {
        if(!angular.equals(nv,ov)) {
          $scope.piechart.applyFilters();
        }
      }, true);

    }
  };
}]);

