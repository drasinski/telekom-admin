angular.module('piechart', [
  'invoice.srvcs.profit',
  'resource.invoices',
  'invoice.filters',
  'piechart.directive',
  'ajoslin.promise-tracker'
])
.factory('Piechart', ['InvoiceProfits','promiseTracker','InvoiceService','$filter', function (InvoiceProfits, promiseTracker, InvoiceService, $filter) {

  function Piechart(invoicedata) {
    angular.extend(this, invoicedata);
    this.promiseTracker = promiseTracker();
    this.getFilterValues();
    this.applyFilters();
  }

  function money_round(num) {
    return Math.ceil(num * 100) / 100;
  }

  function filterData(data, filters) {
    return _.filter(data, function (value) {
      return _.contains(filters.location, value.location) && _.contains(filters.processed_by, value.processed_by);
    });
  }

  function formatData(data, filters, types) {
    var filteredData = filterData(data, filters);
    if(filteredData.length <= 0) {
      return [];
    } else {
      return _.map(types, function (type) {
        return {
          'key':type.name,
          'profit':money_round(_.reduce(filteredData, function (memo,val) {
              return memo+(val[type.id]||0);
            },0))
        };
      });
    }
  }

  Piechart.prototype = {
    applyFilters: function() {
      this.formattedData = formatData(this.data, this.filters, this.types);
    },
    getFilterValues: function() {
      var self = this;
      self.filters = {
        location: _.pluck(self.locations,'id'),
        processed_by: _.pluck(self.roles,'id')
      }
    },
    getTotal: function() {
      var self = this;
      return _.reduce(self.formattedData, function (memo, val) {
        return memo + (val.profit||0);
      },0);
    },
    getInvoices: function() {
      var self = this;
      var promise = InvoiceService.get({created_date:'{gte}'+moment(self.daterange.startDate).format()+'{lte}'+moment(self.daterange.endDate).format()});
      self.promiseTracker.addPromise(promise);
      promise.then(function (invoices) {
        angular.extend(self, InvoiceProfits.build(invoices, self.daterange));
        self.getFilterValues();
        self.applyFilters();
      });
    }
  }

  Piechart.build = function(invoices, daterange) {
    if(angular.isObject(daterange)) {
      invoices = $filter('daterangefilter')(invoices, daterange.startDate, daterange.endDate);
    }
    return new Piechart(InvoiceProfits.build(invoices, daterange));
  }

  return Piechart;

}])