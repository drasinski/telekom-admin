angular.module('barchart.directive',[
  'nvd3ChartDirectives',
  'hdirectives',
  'filters'
])
.directive('barChart', ['$filter', function ($filter) {
  return {
    restrict: 'E',
    scope: {
      barchart:'=',
      colorfunction:'&'
    },
    templateUrl: 'admin/graphs/charts/templates/barchart.html',
    link: function ($scope, iElement, iAttrs) {

      $scope.toolTipContentFunction = function toolTipContentFunction(x_axis) {
        return function (key,x,y,e,graph) {
          var str = {
            'location':y+' at '+x,
            'type':y+' for '+x,
            'day_of_week':y+' on '+x,
            'processed_by':x+' made ' + y,
            'start_of_week':y+' from '+x,
            'start_of_month':y+ ' in ' + x,
            'start_of_day':y+ ' on ' + x
          };
          return '<div class="nvtooltip xy-tooltip"><h3>' + key + '</h3>' + '<p>' + str[x_axis] + '</p></div>';
        };
      };

      $scope.xAxisTickFormatFunction = function xAxisTickFormatFunction(x_axis) {
        return function (x) {
          var str = {
            'location':x,
            'type':x,
            'day_of_week':x+'s',
            'processed_by':x,
            'start_of_week':moment(x).format("MMM. Do") + " - " + moment(x).add(6,'days').format("MMM. Do"),
            'start_of_month':moment(x).format("MMM. YYYY"),
            'start_of_day':moment(x).format('ddd MMM. Do')
          };
          return str[x_axis];
        };
      };

      $scope.yAxisTickFormatFunction = function yAxisTickFormatFunction() {
        return function (d) {
          return $filter('noFractionCurrency')(d);
        };
      };

      $scope.yFunction = function yFunction(){
        return function (d){
          return d.profit;
        };
      };

      $scope.xFunctionBar = function xFunctionBar(){
        return function (d){
          return d['x_value'];
        };
      };

      $scope.$watch('barchart.filters',function (nV,oV) {
        if(!angular.equals(nV,oV)) {
          $scope.barchart.applyFilters();
        }
      },true);

      $scope.$watch('barchart.settings', function (nV,oV) {
        if(!angular.equals(nV,oV)) {
          $scope.barchart.applyFilters();
        }
      }, true);

    }
  };
}]);