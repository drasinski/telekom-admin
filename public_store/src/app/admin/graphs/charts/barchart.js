angular.module('barchart', [
  'invoice.srvcs.profit',
  'ajoslin.promise-tracker',
  'resource.invoices',
  'invoice.filters',
  'barchart.directive'
])
.factory('Barchart', [ 'InvoiceProfits', 'promiseTracker','InvoiceService','$filter', function (InvoiceProfits, promiseTracker, InvoiceService, $filter) {

  function Barchart(data) {
    angular.extend(this, data);
    this.x_axis_options = [
      {"id":"type","name":"Type"},
      {"id":"processed_by","name":"Users"},
      {"id":"day_of_week","name":"Day"},
      // {"id":"week_of_month","name":"Week of the Month"},
      {"id":"start_of_day","name":"Consecutive Days"},
      {"id":"start_of_week","name":"Consecutive Weeks"},
      {"id":"start_of_month","name":"Consecutive Months"},
      {"id":"location","name":"Locations"}
    ];
    this.settings = {
      color_by:'type',
      x_axis:'start_of_day'
    }
    this.promiseTracker = promiseTracker();
    this.getFilterValues();
    this.applyFilters();
  }

  function filterData(data, filters) {
    return _.filter(data, function (value) {
      return _.contains(filters.location, value.location) && _.contains(filters.processed_by, value.processed_by) && _.contains(filters.day_of_week, value.day_of_week);
    });
  }

  function formatData(data, settings, filters) {
    var filteredData = filterData(data,filters);
    if(settings.color_by === 'type') {
      return _.map(filters.type, function (val_type) {
        return {
          key: val_type,
          values: _.chain(filteredData).groupBy(settings.x_axis).map(function (invoices, x_key) {
            return {
              profit: _.reduce(invoices, function (memo, val) {
                return memo + (val[val_type] || 0);
              },0),
              x_value: x_key
            }
          }).value()
        }
      });
    } else {
      return _.chain(data).pluck(settings.color_by).uniq().map(function (val_type) {
        return {
          key: val_type,
          values: findValues(filteredData, settings, val_type, filters.type)
        }
      }).value();
    }
  }

  function findValues(data, settings, filter_val, y_type) {
    if(settings.x_axis === 'type') {
      return _.chain(y_type).map(function (x_value) {
        return {
          profit: _.chain(data).filter(function (invoice) {
            return invoice[settings.color_by] === filter_val;
          }).reduce(function (memo, val) {
            return memo + (val[x_value]||0);
          },0).value(),
          x_value: x_value
        }
      }).value();
    } else {
      return _.chain(data).groupBy(settings.x_axis).map(function (invoices, x_value_key) {
        return {
          profit: _.chain(invoices).filter(function (invoice) {
            return invoice[settings.color_by] === filter_val;
          }).reduce(function (memo, val) {
            angular.forEach(y_type, function (y_type_key) {
              memo = memo + (val[y_type_key] || 0);
            });
            return memo;
          },0).value(),
          x_value: x_value_key
        }
      }).value()
    }
  }

  Barchart.prototype = {
    getFilterValues: function() {
      var self = this;
      self.filters = {
        location: _.pluck(self.locations, 'id'),
        processed_by: _.pluck(self.roles, 'id'),
        day_of_week: _.pluck(self.day_of_week, 'id'),
        type: _.pluck(self.types, 'id')
      }
    },
    applyFilters: function() {
      var self = this;
      if(self.settings.x_axis.indexOf("start_") > -1) {
        if(self.settings.color_by === 'day_of_week') {
          self.settings.color_by = 'type';
        }
        self.color_by_options = [{"id":"type","name":"Type"},{"id":"processed_by","name":"Users"},{"id":"location","name":"Locations"}];
      } else {
        self.color_by_options = _.filter([{"id":"type","name":"Type"},{"id":"processed_by","name":"Users"},{"id":"day_of_week","name":"Day"},{"id":"location","name":"Locations"}], function (value) {
          return value.id !== self.settings.x_axis;
        });
        if(self.settings.x_axis === self.settings.color_by) {
          self.settings.color_by = self.color_by_options[0].id;
        }
      }
      self.formattedData = formatData(self.data, self.settings, self.filters);
    },
    getInvoices: function () {
      var self = this;
      var promise = InvoiceService.get({created_date:'{gte}'+moment(self.daterange.startDate).format()+'{lte}'+moment(self.daterange.endDate).format()});
      self.promiseTracker.addPromise(promise);
      promise.then(function (invoices) {
        angular.extend(self, InvoiceProfits.build(invoices, self.daterange));
        self.getFilterValues();
        self.applyFilters();
      }); 
    }
  }

  Barchart.build = function (invoices, daterange) {
    if(angular.isObject(daterange)) {
      invoices = $filter('daterangefilter')(invoices, daterange.startDate, daterange.endDate);
    }
    return new Barchart(InvoiceProfits.build(invoices, daterange));
  };


  return Barchart;

}])