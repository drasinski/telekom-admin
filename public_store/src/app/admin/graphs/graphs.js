angular.module( 'admin.graphs', [
  'ui.router',
  'security.authorization',
  'piechart',
  'barchart',
  'resource.invoices',
  'constants'
])
.config([ '$stateProvider','securityAuthorizationProvider','STORE', function config( $stateProvider, securityAuthorizationProvider, STORE) {
  $stateProvider.state( 'graphs', {
    url: '/'+STORE.slug+'/graphs',
    views: {
      "main": {
        controller: 'ProfitCtrl',
        templateUrl: 'admin/graphs/templates/graphs.html'
      }
    },
    resolve: {
      currentUser: securityAuthorizationProvider.requireAdminUser,
      invoices: ['InvoiceService', function (InvoiceService) {
        var startDate = moment().startOf("month").subtract(13,'days').format('MM-DD-YYYY');
        var endDate = moment().startOf('day').add(1,'days').format('MM-DD-YYYY');
        return InvoiceService.get({created_date:'{gte}'+startDate+'{lte}'+endDate});
      }],
      piechart: ['invoices','Piechart', function (invoices, Piechart) {
        return {
          day: Piechart.build(invoices, {startDate:moment().startOf('day'), endDate: moment().add(1,'days')}),
          week: Piechart.build(invoices, {startDate:moment().startOf('week'), endDate: moment().add(1,'days')}),
          month: Piechart.build(invoices, {startDate:moment().startOf('month'), endDate: moment().add(1,'days')})
        };
      }],
      barchart: ['invoices','Barchart', function (invoices, Barchart ) {
        return Barchart.build(invoices, {startDate: moment().startOf("day").subtract(12,'days'), endDate:moment().add(1,'days').startOf("day")});
      }]
    },
    data: {
      pageTitle: 'Graph'
    }
  });
}])
.controller('ProfitCtrl', ['$scope','GRAPH_COLORS','piechart', 'barchart', function ($scope, GRAPH_COLORS, piechart, barchart) {

    $scope.colorFunction = function colorFunction() {
      return function (d, i) {
        return GRAPH_COLORS[i];
      };
    };

    $scope.piechart = piechart;
    $scope.barchart = barchart;

}])
;

