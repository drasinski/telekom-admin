angular.module('resource.income', [
  'constants',
  'service.xhr'
])
.factory('IncomeService', ['xhr', 'STORE', 'apiPrefix', function (xhr, STORE, apiPrefix) {
  return {
    get: function (query) {
      var x = angular.extend({per_page:'0', sort_by:'created_date'},query);
      return xhr([STORE.storePrefix+'/incomes',{params:x, cache:false}]);
    },
    put: function () {
      return xhr('put',[apiPrefix+this.store.storeId+'/incomes/'+this._id, this]);
    },
    post: function() {
      return xhr('post',[apiPrefix+this.store.storeId+'/incomes', this]);
    }
  }
}]);

