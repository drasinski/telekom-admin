angular.module('income-year.drtv', [
  'constants',
  'filters',
  'hdirectives',
  'resource.income',
  'ui.bootstrap.collapse',
  'ajoslin.promise-tracker',
  'nvd3ChartDirectives'
])
.directive('incomeYearGraph', [ '$filter','GRAPH_COLORS', function ($filter, GRAPH_COLORS) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl:'admin/income/templates/income-year-graph.html',
    link: function ($scope, iElement, iAttrs) {

      $scope.xFunction = function xFunction() {
        return function (d){
          return d.date;
        };
      }

      $scope.yFunction = function yFunction(){
        return function (d){
          return d.net; 
        };
      };

      $scope.colorFunction = function colorFunction() {
        return function (d, i) {
          return GRAPH_COLORS[i];
        }
      };

      $scope.xAxisTickFormatFunction = function xAxisTickFormatFunction() {
        return function (x) {
          return moment(x,'M').format("MMM");
        }
      };

      $scope.yAxisTickFormatFunction = function yAxisTickFormatFunction() {
        return function (d) {
          return $filter('noFractionCurrency')(d);
        };
      };

      // $scope.toolTipContentFunction = function toolTipContentFunction(){
      //   return function (key, x, y, e, graph) {
      //     return '<div class="nvtooltip xy-tooltip"><h3 class="text-center">' + key + '</h3>' + '<p>' + y + " in " + x + '</p></div>';
      //   }
      // };

    }
  };
}])
.directive('incomeYearSummary', [ function () {
  return {
    restrict: 'E',
    replace: true,
    templateUrl:'admin/income/templates/income-yearly-summary.html',
    scope: {
      incomeData:'='
    },
    link: function ($scope, iElement, iAttrs) {


      $scope.sort = function sort(fieldName) {
        if ($scope.sortField === fieldName) {
          $scope.reverse = !$scope.reverse;
        } else {
          $scope.sortField = fieldName;
          $scope.reverse = false;
        }
      };

      $scope.isSortUp = function isSortUp(fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
      };

      $scope.isSortDown = function isSortDown(fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
      };

    }
  };
}])
