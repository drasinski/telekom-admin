angular.module('income.drtv', [
  'constants',
  'filters',
  'hdirectives',
  'invoice.srvcs',
  'resource.invoices',
  'resource.income',
  'service.notify',
  'ui.bootstrap.collapse',
  'ajoslin.promise-tracker'
])
.directive('incomeListDynamic', [ function () {
  return {
    restrict: 'E',
    replace:true,
    scope: {
      data:'=',
      header:'@',
      onSave:'&',
      locations:'='
    },
    templateUrl: 'admin/income/income-details/templates/income-list-dynamic.html',
    link: function($scope, el, attrs) {

      $scope.isCollapsed = true;
      var initData;
      $scope.$watch('data', function (nv,ov) {
        if(angular.equals(nv,ov)) {
          initData = angular.copy(nv);
        }
      });

      $scope.cancel = function cancel() {
        $scope.data = angular.copy(initData);
      };

      $scope.clickSave = function clickSave(data) {
        $scope.onSave().then(function (success) {
          initData = angular.copy(data);
          console.log("Successfully saved income");
        });
      };

      $scope.getTotal = function getTotal(data) {
        return _.reduce(data, function (memo, val) {
          return memo + val.amount;
        },0);
      }

      $scope.showSave = function showSave(data) {
        return !angular.equals(data,initData);
      };

      $scope.addRow = function addRow() {
        $scope.data.push({});
        $scope.sortField = undefined;
      };

      $scope.removeRow = function removeRow(idx) {
        if(idx >= 0 && idx < $scope.data.length) {
          $scope.data.splice(idx,1);
        };
      };

      $scope.sort = function sort(fieldName) {
        if ($scope.sortField === fieldName) {
          $scope.reverse = !$scope.reverse;
        } else {
          $scope.sortField = fieldName;
          $scope.reverse = false;
        }
      };

      $scope.isSortUp = function isSortUp(fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
      };

      $scope.isSortDown = function isSortDown(fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
      };


    }
  };
}])
.directive('incomeListStatic', [ function () {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      data: '=',
      header: '@',
      onSave:'&',
      date:'='
    },
    templateUrl:'admin/income/income-details/templates/income-list-static.html',
    link: function ($scope, iElement, iAttrs) {

      $scope.isCollapsed = true;

      $scope.clickSave = function clickSave(data) {
        $scope.onSave().then(function (success) {
          console.log("Successfully saved income data")
        });
      };

      $scope.sort = function sort(fieldName) {
        if ($scope.sortField === fieldName) {
          $scope.reverse = !$scope.reverse;
        } else {
          $scope.sortField = fieldName;
          $scope.reverse = false;
        }
      };

      $scope.isSortUp = function isSortUp(fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
      };

      $scope.isSortDown = function isSortDown(fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
      };

      $scope.getTotal = function getTotal(data) {
        return _.reduce(data, function (memo,val) {
          return memo + val.amount;
        },0);
      }

    }
  }
}])
.directive('incomeListSummary', [ function () {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      data: '=',
      header: '@'
    },
    templateUrl:'admin/income/income-details/templates/income-list-summary.html',
    link: function ($scope, iElement, iAttrs) {


      $scope.sort = function sort(fieldName) {
        if ($scope.sortField === fieldName) {
          $scope.reverse = !$scope.reverse;
        } else {
          $scope.sortField = fieldName;
          $scope.reverse = false;
        }
      };

      $scope.isSortUp = function isSortUp(fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
      };

      $scope.isSortDown = function isSortDown(fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
      };

      $scope.getTotal = function getTotal(data, key) {
        return _.reduce(data, function (memo, val) {
          return memo + (val[key]||0);
        },0);
      }
    }
  };
}])