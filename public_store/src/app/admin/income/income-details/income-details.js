angular.module( 'admin.income.income-details', [
  'ui.router',
  'security.authorization',
  'constants',
  'service.confirm',
  'service.notify',
  'resource.income',
  'resource.invoices',
  'invoice.srvcs.profit',
  'store.srvcs',
  'ui.bootstrap.tooltip',
  'income.drtv'
])
.config([ '$stateProvider','securityAuthorizationProvider','STORE', function config( $stateProvider, securityAuthorizationProvider, STORE) {
  $stateProvider.state('income.details', {
    url: '/{month:(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)}',
    templateUrl: 'admin/income/income-details/templates/income-details.html',
    controller: 'IncomeDetailsCtrl',
    resolve: {
      incomeData: ['$stateParams','IncomeService','InvoiceService','$q','Income', function ($stateParams, IncomeService, InvoiceService, $q, Income) {
        var startDate = moment().year($stateParams.year).month($stateParams.month).startOf('month');
        // var endDate = moment().year($stateParams.year).month($stateParams.month).startOf('month').add(1,'months').add(1,'days').startOf('day');
        var endDate = moment().year($stateParams.year).month($stateParams.month).endOf('month');
        var invQuery = {created_date:'{gte}'+startDate.format()+'{lte}'+endDate.format()};
        var incQuery = {date:'{gte}'+startDate.endOf('month').format('MMM-YYYY')+'{lte}'+endDate.format('MMM-YYYY')};
        return $q.all([InvoiceService.get(invQuery),IncomeService.get(incQuery)]).then(function (response) {
          return Income.build(response[1], response[0], moment().year($stateParams.year).month($stateParams.month).format('MMM-YYYY'));
        });
      }]
    }
  });
}])
.controller('IncomeDetailsCtrl', ['$scope','incomeData','ConfirmService','$q', function ($scope, incomeData, ConfirmService, $q) {
  $scope.activeDate = moment().year($scope.$stateParams.year).month($scope.$stateParams.month).format();
  $scope.move = function move(direction, date) {
    var ref = moment(date).add(direction,'months');
    $scope.$state.go('income.details',{month:ref.format("MMM"), year: ref.year()});
  };

  $scope.incomeData = incomeData;

  $scope.leftTooltip = moment($scope.activeDate).add(-1,'months').format();
  $scope.rightTooltip = moment($scope.activeDate).add(1,'months').format();

  $scope.saveIncome = function saveIncome(incomeData) {
    return ConfirmService("Are you sure you want to save?").then(function() {
      return incomeData.$save().then(function (income) {
        return $q.when(income);
      }, function (err) {
        return $q.reject();
      });
    });
  };

}])
.factory('Income', [ 'IncomeService', 'InvoiceProfits','STORE', 'Store', 'notifyService', '$q', function (IncomeService, InvoiceProfits, STORE, Store, notifyService, $q) {

  function Income(options) {
    _.defaults(this, options, {other_income:[], expenses:[], income:[]});
    var self = this;
    self.summary = _.map(self.store.locations, function (val) {
      return {
        locationId: val.id,
        name: val.name,
        get expenses() {
          return reduce.call(self.expenses, val.id)
        },
        get income() {
          return reduce.call(self.other_income.concat(self.income), val.id)
        },
        get net() {
          return this.income - this.expenses;
        }
      }
    });
  }

  function reduce(id) {
    return _.reduce(this, function (memo, val) {
      if(val.locationId === id) {
        memo = val.amount + memo;
      }
      return memo;
    },0);
  }

  function formatInvoices() {
    var temp = {};
    var self = this;
    angular.forEach(this.locations, function (val_loc) {
      temp[val_loc.id]={};
      angular.forEach(self.types, function (val_type) {
        temp[val_loc.id][val_type.id] = {
          total:0,
          icon: val_type.icon,
          name: val_type.name
        };
      });
    });
    angular.forEach(self.data, function (val_inv) {
      angular.forEach(self.types, function (val_type) {
        temp[val_inv.location][val_type.id].total = temp[val_inv.location][val_type.id].total + (val_inv[val_type.id] || 0);
      });
    });
    var last = [];
    angular.forEach(Object.keys(temp), function (val_loc) {
      var obj = {locationId:val_loc};
      angular.forEach(Object.keys(temp[val_loc]), function (val_type) {
        obj.description = val_type;
        obj.name = temp[val_loc][val_type].name;
        obj.icon = temp[val_loc][val_type].icon;
        obj.amount = temp[val_loc][val_type].total;
        obj.locationName = _.findWhere(STORE.locations, {id: obj.locationId}).name;
        last.push(angular.copy(obj));
      });
    });
    return last;
  }

  function deserialize(income) {
    delete income.summary;
    return income;
  }

  Income.prototype = {
    isNewIncome: function() {
      return angular.isUndefined(this._id);
    },
    $save: function() {
      var self = this;
      if(this.isNewIncome()) {
        return IncomeService.post.call(self).then(function (resp) {
          notifyService("Succesfully created new income data.", 'alert-success');
          self._id = resp._id;
          return resp;
        }, function (err) {
          notifyService("Could not create new income data, please try again.", 'alert-danger');
          return $q.reject(err);
        });
      } else {
        return IncomeService.put.call(self).then(function (resp) {
          notifyService("Succesfully updated income data.", 'alert-success');
          return resp;
        }, function (err) {
          notifyService("Could not update income data, please try again.", 'alert-danger');
          return $q.reject(err);
        });
      }
    },
    getLocName: function(locationId) {
      return _.findWhere(this.store.locations, {id:locationId}).name;
    },
    applyInvoices: function (invoices) {
      this.income = formatInvoices.call(InvoiceProfits.build(invoices));
    },
    getTotal: function (data, key) {
      return _.reduce(data, function (val, memo) {
        memo = val[key]+memo
        return memo;
      })
    }
  }

  Income.build = function(income, invoices, date) {
    if(angular.isArray(income)) {
      income = (income[0] || {});
    }
    if(angular.isArray(invoices)) {
      income.income = formatInvoices.call(InvoiceProfits.build(invoices));
      // income.income = _.map(income.income, function (val) {
      //   val.locationName = _.findWhere(STORE.locations, {id: val.locationId}).name;
      //   return val;
      // })
    }
    if(angular.isDefined(income)) {
      income.store = Store.build(STORE);
    }
    if(angular.isDefined(date)) {
      income.date = date;
    }
    return new Income(deserialize(income));
  }

  return Income;

}])
