angular.module( 'admin.income', [
  'admin.income.income-details',
  'income-year.drtv',
  'ui.router',
  'security.authorization',
  'constants',
  'service.confirm',
  'ui.bootstrap.tooltip',
  'resource.income'
])
.config([ '$stateProvider','securityAuthorizationProvider','STORE', function config( $stateProvider, securityAuthorizationProvider, STORE) {
  $stateProvider.state( 'income', {
    url: '/'+STORE.slug+'/income/{year:(?:19|20)[0-9]{2}}',
    views: {
      "main": {
        controller: 'IncomeSummaryCtrl',
        templateUrl: 'admin/income/templates/income.html'
      }
    },
    resolve: {
      authUser: securityAuthorizationProvider.requireAdminUser,
      incomeData: ['IncomeSummary','IncomeService','$stateParams', function (IncomeSummary, IncomeService, $stateParams) {
        $stateParams.year = $stateParams.year || new Date().getFullYear();
        return IncomeService.get({date:'{gte}'+moment().year($stateParams.year).startOf('year').format('MMM-YYYY')+'{lte}'+moment().year($stateParams.year).endOf('year').format('MMM-YYYY')}).then(function (incomes) {
          return IncomeSummary.build(incomes,$stateParams.year);
        });
      }]
    },
    data: {
      pageTitle: 'Income'
    }
  })
  .state('income.year', {
    url: '/summary',
    controller: 'IncomeYearSummaryCtrl',
    templateUrl: 'admin/income/templates/income-year.html',
    data: {
      pageTitle: 'Income Summary'
    }
  });
}])
.controller('IncomeYearSummaryCtrl', ['$scope', 'incomeData', function ($scope, incomeData) {

  $scope.move = function move(direction, date) {
    var year = moment(date).add(direction,'years').format('YYYY');
    $scope.$state.go('income.year',{year:year})
  };

  $scope.incomeData = incomeData;

}])
.controller('IncomeSummaryCtrl', ['$scope','incomeData', function ($scope, incomeData) {

  $scope.goMonth = function goMonth(date) {
    var month = moment(date).format('MMM');
    $scope.$state.go('income.details',{month:month})
  };

  $scope.move = function move(direction, date) {
    var year = moment(date).add(direction,'years').format('YYYY');
    $scope.$state.go('income',{year:year})
  };

  $scope.data = incomeData;

  $scope.rows = _.chain($scope.data.incomes).groupBy(function (ele,idx){return Math.floor(idx/3)}).toArray().value();

}])
.factory('IncomeMonth', ['Store',function (Store) {

  function IncomeMonth(options) {
    angular.extend(this, options);
  }

  function reduce_wrapper(key, locations) {
    return _.reduce(this, function (memo, val) {
      if(_.contains(locations, val.locationId)) {
        return memo + val[key];
      } else {
        return memo;
      }
    }, 0)
  }

  IncomeMonth.prototype = {
    get expenses_total() {
      return this.total('expenses', _.pluck(this.store.locations, 'id'));
    },
    get income_total() {
      return this.total('income', _.pluck(this.store.locations, 'id'));
    },
    get net_total() { 
      return this.total('net', _.pluck(this.store.locations, 'id'));
    },
    total: function (key, locations) {
      if(key === 'net') {
        return this.total('income',locations) - this.total('expenses', locations);
      } else {
        return reduce_wrapper.call(this.summary, key, locations);
      }
    },
    isThisMonthAndYear: function() {
      return moment().isSame(this.date,'month') && moment().isSame(this.date,'year');
    },
    getLocName: function(locationId) {
      return _.findWhere(this.store.locations, {id:locationId}).name;
    }
  }

  IncomeMonth.build = function (income) {
    income.store = Store.build(income.store);
    return new IncomeMonth(income);
  } 

  return IncomeMonth;

}])
.factory('IncomeSummary',['IncomeMonth','Store', function (IncomeMonth, Store) {

  function IncomeSummary(options) {
    angular.extend(this, options);
    var self = this;
    self.rightTooltip = moment(self.date).add(1,'years').format();
    self.leftTooltip = moment(self.date).add(-1,'years').format();
    self.summary = _.map(self.store.locations, function (val) {
      return {
        locationId: val.id,
        name: val.name,
        get expenses() {
          return _.reduce(self.incomes, function (m,income) {
            return m + income.total('expenses', [val.id])
          },0);
        },
        get income() {
          return _.reduce(self.incomes, function (m,income) {
            return m + income.total('income', [val.id])
          },0);
        },
        get net() {
          return _.reduce(self.incomes, function (m,income) {
            return m + income.total('net', [val.id])
          },0);
        }
      }
    });
    self.graph = _.map(self.store.locations, function (val) {
      return {
        key: val.name,
        values: _.map(self.incomes, function (income) {
          return {
            net: income.total('net',[val.id]),
            date: moment(income.date).format("M")
          }
        })
      }
    });
  }

  function reduce_wrapper(key) {
    return _.reduce(this, function (memo, val) {
      return memo + val[key];
    }, 0)
  }

  IncomeSummary.prototype = {
    get income_total() {
      return reduce_wrapper.call(this.summary,'income');
    },
    get expenses_total() {
      return reduce_wrapper.call(this.summary,'expenses');
    },
    get net_total() {
      return this.income_total - this.expenses_total;
    }
  }

  IncomeSummary.build = function (incomes, year) {
    if(angular.isArray(incomes)) {
      var data = new Array(12);
      for(var i = 0; i < 12; i++) {
        var income = _.filter(incomes, function (income) {
          return (moment(income.date).month() === i);
        });
        data[i] = IncomeMonth.build(income[0]||{date:new Date(year, i, 1), summary:[]});
      }
      return new IncomeSummary({incomes:data, date:moment().year(year).format(), store: Store.build({}) });
    }
  }

  return IncomeSummary;

}])

;
