angular.module('admin', [
  'admin.graphs',
  'admin.inventory',
  'admin.users',
  'admin.config',
  'admin.income',
  'admin.messages'
]);
