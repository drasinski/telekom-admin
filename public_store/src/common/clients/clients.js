angular.module('clients.srvcs',[
  'ajoslin.promise-tracker',
  'resource.clients',
  'invoice.srvcs',
  'service.address',
  'store.srvcs',
  'service.notify',
  'filters'
])
.provider('clientInit', {

  requireCarriers: ['clientInit', function (clientInit) {
    return clientInit.requireCarriers()
  }],

  $get: ['TypeService','Client', function ( TypeService, Client) {

    return {
      requireCarriers: function() {
        return TypeService.get({category_slug:"phone-bill"}).then(Client.initCarriers);
      }
    }

  }]

})
.factory('Client', ['$filter', '$q' ,'Store', 'ClientService','InvoiceService','getAddress','generateClientSMS','notifyService', function ($filter, $q, Store, ClientService, InvoiceService, getAddress, generateClientSMS, notifyService) {

  function Client(options) {
    angular.extend(this,options);
    angular.extend(this.store, options.store);
    this.oldClient = angular.copy(this.serialize());
    this.getSMSText();
  };

  var telephoneRegex = /^\d{10}$/;
  var mongodbRegex = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
  Client.telephoneRegex = angular.copy(telephoneRegex);
  Client.idRegex = angular.copy(mongodbRegex);

  var carriers = [];
  Client.carriers = carriers;

  var paymenttypes = [
    {id:'none', name:'None', icon:'fa-times'},
    {id:'prepaid', name:'Prepaid', icon: 'fa-mail-forward'},
    {id:'creditcard', name:'Recurring', icon:'fa-credit-card'}
  ];
  Client.paymenttypes = paymenttypes;

  var languages = [
    {id:'english',name:'English'},
    {id:'polish',name:"Polski"},
    {id:'ukraine',name:'Ukraiński'},
    {id:'spanish',name:'Español'},
    {id:'czech',name:"Czech"}
  ];
  Client.languages = languages;

  function deserialize(client) {
    if(!angular.isObject(client)) {
      client = {};
    }
    if(angular.isDefined(client.plan)) {
      client.plan = parseInt(client.plan,10);
    } else if (angular.isUndefined(client.plan)) {
      client.plan = 0;
    }
    client.language = client.language || 'english';
    client.paymenttype = client.paymenttype || 'none';
    client.carrier = client.carrier || '';
    client.address = client.address || {};
    if(angular.isDefined(client.paiduntil)) {
      client.paiduntil = new Date(client.paiduntil);
    }
    if(angular.isDefined(client.created_date)) {
      client.created_date = new Date(client.created_date);
    }
    if(angular.isDefined(client.last_modified_date)) {
      client.last_modified_date = new Date(client.last_modified_date);
    }
    if(angular.isDefined(client.clientId)) {
      client._id = client.clientId;
    }
    if(angular.isUndefined(client.invoice_page)) {
      client.invoice_page = 1;
    }
    client.searchField = (client.firstname || '') + ' ' + (client.lastname || '') + ' ' + (client.telephone);
    return client;
  }

  Client.prototype = {
    get display() {
      if(this.hasName()) {
        return $filter('capitalizeFirst')(this.firstname || '') + ' ' + $filter('capitalizeFirst')(this.lastname || '');
      } else {
        return $filter('tel')(this.telephone);
      }
    },
    get display_paymenttype() {
      var self = this;
      return _.findWhere(paymenttypes, {id:self.paymenttype}).name;
    },
    get display_language() {
      var self = this;
      return _.findWhere(languages, {id:self.language}).name;
    },
    get display_carrier() {
      var self = this;
      return _.findWhere(carriers, {id:self.carrier }).name;
    },
    get paymenttypes() {
      return paymenttypes;
    },
    get carriers() {
      return carriers;
    },
    get languages() {
      return languages;
    },
    get telephoneRegex() {
      return telephoneRegex;
    },
    get rechargeDate() {
      var date;
      if(angular.isUndefined(this.paiduntil)) {
        return false;
      }
      var paiduntil = angular.copy(moment(this.paiduntil).startOf('day'));
      if(this.paymenttype === 'creditcard') {
        date = paiduntil.subtract(3,'days');
      } else if(this.paymenttype === 'none') {
        if(!this.receive_sms) {
          return false;
        } else {
          date = paiduntil.subtract(3,'days');
        }
      } else if(this.paymenttype === 'mail') {
        date = paiduntil.subtract(14,'days');
      } else if(this.paymenttype === 'prepaid') {
        var paydate = moment(paiduntil).subtract(3,'days');
        if(Math.floor(paydate.diff(moment(),'days')/30) > 0) {
          date = paydate.subtract(Math.floor(paydate.diff(moment(),'days')/30)*30, 'days');
        // if we already made the last payment.
        } else {
          // NEED TO THINK ABOUT THIS LINE HERE
          this.paymenttype = 'none';
          date = moment(paiduntil).subtract(3,'days');
        }
      }
      return date;
    },
    filterUser: function() {
      return (!this.receive_sms && this.paymenttype === 'none') || (angular.isUndefined(this.paiuntil))
    },
    hasChanged: function() {
      return !angular.equals(this.oldClient, this.serialize());
    },
    paymentOverdue: function() {
      return moment(this.paiduntil).isBefore(moment(),'day');
    },
    getRechargePopover: function() {
      if(!this.paymentOverdue()) {
        return "Next payment due:  " + $filter('date')(this.paiduntil,'shortDate');
      } else {
        return "Payment overdue, was due:  " + $filter('date')(this.paiduntil,'shortDate');
      }
    },
    serialize: function(options) {
      var obj = {
        clientId: this._id,
        _id: this._id,
        address: this.address,
        firstname: this.firstname,
        lastname: this.lastname,
        language: this.language,
        telephone: this.telephone,
        carrier: this.carrier,
        store: this.store.serialize(),
        plan: this.plan,
        paiduntil: this.paiduntil,
        paymenttype: this.paymenttype,
        receive_sms: this.receive_sms,
        notes: this.notes
      }
      return obj;
    },
    hasName: function() {
      return !(((this.lastname==='')&&(this.firstname===''))||(angular.isUndefined(this.firstname)&&angular.isUndefined(this.lastname)));
    },
    clear: function() {
      for (prop in this) {
        if(this.hasOwnProperty(prop) && prop !== 'store') {
          delete this[prop];
        }
      }
    },
    changeRechargeDate: function(direction) {
      if(!this.paymentOverdue()) {
        if(direction <= -1 && moment(this.paiduntil).isSame(moment().add(30,'days'),'day')) {
          this.paiduntil = moment(this.oldClient.paiduntil).format();
        } else {
          this.paiduntil = moment(this.paiduntil).add(direction*30,'days').format();  
        }
      } else {
        this.paiduntil = moment().add(direction*30,'days').format();
      }
    },
    isNewClient: function () {
      return angular.isUndefined(this._id) && telephoneRegex.test(this.telephone);
    },
    isExistingClient: function() {
      return !this.isNewClient() && mongodbRegex.test(this._id) && telephoneRegex.test(this.telephone);
    },
    isClient: function() {
      return this.isNewClient() || this.isExistingClient();
    },
    hasPlanAndCarrier: function() {
      return angular.isNumber(this.plan) && angular.isDefined(this.carrier);
    },
    watchClientCarrier: function(transaction) {
      if(this.isClient() && (transaction.category_slug === 'phone-bill') || (transaction.category_slug === 'activation')) {
        this.carrier = transaction.typeId.description;
      }
      if(angular.isDefined(transaction.amount)) {
        this.watchClientPlan(transaction);
      }
    },
    watchClientPlan: function(transaction) {
      if(this.isClient() && (transaction.category_slug === 'phone-bill')) {
        this.plan = transaction.amount;
      }
    },
    onSelectAddress: function (address) {
      angular.extend(this.address, address);
    },
    getSMSText: function() {
      this.message = generateClientSMS.call(this);
    },
    messageSent: function() {
      var self = this;
      return (_.filter(self.messages_sent, function (msg_info) {
        var x = (msg_info.message_type === 'Prepaid') || (msg_info.message_type === 'Upcoming');
        return x && moment(msg_info.for_date).isSame(self.rechargeDate,'day');
      })).length > 0;
    },
    $predictAddress: function(address) {
      return getAddress(address);
    },
    $getInvoices: function(query) {
      return InvoiceService.get(angular.extend({'client.clientId':this._id},query));
    },
    $getInvoicesCount: function() {
      var self = this;
      if(angular.isUndefined(self.invoice_count)) {
        return ClientService.getClientInvoiceCount.call(self).then(function (data) {
          self.invoice_count = data.invoice_count;
          return data.invoice_count;
        });
      } else {
        return $q.when(self.invoice_count);
      }
    },
    $save: function(options) {
      var self = this;
      if(self.isNewClient()) {
        return ClientService.post.call(self.serialize()).then(function (resp) {
          self._id = resp._id;
          self.oldClient = angular.copy(self.serialize());
          notifyService("Successfully created new client:  " + self.display +'.', 'alert-success');
          return resp;
        }, function (err) {
          notifyService("Could not create "+ self.display +'.', 'alert-danger');
          return $q.reject(err);
        });
      } else if(self.isExistingClient() && self.hasChanged()) {
        return ClientService.put.call(self.serialize()).then(function (client) {
          notifyService(self.display + " was successfully updated.", 'alert-success');
          if(angular.isArray(self.invoices)) {
            _.map(self.invoices, function (invoice) {
              angular.extend(invoice.client, client);
              return invoice;
            });
          }
          self.oldClient = angular.copy(self.serialize());
          return client;
        }, function (err) {
          notifyService(self.client+" could not be updated. Please try again", 'alert-danger');
          return $q.reject(err);
        });
      } else {
        return $q.when(self.serialize());
      }
    },
    $remove: function() {
      if(this.isExistingClient()) {
        var self = this;
        return ClientService['delete'].call(self).then(function (client) {
          notifyService(self.display + " was successfully removed.", 'alert-success');
          return client
        }, function (err) {
          notifyService(self.display + " could not be removed, please try again.", 'alert-danger');
          return $q.reject(err);
        });
      }
    },
    //{
    //  message_type: 
    //  message: 
    //  for_date:
    //}
    $sendSMS: function(options) {
      var self = this;
      if(self.isClient()) {
        options.from = self.store.twilio_number;
        return ClientService.sms.call(self,options).then(function (resp) {
          self.messages_sent = resp.messages_sent;
          notifyService("Successfully sent message to "+ self.display +'.', 'alert-success');
          return resp;
        }, function (err) {
          notifyService("Message could not be sent to "+ self.display+'.', 'alert-danger' );
          return $q.reject(err);
        });
      }
    }
  };

  Client.build = function (options) {
    options.store = Store.build(options.store || {});
    return new Client(deserialize(options));
  };

  Client.initCarriers = function (types) {
    if(angular.isArray(types)) {
      carriers = _.map(types, function (type) {
        return {id: type.description, name: type.description};
      });
      carriers.unshift({name:'No Carrier', id: ''});
      return carriers;
    }
  };

  return Client;

}])
.factory('generateClientSMS', ['$filter', function ($filter) {

  function getPhrase() {
    var firstname = $filter('capitalizeFirst')(this.firstname) || '';
    var lastname = $filter('capitalizeFirst')(this.lastname) || '';
    var plan = $filter('currency')(this.plan);
    var telephone = $filter('tel')(this.telephone);
    var closing = (this.store.name || '') + '\n' + $filter('tel')(this.store.locations[0].phone_number);
    var phrases = {
      'none': {
        'czech': "Ahoj " + firstname + '. Tvoje mesicni telefonni sluzba brzy vyprsi. Prosim zavolej nebo prijd do Telekomu zaplatit za svuj telefon.\n'+ closing,
        'polish': "Witam " + firstname + '. Twoj serwis tel. wygasa niedlugo. Prosze przyjdz lub zadzwon do Telekomu aby zaplacic za swoj telefon.\nDziekujemy,\n'+ closing,
        'english': "Hello " + firstname + " your payment is due on soon, please call or come visit us to pay your bill !\nThank you,\n"+ closing,
        'spanish': "Hola "+ firstname+ " tu pago se espera. Tu servicio telefonico expirara pronto, por favor visitanos.\nGracias,\n"+ closing,
        'ukraine': "Вітаю " + firstname+". Твій сервіс телефону виключається за недовго. Просимо прийти або задзвонити до телекому, щоб заплатити за свій телнфон.\n"+ closing
      },
      'prepaid': {
        'polish': "Wysli message do " + firstname + ' ' + lastname+'\n'+ closing,
        'english': "Send a message to " + firstname + ' ' + lastname+'\n'+ closing,
        'spanish': "Enviar un mensaje a " + firstname + ' ' + lastname+'\n'+ closing,
        'ukraine': "Надіслати повідомлення для " + firstname + ' ' + lastname+'\n'+ closing,
        'czech': "Odeslat zprávu "+ firstname+ ' ' + lastname+'\n'+ closing
      },
      'creditcard': {
        'polish': firstname + " twoja platnosc (" + plan + ") za telefon:  " + telephone + ' zostala zaplacona ' + moment().calendar() + '\n\nDziekujemy\n' +  closing,
        'english': firstname + ", your payment ("+ plan + ') for:  ' + telephone + ' was paid ' + moment().calendar() + '\n\nThank You,\n' +  closing,
        'spanish': firstname + ' ' + lastname + " espanol words  " + moment().calendar(),
        'ukraine': firstname + ' ' + lastname + ", ваш платіж " + "(" + plan + ") для " + telephone + " приділялася " + moment().calendar() + '\n\nСпасибі,\n' + closing,
        'czech': firstname + ' vaše platba byla zaplacena.\n' + moment().calendar() + "\nDěkuji\n" + closing
      }
    }
    return phrases[this.paymenttype][this.language];
  }

  return function () {
    return getPhrase.call(this);
  };

}])
.factory('clientsFilter', ['ClientService','$filter', function (ClientService, $filter) {
  var promise;
  return function clientsFilter(val) {
    var clients;
    var keys = Array.prototype.slice.call(arguments,1);
    return ClientService.get().then(function (fetchedClients) {
      clients = $filter('searchField')(fetchedClients,keys);
      fetchedClients = $filter('filter')(clients,val);
      fetchedClients = $filter('limitTo')(fetchedClients,8);
      return fetchedClients;
    });
  }
}])
;