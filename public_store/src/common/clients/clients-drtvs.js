angular.module('clients.directive', [
  'constants',
  'angulartics',
  'ui.bootstrap.buttons',
  'angulartics.google.analytics'
])
.directive('paymentType', [ function () {
  return {
    restrict: 'E',
    templateUrl:'clients/templates/paymenttypes.html',
    scope:{
      client:'=',
      analyticsCategory:'@',
      analyticsEvent:'@'
    },
    link: function ($scope, $element, $attr) {
    }
  };
}])
;