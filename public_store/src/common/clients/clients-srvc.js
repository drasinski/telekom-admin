angular.module('resource.clients', [
  'constants',
  'angular-data.DSCacheFactory',
  'service.xhr'
])
.factory('ClientService', ['$q', 'xhr', 'STORE', 'DSCacheFactory', 'apiPrefix', function ($q, xhr, STORE, DSCacheFactory, apiPrefix) {

    var DSCache = DSCacheFactory('clients', {
      maxAge: 12600000,
      cacheFlushInterval: 18000000,
      deleteOnExpire: 'aggressive',
      onExpire: function (key, value) {
        xhr([STORE.storePrefix+'/clients', {params:{per_page:'0',sort_by:'lastname'}, cache:false }]).then(function (data) {
          DSCache.put(key,data);
          console.log("Refreshed client cache");
        });
      }
    });

    var service = {
      get: function(query, force) {
        var cache = DSCache.get('clients');
        if(angular.isArray(cache) && angular.isUndefined(force)) {
          return $q.when(cache);
        } else {
          var x = angular.extend({per_page:'0',sort_by:'lastname'},query);
          return xhr([STORE.storePrefix+'/clients',{params:x, cache:false }]).then(function (data) {
            if(angular.isUndefined(force)) {
              DSCache.put('clients',data);
            }
            return data;
          });
        }
      },
      getClientInvoiceCount: function() {
        return xhr([apiPrefix+this.store.storeId+'/clients/'+this._id+'/invoices/count']).then(function (data) {
          return data;
        });
      },
      getClient: function(id) {
        var cache = DSCache.get('clients');
        if(angular.isArray(cache)) {
          var client = _.findWhere(cache, {_id: id});
          if(angular.isDefined(client)) {
            return $q.when(client);
          }
        }
        return xhr([STORE.storePrefix+'/clients/'+id]);
      },
      post: function() {
        return xhr('post',[apiPrefix+this.store.storeId+'/clients',this]).then(function (data) {
          service.updateCache(data)
          return data;
        });
      },
      delete: function() {
        return xhr('delete',[apiPrefix+this.store.storeId+'/clients/'+this._id]).then(function (data) {
          service.deleteFromCache(data);
          return data;
        });
      },
      deleteFromCache: function(id) {
        var x = DSCache.get('clients');
        if(angular.isArray(x)) {
          DSCache.put('clients', _.reject(x, function (clientList) {
            return (clientList._id === id);
          }));
        }
      },
      updateCache: function(data) {
        var x = DSCache.get('clients');
        if(angular.isArray(x)) {
          var obj = _.findWhere(x, {_id: data._id});
          if(angular.isDefined(obj)) {
            angular.extend(obj,data);
          } else {
            x.push(data);
          }
        } else {
          x = [data];
        }
        DSCache.put('clients',x);
      },
      put: function() {
        return xhr('put',[apiPrefix+this.store.storeId+'/clients/'+this._id, this]).then(function (data) {
          service.updateCache(data);
          return data;
        });
      },
      smsBulk: function(options) {
        return xhr('post',[STORE.storePrefix+'/clients/sms', options ]);
      },
      sms: function(options) {
        return xhr('post',[apiPrefix+this.store.storeId+'/clients/'+this._id+'/sms', options ]);
      }
    };

    return service;

}]);


