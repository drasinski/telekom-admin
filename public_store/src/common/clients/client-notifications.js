angular.module('client.notifications',[
  'clients.srvcs',
  'resource.clients',
  'service.notify',
  'user.srvcs'
])
.factory('clientNotifications', ['Client', 'User','ClientService','notifyService', function (Client, User, ClientService, notifyService) {

  return function (socket) {

    socket.on('new:client', function (data) {
      var client = Client.build(data.client);
      var user = User.build(data.user);
      ClientService.updateCache(data.client);
      notifyService( user.name + ' has added a new client, ' + client.display +', at ' + user.store.locationInfo.name, 'alert-info', 30000);
    });
  
    socket.on('delete:client', function (data) {
      var client = Client.build(data.client);
      var user = User.build(data.user);
      ClientService.deleteFromCache(data.client._id);
      notifyService( user.name + ' has deleted a client, ' + client.display +', at ' + user.store.locationInfo.name, 'alert-info', 30000);
    });
  
    socket.on('update:client', function (data) {
      ClientService.updateCache(data.client);
    });

  }

}])