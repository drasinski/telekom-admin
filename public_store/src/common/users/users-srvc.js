angular.module('resource.users', [
  'constants',
  'service.xhr',
  'angularFileUpload'
])
.factory('UsersService', ['xhr', '$upload', 'STORE','apiPrefix', function (xhr, $upload, STORE, apiPrefix) {
  return {
    get: function(query) {
      var x;
      var id;
      if(angular.isObject(query)) {
        x = angular.extend({per_page:'0',sort_by:'created_date'},query);
        id = '';
      } else if (angular.isString(query)) {
        x = {};
        id = '/'+query;
      }
      return xhr([STORE.storePrefix+'/users'+id,{params:x, cache: false}]);
    },
    post: function () {
      return xhr('post',[apiPrefix+this.store.storeId+'/users',this]);
    },
    delete: function () {
      return xhr('delete',[apiPrefix+this.store.storeId+'/users/'+this._id]);
    },
    put: function () {
      return xhr('put',[apiPrefix+this.store.storeId+'/users/'+this._id, this]);
    },
    upload: function(file) {
      var self = this;
      return $upload.upload({
        url: apiPrefix+ self.store.storeId + '/users/' + self._id + '/upload',
        method: 'POST',
        file: file,
        headers: {'Content-Type': file.type}
      });
    }
  };
}]);
