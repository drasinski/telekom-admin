angular.module('constants', [])
.constant('OTHER',['Internet','Fax','Scan','Tip','Copy','Battery','SD Card','Sim Swap'])
.constant('apiPrefix','api/v1/stores/')
.constant('GRAPH_COLORS',['#008cba', 'rgb(0, 42, 196)', 'rgb(255, 177, 0)', 'rgb(138, 219, 33)', '#E24FE2','#5BC1DE','#F04024','rgb(233, 182, 2)','#59249E','#981898','#C1E124','#E9D725'])
.constant('TYPING_TIMER_LENGTH', 500)
.constant('EPAY_URL','https://webpos.epayworldwide.com/secure/login.aspx?');
