angular.module('inventory.srvcs', [
  'store.srvcs',
  'resource.inventory',
  'service.notify'
])
.factory('Inventory', ['Store','InventoryService','$filter','notifyService', function (Store, InventoryService, $filter, notifyService) {

  function Inventory(options) {
    angular.extend(this, options);
    angular.extend(this.store, options.store);
    this.oldPhone = angular.copy(this.serialize());
  }

  function deserialize(options) {
    if(angular.isDefined(options.created_date)) {
      options.created_date = new Date(options.created_date);
    }
    if(angular.isDefined(options.last_modified_date)) {
      options.last_modified_date = new Date(options.last_modified_date);
    }
    return options;
  }

  Inventory.prototype = {

    hasChanged: function() {
      return !angular.equals(this.serialize(),this.oldPhone);
    },

    revert: function() {
      if(this.hasChanged()) {
        angular.extend(this, this.oldPhone);
        this.store = Store.build(this.oldPhone.store);
        this.oldPhone = angular.copy(this.serialize());
      }
    },

    isValid: function() {
      return angular.isNumber(this.amount) &&
             angular.isNumber(this.fixed_cost) &&
             angular.isString(this.imei) && 
             (this.amount >= 0) &&
             (this.fixed_cost >= 0) &&
             !_.isEmpty(this.imei) &&
             angular.isString(this.description) &&
             !_.isEmpty(this.description) &&
             angular.isString(this.store.locationId) &&
             !_.isEmpty(this.store.locationId);
    },

    serialize: function() {
      var self = this;
      return {
        _id: self._id || undefined,
        fixed_cost: self.fixed_cost,
        sold: self.sold,
        amount: self.amount,
        imei: self.imei,
        description: self.description,
        store: self.store.serialize()
      }
    },

    isNewPhone: function() {
      return angular.isUndefined(this._id);
    },

    $save: function (options) {
      var self = this;
      if(self.isNewPhone()) {
        return InventoryService.post.call(self.serialize()).then(function (resp) {
          notifyService(self.description +" with IMEI: " + self.imei + " was successfully created.", 'alert-success');
          self._id = resp._id;
          self.oldPhone = angular.copy(self.serialize());
          return resp;
        }, function (err) {
          notifyService(self.description +" with IMEI: " + self.imei + " could not be created.", 'alert-danger');
          return $q.reject(err);;
        });
      } else {
        return InventoryService.put.call(self.serialize()).then( function (resp) {
          notifyService(self.description +" with IMEI: " + self.imei + " was updated.", 'alert-success');
          self.oldPhone = angular.copy(self.serialize());
          return resp;
        }, function (err) {
          notifyService(self.description +" with IMEI: " + self.imei + " could not be updated.", 'alert-danger');
          return $q.reject(err);;
        });
      }
    },

    $remove: function() {
      var self = this;
      if(angular.isDefined(self._id)) {
        return InventoryService['delete'].call(this).then(function (inv) {
          notifyService(self.description +" with IMEI: " + self.imei + " was removed.", 'alert-success');
          return inv;
        }, function (err) {
          notifyService(self.description +" with IMEI: " + self.imei + " could not be removed.", 'alert-danger');
          return $q.reject(err);
        });
      }
    },

    $predictPhoneNames: function(val) {
      return InventoryService.get({sold:'f'}).then(function (inventory) {
        return $filter('filter')(_.chain(inventory).pluck('description').uniq().value(), val);
      });
    }

  }

  Inventory.build = function(options) {
    options.store = Store.build(options.store || {});
    return new Inventory(deserialize(options));
  }

  return Inventory;

}]);