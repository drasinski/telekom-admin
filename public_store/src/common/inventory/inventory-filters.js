angular.module('inventory.filters',[
  'service.hasher'
 ])
.filter('groupBy', ['hasher', function (hasher) {
  return _.memoize( function groupBy(input,key) {
    return _.groupBy(input, key);
  },hasher());
}])
.filter('partitionBy', ['hasher', function (hasher) {
  return _.memoize( function partition(collection) {
    if (!collection) { return; }
    var newArr = [];
    var temp = []
    angular.forEach(collection, function (val, key) {
      var x = {};
      x[key]=val;
      temp.push(x);
    });
    var half_length = Math.ceil(temp.length / 2);  
    newArr.push(temp.splice(0,half_length));
    newArr.push(temp);
    return newArr;
  }, hasher());
}])