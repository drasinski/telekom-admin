angular.module('resource.inventory', [
  'ui.bootstrap.modal',
  'constants',
  'angular-data.DSCacheFactory',
  'service.xhr',
  'hdirectives'
])
.factory('InventoryService', [ '$q','xhr','STORE','apiPrefix','DSCacheFactory', function ($q, xhr, STORE, apiPrefix, DSCacheFactory) {

  var inventoryCache = DSCacheFactory('inventoryCache', {
    maxAge: 43200000,
    cacheFlushInterval: 46800000,
    deleteOnExpire: 'aggressive'
  });


  return {
    get: function (query) {
      /* think about the caching and query sold & non sold phones */
      query = query || {};
      var cache = inventoryCache.get(JSON.stringify(query));
      if(angular.isDefined(cache)) {
        return $q.when(cache);
      } else {
        var x = angular.extend({per_page:'0',sort_by:'description'},query);
        return xhr('get',[STORE.storePrefix+'/inventory',{params:x, cache: false}]).then(function (data) {
          inventoryCache.put(JSON.stringify(query), data);
          return data;
        });
      }
    },
    post: function () {
      return xhr('post',[apiPrefix+this.store.storeId+'/inventory',this]).then(function (data) {
        inventoryCache.removeAll();
        return data;
      });
    },
    delete: function () {
      return xhr('delete',[apiPrefix+this.store.storeId+'/inventory/'+this._id]).then(function (data) {
        inventoryCache.removeAll();
        return data;
      });
    },
    put: function () {
      return xhr('put',[apiPrefix+this.store.storeId+'/inventory/'+this._id,this]).then(function (data) {
        inventoryCache.removeAll();
        return data;
      });
    },
    clearCache: function() {
      inventoryCache.removeAll();
    }
  };

}])
.service('inventoryModal', [ '$modal','$q', function ($modal, $q ) {

    this.addPhone = function addPhone(phone) {
      var modalInstance = $modal.open({
        templateUrl: 'inventory/templates/inventory-add-modal.html',
        controller: 'inventoryModalCtrl',
        resolve: {
          phone:[function() {
            return phone;
          }]
        }
      });
      var defer = $q.defer();
      modalInstance.result.then(function (data) {
        data.$save().then(function (result) {
          defer.resolve(result);
        }, function (err) {
          defer.reject(true);
        })
      }, function() {
        defer.reject(false);
      });
      return defer.promise;
    };

}])

.controller('inventoryModalCtrl', ['$scope', '$modalInstance','phone', function ($scope, $modalInstance, phone) {

  $scope.phone = phone;

  $scope.submit = function submit(phone) {
    $modalInstance.close(phone);
  };

  $scope.cancel = function cancel() {
    $modalInstance.dismiss('cancel');
  };

}])
;
