angular.module('invoice.srvcs.new-modal',[
  'ui.keypress',
  'ui.bootstrap.modal',
  'ui.bootstrap.datepicker',
  'hdirectives',
  'invoice.srvcs',
  'filters',
  'ngCookies',
  'resource.invoices'
])
.service('invoiceModal', [ '$modal','$q','epayModal', function ($modal, $q, epayModal) {

    this.open = function open(invoice) {

      var modalInstance = $modal.open({
          templateUrl: 'invoice/templates/new-invoice-modal.html',
          controller: 'invoiceModalCtrl',
          resolve: {
            invoice: [function() {
              return invoice; 
            }]
          }
      });

      var defer = $q.defer();

      modalInstance.result.then(function (response) {
        invoice.$save(response.options).then(function (resp) {
          defer.resolve({invoice:(resp[0] || resp), printFlag: response.printFlag});
        }, function (err) {
          defer.reject(err);
        });
        if(invoice.hasEpayTrans()) {
          epayModal(invoice).result.then(function (response) {
            console.log("Invoice submitted on Epay");
          });
        }
      }, function (err) {
        defer.reject(false);
      });

      return defer.promise;
    };

}])

.controller('invoiceModalCtrl', ['$scope', '$modalInstance','invoice', function ($scope, $modalInstance, invoice) {

  $scope.minDate = moment().add(-30,'days').format("D-MMM-YYYY");

  $scope.invoice = invoice;

  $scope.goToClientPage = function goToClientPage(invoice) {
    if(invoice.client.isExistingClient()) {
      $scope.$state.go("clientinfo.detail.info",{id:invoice.client._id});
      $modalInstance.dismiss('cancel');
    }
  };

  $scope.$watch('invoice.client.language', function (nv) {
    if(angular.isDefined(nv)) {
      $scope.invoice.setInvoiceSMS();
    }
  });

  $scope.onModalClose = {
    options: {
      smsInvoice: false
    },
    printFlag: true
  };

  $scope.submit = function submit(settings) {
    $modalInstance.close(settings);
  };

  $scope.cancel = function cancel() {
    $modalInstance.dismiss('cancel');
  };

}])
.factory('epayModal', ['$modal', function ($modal) {
  return function (invoice) {
    return $modal.open({
        templateUrl: 'invoice/templates/epay-modal.html',
        controller: 'epayModalCtrl',
        resolve: {
          invoice: [function() {
            return invoice; 
          }]
        }
    });
  };
}])
.controller('epayModalCtrl', ['$scope','$modalInstance', 'invoice','openIframe', function ($scope, $modalInstance, invoice, openIframe) {

  $scope.cancel = function cancel() {
    $modalInstance.dismiss('cancel');
  };

  $scope.iFrameHtml = function iFrameHtml() {
    return openIframe(invoice);
  }

}])
.factory("openIframe", ['$cookies','$window','EPAY_URL', function ($cookies, $window, EPAY_URL) {
  
  return function (invoice) {
      var htmlContent = "<html>" +
          "<head>" +
            "<script type='text/javascript'>" +
              "document.addEventListener('DOMContentLoaded', function(event) {" +
                "if(document.epayForm.onsubmit && !document.epayForm.onsubmit()) { return; }" +
                "document.epayForm.submit()" +
              "});" +
            "</script>" +
          "</head>" +
          "<body>" +
            "<form method='POST', name='epayForm', action='" + EPAY_URL + "'>" +
              "<input type='hidden' name='Header.Application' value='CellBetterApp' />" +
              "<input type='hidden' name='Header.Client' value='CellBetter' />" +
              "<input type='hidden' name='Header.Channel' value='Web' />" +
              "<input type='hidden' name='Header.Version' value='1.0' />" +
              "<input type='hidden' name='Body.TxnReference' value='CELLBETTER1234567' />" +
              "<input type='hidden' name='Body.Data.CommentsRO' value='N/A'/>" +
              "<input type='hidden' name='Body.TxnInputRequired' value='N'/>" +
              "<input type='hidden' name='Body.CallbackUrl' value='http://" + $window.location.host + "/api/v1/epay-callback?_csrf=" + $cookies['XSRF-TOKEN'] + "'/>" +
              "<input type='hidden' name='Body.Data.PROMPT1' value='" + invoice.client.telephone + "'/>" +
              "<input type='hidden' name='Body.Data.PayQuicker' value='N'/>" +
              "<input type='hidden' name='Body.ProductSKU' value='" + invoice.firstEpayTrans.epaySku + "'/>" +
              "<input type='hidden' name='Body.Amount' value='" + invoice.firstEpayTrans.amount + " '/>" +
            "</form>" +
          "</body>" +
        "</html>";
        console.log("SKU:  " + invoice.firstEpayTrans.epaySku);
        console.log("AMOUNT:  " + invoice.firstEpayTrans.amount);
    return htmlContent;
  }

}]);