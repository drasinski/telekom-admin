angular.module('invoice.directive', [
  'constants',
  'filters',
  'ui.bootstrap.buttons',
  'ui.bootstrap.modal',
  'resource.inventory',
  'service.confirm',
  'hdirectives',
  'invoice.srvcs',
  'inventory.srvcs',
  'angulartics',
  'service.printer',
  'ngAnimate'
])
.directive('invoice', ['$timeout','$window','$analytics','ConfirmService','InventoryService','inventoryModal','Inventory','$filter','printer', function ($timeout, $window, $analytics, ConfirmService, InventoryService, inventoryModal, Inventory, $filter, printer) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'invoice/templates/invoice-small.html',
    link: function($scope, element, attrs) {

      // var invoiceCopy = angular.copy($scope.invoice);

      $scope.smsInvoice = function smsInvoice(invoice) {
        invoice.$sendInvoiceSMS();
      };

      // $scope.showUndo = function showUndo(invoice) {
      //   return !angular.equals(invoice, invoiceCopy);
      // };

      // $scope.undo = function undo() {
      //   $scope.invoice = angular.copy(invoiceCopy);
      // };

      $scope.delInvoice = function delInvoice(invoice) {
        ConfirmService("Are you sure you want to delete this invoice?").then(function () {
          invoice.$remove().then(function (id) {
            $analytics.eventTrack('Delete Invoice', {category:'Invoice'});
            $scope.deleteInvoice(id);
          });
        });
      };

      $scope.saveInvoice = function saveInvoice(invoice) {
        ConfirmService("Are you sure you want to save your changes?").then(function() {
          invoice.$save().then(function () {
            $analytics.eventTrack('Update Invoice', {category:'Invoice'});
            invoiceCopy = angular.copy(invoice);
          });
        });
      };

      $scope.addPhone = function addPhone(store, transaction) {
        var phone = Inventory.build({store:store});
        inventoryModal.addPhone(phone).then(function (data) {
          $analytics.eventTrack('Add Phone',{category:"Invoice"})
          transaction.addPhone(data);
        });
      };

      $scope.printInfo = function printInfo(invoice) {
        printer.printFromScope('invoice/templates/invoice-print.html',$scope);
      };

    }
  };
}])
.directive('printInvoice', [ function () {
  return {
    restrict: 'E',
    scope: {
      invoice:"="
    },
    templateUrl: "invoice/templates/invoice-print.html",
    link: function ($scope, iElement, iAttrs) {
      $scope.today = new Date();
    }
  };
}])
.directive('datepickerInput', ['$filter', function ($filter) {
  return {
      templateUrl: 'invoice/templates/datepickerinput.html',
      restrict:'E',
      replace:true,
      scope: {
        date:"=ngModel"
      },
      link: function($scope) {
        $scope.$watch('date', function (nV, oV) {
          if(nV === oV) {
            $scope.date = new Date(nV);
          }
        });
      }
  };
}])
.directive('paymentMethodMultiple', [function () {
  return {
    restrict: 'E',
    templateUrl:'invoice/templates/paymentmethod-multiple.html',
    link: function ($scope, $element, $attr) {
    }
  };
}])
;
