angular.module('resource.invoices', [
  'constants',
  'service.xhr'
])
.factory('InvoiceService', ['xhr', 'STORE','apiPrefix', function (xhr, STORE, apiPrefix) {

  return {
    get: function(query) {
      var x = angular.extend({per_page:'0',sort_by:'created_date'},query);
      return xhr([STORE.storePrefix+'/invoices',{params:x, cache: false}]);
    },
    post: function () {
      return xhr('post',[apiPrefix+this.store.storeId+'/invoices',this]);
    },
    delete: function () {
      return xhr('delete',[apiPrefix+this.store.storeId+'/invoices/'+this._id]);
    },
    put: function () {
      return xhr('put',[apiPrefix+this.store.storeId+'/invoices/'+this._id, this]);
    }
  };
  
}])
