angular.module('invoice.filters',[
  'service.hasher'
 ])
.filter('system',[function() {
  return _.memoize( function findSystems(invoices) {
    return _.chain(invoices).map(function (invoice) {
      return invoice.transactions
    }).flatten().reduce(function (memo, trans) {
      var trans_type = trans.typeId || trans.otherId || trans.inventoryId;
      var system = trans_type.system || trans_type.category;
      var system_slug = trans_type.system_slug || trans_type.category_slug;
      var element = _.findWhere(memo,{system_slug:system_slug});
      if(angular.isDefined(element)) {
        var i = memo.indexOf(element);
        memo[i].total = memo[i].total + trans.amount - trans.amtsale;
      } else {
        memo.push({system:system, system_slug:system_slug, total:trans.amount});
      }
      return memo;
    }, []).value();
  });
}])
// .filter('contains', ['$parse', function containsFilter($parse) {
//     return function (collection, expression) {

//       collection = (angular.isObject(collection)) ? _.toArray(collection) : collection;

//       if(!angular.isArray(collection) || angular.isUndefined(expression)) {
//         return false;
//       }

//       return collection.some(function (elm) {
//         return (angular.isObject(elm) || angular.isFunction(expression)) ? $parse(expression)(elm) : elm === expression;
//       });

//     }
// }])
.filter('contains', ['hasher', function (hasher) {
    return _.memoize(function filterByUsers(invoices, object) {
      return _.filter(invoices, function (invoice){
        return _.contains(object.users,invoice.user.username) && _.contains(object.locs, invoice.store.locationId);
      });
    }, function (invoices, obj) {
      angular.forEach(obj, function (v, k) {
        obj[k] = v.sort();
      });
      invoices = _.map(invoices, function (invoice) {
        return {
          _id: invoice._id,
          grandtotal: invoice.grandtotal,
          typeofpayment: invoice.typeofpayment,
          created_date: invoice.created_date, 
          username: invoice.user.username,
          store: invoice.store.locationId,
          descriptions: _.map(invoice.transactions, function (trans) {
            var obj = trans.otherId || trans.typeId || trans.inventoryId;
            return obj.description;
          })
        }
      });
      return hasher()(invoices, obj);
    });
}])
//http://stackoverflow.com/questions/16507040/angular-filter-works-but-causes-10-digest-iterations-reached
.filter('groupByInvoices', ['hasher',function (hasher) {
  return _.memoize( function groupBy(invoices, group_by) {
    group_by = group_by.toLowerCase();
    group_by = (group_by === 'day' || group_by === 'week' || group_by === 'month' || group_by === 'username') ? group_by : 'day';
    return _.groupBy(invoices, function (val){
      if(group_by === 'username') {
        return val.processed_by;
      } else {
        return moment(val.created_date).startOf(group_by).valueOf();
      }
    });
  }, function (invs, group) {
    return hasher()(_.pluck(invs, '_id'), group);
  });
}])
.filter('total',[function(){
  return function (invoices,field) {
    return _.reduce(invoices, function (memo, value){
      if(field === 'Total') {
        memo += value.cash_total;
        memo += value.credit_total;
        memo += value.check_total;
      } else {
        memo += value[field.toLowerCase()+'_total'];
      }
      return memo;
    }, 0);
  };
}])
.filter('daterangefilter',[function() {
  return function (invoices,startDate,endDate) {
    endDate = (angular.isUndefined(endDate)) ? moment().add('days',1) : endDate;
    return _.filter(invoices, function (value, key, list){
      var now = moment(value.created_date);
      return (now.isBefore(endDate,'date') && now.isAfter(startDate,'date'));
    });
  };
}])
.filter('formatpayment', [function() {
  return function (typeofpayment) {
    if(angular.isArray(typeofpayment) && typeofpayment.length) {
      return typeofpayment.toString().replace(/,/g,', ');
    }
  }
}])
;

// .filter({
//     contains: ['$parse', containsFilter],
//     some: ['$parse', containsFilter]
//   });

// function containsFilter( $parse ) {
//     return function (collection, expression) {

//       collection = (angular.isObject(collection)) ? _.toArray(collection) : collection;

//       if(!angular.isArray(collection) || angular.isUndefined(expression)) {
//         return false;
//       }

//       return collection.some(function (elm) {
//         return (angular.isObject(elm) || angular.isFunction(expression)) ? $parse(expression)(elm): elm === expression;
//       });

//     }
//  }

