angular.module('invoice.srvcs.profit', [
  'constants',
  'resource.inventory'
])
.factory('InvoiceProfits', ['STORE', function (STORE) {

  function InvoiceProfits(options) {
    angular.extend(this, options);
  }

  function getProfitTrans(created_date) {
    var typeObj = this.inventoryId || this.otherId || this.typeId;
    if(angular.isArray(typeObj.profit)) {
      created_date = moment(created_date);
      var profitObj = _.find(typeObj.profit, function (profit) {
        var start_date = moment(profit.start_date);
        var end_date = moment(profit.end_date);
        return created_date.isBetween(start_date,end_date, 'day') || created_date.isSame(start_date, 'day') || created_date.isSame(end_date,'day')
      });
      var amount_profit = (profitObj.has_amount) ? (this.amount-profitObj.amount_fixed)*(profitObj.amount_comm/100) : 0;
      var tax_profit = (profitObj.has_tax) ? (this.taxtotal-profitObj.tax_fixed)*(profitObj.tax_comm/100) : 0;
      var servicecharge_profit = (profitObj.has_servicecharge) ? (this.servicecharge-profitObj.servicecharge_fixed)*(profitObj.servicecharge_comm/100) : 0;
      return amount_profit+tax_profit+servicecharge_profit-(profitObj.fixed_cost||0)-(this.amtsale||0);
    } else {
      return this.amount - (typeObj.fixed_cost||0) - (this.amtsale||0);
    }
  }

  function getProfits() {
    var self = this;
    return _.reduce(self.transactions, function (memo,trans) {
      var typeObj = trans.inventoryId || trans.otherId || trans.typeId;
      var key = typeObj.category_slug || 'other';
      memo[key] =  (memo[key] || 0) + getProfitTrans.call(trans, self.created_date);
      memo['Total'] = (memo['Total'] || 0) + getProfitTrans.call(trans, self.created_date);
      return memo;
    },{});
  }

  function getKey(keys) {
    return _.reduce(this.transactions, function (memo, trans) {
      var typeObj = trans.inventoryId || trans.otherId || trans.typeId;
      var key = typeObj.category_slug || 'other';
      var name = typeObj.category || 'Other';
      var icon;
      if(trans.inventoryId) {
        icon = 'fa-mobile-phone';
      } else {
        icon = typeObj.icon || 'fa-question';
      }
      if(angular.isUndefined(_.findWhere(memo,{id:key}))) {
        memo.push({id:key, name:name, icon:icon});
      }
      return memo;
    }, keys)
  }

  function uniqueAndMap(data,key) {
    return _.chain(data).pluck(key).uniq().map(function (val) {
      return {name: val, id:val};
    }).value();
  }

  InvoiceProfits.prototype = {
    getLocName: function (locationId) {
      return _.findWhere(this.store.locations,{id:locationId}).name
    },
    getTotal: function (data) {
      return _.reduce(data, function (memo, val) {
        return memo+val.amount;
      },0);
    }
  }

  InvoiceProfits.build = function (invoices, daterange) {
    if(angular.isArray(invoices)) {
      var options = {};
      var types = [];
      if(angular.isObject(daterange)) {
        options.daterange = daterange;
      } else {
        options.daterange = {};
      }
      options.data = _.map(invoices, function (invoice, key) {
        if(moment(invoice.created_date).isBefore(options.daterange.startDate) || angular.isUndefined(options.daterange.startDate) && !angular.isObject(daterange)) {
          options.daterange.startDate = moment(invoice.created_date); //.format('DD-MMM-YYYY');
        }
        if(moment(invoice.created_date).isAfter(options.daterange.endDate) || angular.isUndefined(options.daterange.endDate) && !angular.isObject(daterange)) {
          options.daterange.endDate = moment(invoice.created_date).add(1,'days'); //.format('DD-MMM-YYYY');
        }
        var y = getProfits.call(invoice);
        types = getKey.call(invoice, types);
        var x = {
          'processed_by':invoice.user.username,
          'day_of_week':moment(invoice.created_date).format('dddd'),
          'week_of_month':"Week " + Math.ceil(moment(invoice.created_date).date()/7),
          'start_of_day':moment(invoice.created_date).startOf('day').format('l'),
          'start_of_week':moment(invoice.created_date).startOf('week').format('l'),
          'start_of_month':moment(invoice.created_date).startOf('month').format('l'),
          'location':invoice.store.locationId
        };
        return angular.extend(x,y);
      });
      options.types = types;
      options.locations = _.chain(options.data).pluck('location').uniq().map(function (val) {
        return {name: _.findWhere(STORE.locations, {id:val}).name, id:val};
      }).value();
      // options.locations = uniqueAndMap(options.data,'location');
      options.roles = uniqueAndMap(options.data,'processed_by');
      options.day_of_week = uniqueAndMap(options.data, 'day_of_week');
      // options.types = _.map(types, function (val) {
      //   return {name: val, id:val};
      // });
    }
    return new InvoiceProfits(options);
  }

  return InvoiceProfits;

}]);