angular.module('invoice.notifications',[
  'invoice.srvcs',
  'user.srvcs',
  'service.notify'
])
.factory('invoiceNotifications', ['$filter','Invoice','User','notifyService', function ($filter, Invoice, User, notifyService) {

  return function (socket) {
    socket.on('new:invoice', function (data) {
      var invoice = Invoice.build(data.invoice);
      var user = User.build(data.user)
      notifyService( user.name + ' has created Invoice #' + invoice.invoice_number +' for ' + $filter('currency')(invoice.grandtotal) +' at ' + invoice.store.locationInfo.name, 'alert-info', 30000);
    });
  
    socket.on('delete:invoice', function (data) {
      var invoice = Invoice.build(data.invoice);
      var user = User.build(data.user)
      notifyService( user.name + ' has deleted Invoice #' + invoice.invoice_number + ' at ' + invoice.store.locationInfo.name, 'alert-info', 30000);
    });
    return socket;
  }

}])