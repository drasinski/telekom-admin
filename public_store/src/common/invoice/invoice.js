angular.module('invoice.srvcs',[
  'constants',
  'clients.srvcs',
  'store.srvcs',
  'user.srvcs',
  'transaction.srvcs',
  'resource.inventory',
  'resource.types',
  'resource.invoices',
  'resource.clients',
  'service.notify'
])
.provider('invoiceInit', {

  requireTypesInventory: ['invoiceInit', 'currentUser', function (invoiceInit, currentUser) {
    return invoiceInit.requireTypesInventory(currentUser);
  }],

  $get: ['$q','TypeService','Transaction','Client','InventoryService', function ( $q, TypeService, Transaction,Client, InventoryService) {

    return {
      requireTypesInventory: function(currentUser) {
        return $q.all([InventoryService.get({sold:"f","store.locationId":currentUser.store.locationId}),TypeService.get({active:'t'})]).then(Transaction.initChoices);
      }
    }

  }]

})
.factory('Invoice', ['Transaction','Client','Store','User','generateInvoiceSMS','InvoiceService', '$q', 'notifyService','InventoryService','ClientService', function (Transaction, Client, Store, User, generateInvoiceSMS, InvoiceService, $q, notifyService, InventoryService, ClientService) {

  function Invoice(options) {
    var self = this;
    angular.extend(this, options);
    angular.extend(this.client, options.client);
    angular.extend(this.store, options.store);
    angular.extend(this.user, options.user);
    this.setInvoiceSMS();
  };

  /* Private Helper Functions */
  function money_round(num) {
    return Math.round(num*100)/100;
  }

  function reduce_wrapper(prop) {
    return money_round(_.reduce(this, function (memo, trans) {
      return memo + trans[prop];
    },0));
  }

  function deserialize(invoice) {
    if(!angular.isObject(invoice)) {
      invoice = {};
    }
    angular.forEach(['servicecharge', 'subtotal', 'saletotal', 'taxtotal', 'grandtotal'], function (val) {
      delete invoice[val];
    });
    if(!angular.isObject(invoice.paymentInfo)) {
      invoice.paymentInfo = {};
      invoice.paymentInfo.Cash = (angular.isNumber(invoice.cash_total) && (invoice.cash_total > 0));
      invoice.cash_total = invoice.cash_total || 0;
      invoice.paymentInfo.Credit = angular.isNumber(invoice.credit_total) && (invoice.credit_total > 0);
      invoice.credit_total = invoice.credit_total || 0;
      invoice.paymentInfo.Check = (angular.isNumber(invoice.check_total) && (invoice.check_total > 0));
      invoice.check_total = invoice.check_total || 0;
      if(!invoice.paymentInfo.Cash && !invoice.paymentInfo.Credit && !invoice.paymentInfo.Check) {
        invoice.paymentInfo.Cash = true;
      }
    }
    invoice.typeofpayment = _.chain(invoice.paymentInfo).map(function (val, key) {
      if(val) {
        return key;
      }
    }).filter(function (val) {
      return val;
    }).value();
    if(angular.isDefined(invoice.created_date)) {
      invoice.created_date = new Date(invoice.created_date);
    }
    if(angular.isDefined(invoice.last_modified_date)) {
      invoice.last_modified_date = new Date(invoice.last_modified_date);
    }
    if(angular.isUndefined(invoice.transactions)) {
      invoice.transactions = [];
    }
    return invoice
  }

  /* Public API */
  Invoice.prototype = {
    get servicecharge() {
      return reduce_wrapper.call(this.transactions, 'servicecharge');
    },
    get subtotal() {
      return reduce_wrapper.call(this.transactions, 'amount');
    },
    get saletotal() {
      return reduce_wrapper.call(this.transactions, 'amtsale');
    },
    get taxtotal() {
      return reduce_wrapper.call(this.transactions, 'taxtotal');
    },
    get grandtotal() {
      return reduce_wrapper.call(this.transactions, 'total');
    },
    get choices() {
      return Transaction.choices;
    },
    get firstEpayTrans() {
      return _.find(this.transactions, function (trans) {
        return trans.isEpayTrans();
      })
    },
    getPhoneSaleChoices: function (trans) {
      var self = this;
      return _.chain(trans.getChoices().concat(self.soldPhones)).filter(function (val) {
        return !_.contains(_.without(self.getUniquePhoneIds(), trans.inventoryId._id), val._id);
      }).value();
    },
    serialize: function() {
      var self = this;
      var serializedInvoice = {
        _id: self._id || undefined,
        typeofpayment: self.typeofpayment,
        invoice_number: self.invoice_number,
        servicecharge: self.servicecharge,
        subtotal: self.subtotal,
        saletotal: self.saletotal,
        taxtotal: self.taxtotal,
        grandtotal: self.grandtotal,
        client: self.client.serialize(),
        store: self.store.serialize(),
        user: self.user.serializeForInvoice()
      };
      if(!self.hasMultiplePaymentMethods()) {
        angular.forEach(['Cash','Credit','Check'], function (val, key) {
          if(val === self.typeofpayment[0]) {
            serializedInvoice[val.toLowerCase()+'_total'] = self.grandtotal;
          } else {
            serializedInvoice[val.toLowerCase()+'_total'] = 0;
          }
        });
      } else {
        if(self.paymentInfo.Cash) {
          serializedInvoice.cash_total = self.getCashTotal();
          serializedInvoice.credit_total = self.credit_total;
          serializedInvoice.check_total = self.check_total;
        }
        if(self.paymentInfo.Credit && !self.paymentInfo.Cash) {
          serializedInvoice.cash_total = 0;
          serializedInvoice.credit_total = self.getCreditTotal();
          serializedInvoice.check_total = self.check_total;
        }
      }
      serializedInvoice.transactions = _.map(this.transactions, function (transaction) {
        return transaction.serialize();
      });
      return serializedInvoice;
    },
    hasEpayTrans: function() {
      return angular.isDefined(this.firstEpayTrans);
    },
    hasSrvcCharge: function() {
      return (this.servicecharge > 0);
    },
    hasSale: function() {
      return (this.saletotal > 0);
    },
    hasTax: function() {
      return (this.taxtotal > 0);
    },
    addTransaction: function(options) {
      var trans = Transaction.build(options);
      this.transactions.push(trans);
      /* if the transaction is a phone bill increment due date */
      if(trans.isPhoneBillTrans() && this.client.isClient()) {
        this.client.changeRechargeDate(1);
        this.setClientPaymentType();
      }
    },
    setClientPaymentType: function() {
      if(this.client.isClient()) {
        if(this.isPrepaidInvoice()) {
          this.client.paymenttype = 'prepaid';
        } else {
          if(this.client.isExistingClient()) {
            this.client.paymenttype = this.client.oldClient.paymenttype;
          } else if(this.client.isNewClient()) {
            this.client.paymenttype = 'none';
          }
        }
      }
    },
    checkExistingClient: function() {
      var self = this;
      if(self.client.isNewClient()) {
        return ClientService.get({'telephone':self.client.telephone},true).then(function (client) {
          if(client.length) {
            self.client = Client.build(client[0]);
          }
          return self;
        });
      } else {
        return $q.when(self);
      }
    },
    getCashTotal: function() {
      var x = this.grandtotal - this.check_total - this.credit_total;
      if(x < 0 || !this.paymentInfo.Cash) {
        return undefined;
      } else {
        return x;
      }
    },
    getCreditTotal: function() {
      var x = this.grandtotal - this.check_total;
      if(x < 0 || !this.paymentInfo.Credit) {
        return undefined;
      } else {
        return x;
      }
    },
    hasMultiplePaymentMethods: function() {
      return (this.typeofpayment.length >= 2);
    },
    setSinglePaymentMethod: function (key) {
      var self = this;
      angular.forEach(['Cash', 'Credit', 'Check'], function (val) {
        if(val !== key) {
          self.paymentInfo[val] = false;
          self[val.toLowerCase()+'_total'] = 0;
        } else {
          self.paymentInfo[val] = true;
          self[val.toLowerCase()+'_total'] = self.grandtotal;
        }
      });
      self.typeofpayment = [key];
    },
    setDoublePaymentMethod: function (key, $event) {
      var self = this;
      if(self.paymentInfo[key] && self.hasMultiplePaymentMethods()) {
        self.paymentInfo[key] = false;
        self.typeofpayment = _.without(self.typeofpayment, key);
        self[key.toLowerCase()+'_total'] = 0;
        if(!self.hasMultiplePaymentMethods()) {
          self[self.typeofpayment[0].toLowerCase()+'_total'] = self.grandtotal;
        }
      } else if(!self.paymentInfo[key]) {
        self.typeofpayment.push(key);
        self.paymentInfo[key] = true;
      }
    },
    clear: function() {
      var self = this;
      angular.extend(self, {
        cash_total:0,
        credit_total:0,
        check_total:0,
        invoice_number: new Date().getTime(),
        typeofpayment:['Cash'],
        paymentInfo: {
          Cash:true,
          Credit:false,
          Check: false
        },
        transactions:[]
      });
      this.client.clear();
    },
    removeTransaction: function (trans) {
      var idx = this.transactions.indexOf(trans);
      if((this.transactions.length > idx) && (idx > -1)) {
        this.transactions.splice(idx,1);
        if(trans.isPhoneBillTrans() && this.client.isClient()) {
          this.client.changeRechargeDate(-1);
          this.setClientPaymentType();
        }
      }
    },
    isValidPaymentSum: function () {
      if(this.typeofpayment.length === 3) {
        return (this.grandtotal >= (this.credit_total + this.check_total));
      } else {
        return true;
      }
    },
    isValidPayment: function (amount) {
      return this.isValidPaymentSum() && angular.isNumber(amount) && (amount <= this.grandtotal);
    },
    isPrepaidInvoice: function() {
      return ((_.filter(this.transactions, function (trans) {
        return trans.isPhoneBillTrans();
      })).length > 1);
    },
    isValidAmount: function(key) {
      return angular.isNumber(this[key]) && !isNaN(this[key]);
    },
    clearInventoryCache: function() {
      if(this.hasPhones()) {
        InventoryService.clearCache();
      }
    },
    hasPhones: function() {
      return (this.getUniquePhoneIds().length > 0);
    },
    getUniquePhoneIds: function() {
      return _.chain(this.transactions).filter(function (trans) {
        return angular.isObject(trans.inventoryId)&&angular.isDefined(trans.inventoryId._id);
      }).map(function (trans) {
        return trans.inventoryId._id;
      }).value();
    },
    isValid: function() {
      var transValid = _.reduce(this.transactions, function (memo, trans) {
        return trans.isValid() && memo;
      }, true);
      var paymentInfoValid = true;
      if(this.hasMultiplePaymentMethods()) {
        if(this.paymentInfo.Credit) {
          paymentInfoValid = paymentInfoValid && this.isValidPayment(this.credit_total);
        }
        if(this.paymentInfo.Check) {
          paymentInfoValid = paymentInfoValid && this.isValidPayment(this.check_total);
        }
      }
      /* Make sure we dont have duplicates in the phone_sale */
      var uniquePhoneSale = this.getUniquePhoneIds();
      var validPhoneSale = (_.uniq(uniquePhoneSale).length === uniquePhoneSale.length);
      return transValid && (this.transactions.length > 0) && validPhoneSale && paymentInfoValid;
    },
    isInvalid: function() {
      return !this.isValid();
    },
    newInvoiceNumber: function() {
      this.invoice_number = new Date().getTime();
    },
    addClient: function(client) {
      this.client.clear();
      this.client = Client.build(client);
      return this.client;
    },
    newPayment: function() {
      var self = this;
      if(self.client.isClient() && self.client.hasPlanAndCarrier()) {
        // this is some hacky shit javascript is not copy deep references so we copy the objects individually.
        var type = angular.copy(_.findWhere(Transaction.choices['phone-bill'],{ description:self.client.carrier, category_slug:'phone-bill' }));
        type.prediction = angular.copy(_.findWhere(Transaction.choices['phone-bill'],{ description:self.client.carrier, category_slug:'phone-bill' }).prediction);
        var transaction = Transaction.build({typeId:type, category_slug:"phone-bill", amount:this.client.plan});
        transaction.setPrediction(transaction.getPrediction());
        this.transactions.push(transaction);
        this.client.changeRechargeDate(1);
        this.setClientPaymentType();
      }
    },
    isNewInvoice: function() {
      return angular.isUndefined(this._id);
    },
    setInvoiceSMS: function() {
      this.invoiceSMS = generateInvoiceSMS.call(this);
    },
    // options: {
    //   smsInvoice: bool
    // }
    $save: function(options) {
      var self = this;
      if(self.isValid()) {
        if(self.isNewInvoice()) {
          if(self.client.isClient()) {
            return self.client.$save().then(function (newClient) {
              self.client._id = newClient._id;
              var promises = [InvoiceService.post.call(self.serialize())];
              if(options.smsInvoice) { 
                promises.push(self.$sendInvoiceSMS());
              }
              return $q.all(promises).then(function (resp) {
                self.clearInventoryCache();
                notifyService("Invoice #"+self.invoice_number+" was successfully created.", 'alert-success');
                return resp;
              });
            });
          } else {
            return InvoiceService.post.call(self.serialize()).then(function (resp) {
              self.clearInventoryCache();
              notifyService("Invoice #"+self.invoice_number+" was successfully created.", 'alert-success');
              return resp;
            }, function (err) {
              notifyService("Invoice #"+self.invoice_number+" could not be submitted.", 'alert-danger');
              return $q.reject(err);
            });
          }
        } else {
          return InvoiceService.put.call(self.serialize()).then(function (resp) {
            self.clearInventoryCache();
            self.cash_total = resp.cash_total;
            self.credit_total = resp.credit_total;
            self.check_total = resp.check_total;
            notifyService("Invoice #"+self.invoice_number+" was successfully updated.", 'alert-success');
            return resp;
          }, function (err) {
            notifyService("Invoice #"+self.invoice_number+" could not be updated. Please try again.", 'alert-danger');
            return $q.reject(err);
          });
        }
      }
    },
    $remove: function() {
      var self = this;
      return InvoiceService['delete'].call(self).then(function (resp) {
        self.clearInventoryCache();
        notifyService("Invoice #"+self.invoice_number+" was succesfully removed.", 'alert-success');
        return resp;
      }, function (err) {
        notifyService("Invoice #"+self.invoice_number+" could not be removed. Please try again.", 'alert-danger');
        return $q.reject(err);
      });
    },
    $sendInvoiceSMS: function() {
      var self = this;
      if(self.client.isClient()) {
        var x = {
          message_type: 'Receipt',
          message: self.invoiceSMS,
          for_date: new Date(),
        }
        return self.client.$sendSMS(x);
      }
    }
   };

  /* Static Method */
  Invoice.build = function (options) {
    if(angular.isArray(options.transactions)) {
      options.transactions = _.map(options.transactions, Transaction.build);
    }
    options.soldPhones = _.chain(options.transactions).filter(function (trans) {
      return angular.isObject(trans.inventoryId)&&angular.isDefined(trans.inventoryId._id);
    }).map(function (trans) {
      return trans.inventoryId;
    }).value();
    options.client = Client.build(options.client || {});
    options.store = Store.build(options.store || {});
    options.user = User.build(options.user || {});
    return new Invoice(deserialize(options));
  };

  return Invoice;

}])
.factory('generateInvoiceSMS', ['$filter', function ($filter) {
  var paidHash = {
    'english':'paid on',
    'polish':'zostala zaplacona',
    'czech':'byla venovana',
    'spanish':'se ha pagado',
    'ukraine':'було приділено'
  };
  var helloHash = {
    'english':'Hello',
    'polish':'Witam',
    'czech':'Ahoj',
    'spanish':'Hola',
    'ukraine':'привіт'
  };
  var thankYouHash = {
    'spanish':'Gracias',
    'english':'Thank you',
    'czech':'Dekuji',
    'polish':'Dziekujemy',
    'ukraine':'спасибі'
  };
  var totalHash = {
    'polish':'Suma',
    'czech':'Uhrn',
    'ukraine':'загальний',
    'english':'Total',
    'spanish':'Suma'
  };

  return function generateText() {
    var date = this.created_date || new Date();
    var language = this.client.language || 'polish';
    var firstname = $filter('capitalizeFirst')(this.client.firstname || '') || '';
    var str = helloHash[language] + ' ' + firstname + ", ";
    angular.forEach(this.transactions, function (trans) {
      str += trans.getPhrase(language)+', ';;
    });
    str += totalHash[language] + ': ' + $filter('currency')(this.grandtotal) + ' ' + paidHash[language] +" ";
    return str += $filter('date')(date,'short') + ',  #' + this.invoice_number+ ".\n" + thankYouHash[language] + '\n' + this.store.name;
  };

}])