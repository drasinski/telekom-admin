angular.module('resource.store', [
  'constants',
  'service.xhr'
])
.factory('StoreService', ['xhr','apiPrefix', function (xhr, apiPrefix) {
  return {
    put: function() {
      return xhr('put',[apiPrefix+this.storeId, this]);
    }
  }
}])