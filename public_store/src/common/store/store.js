angular.module('store.srvcs',[
  'constants',
  'service.address',
  'service.notify',
  'resource.store'
])
.factory('Store', ['STORE','getAddress','notifyService','StoreService', function (STORE, getAddress, notifyService, StoreService) {

  function Store(options) {
    angular.extend(this, options);
    this.oldStore = angular.copy(this);
  }

  function deserialize(store) {
    store = angular.extend(store || {}, STORE)
    if(angular.isDefined(store._id)) {
      store.storeId = store._id;
    } else if(angular.isDefined(store.storeId)) {
      store._id = store.storeId;
    }
    store.locations = _.map(store.locations, Location.build);
    return store;
  }

  var telephoneRegex = /^\d{10}$/;
  Store.telephoneRegex = angular.copy(telephoneRegex);

  function Location(location) {
    angular.extend(this,location);
    this.oldLocation = {};
    angular.extend(this.oldLocation, location);
    angular.extend(this.oldLocation.address, location.address);
  }

  function deserializeLocation(location) {
    delete location.address.location
    return location;
  }

  Location.prototype = {
    onSelectAddress: function(address) {
      angular.extend(this.address, address);
    },
    revert: function() {
      angular.extend(this, this.oldLocation);
      angular.extend(this.address, this.oldLocation.address);
      angular.extend(this.oldLocation, this);
      angular.extend(this.oldLocation.address, this.address);
      return this;
    },
    isValid: function() {
      return angular.isString(this.address.formatted_address) &&
             angular.isString(this.phone_number) && 
             telephoneRegex.test(this.phone_number) &&
             angular.isString(this.id) &&
             angular.isString(this.name) && 
             !_.isEmpty(this.id) &&
             !_.isEmpty(this.address.formatted_address) &&
             !_.isEmpty(this.name);
    }
  }

  Location.build = function (location) {
    return new Location(deserializeLocation(location));
  }

  // Do we need a store.clear function here?
  Store.prototype = {
    get locationInfo() {
      return _.findWhere(this.locations, {id:this.locationId});
    },
    get telephoneRegex() {
      return telephoneRegex;
    },
    isValid: function() {
      var validLocations = _.reduce(this.locations, function (memo, loc) {
        return memo && loc.isValid();
      }, true);
      return angular.isNumber(this.tax_fee) && 
             this.tax_fee >=0 && 
             this.tax_fee <= 100 && 
             angular.isString(this.name) && 
             !_.isEmpty(this.name) &&
             validLocations;
    },
    // getLocationAddress: function() {
    //   return _.findWhere(this.locations, {id:this.locationId}).address.formatted_address;
    // },
    serialize: function() {
      var self = this;
      return {
        storeId: self._id,
        locationId: self.locationId
      };
    },
    $predictAddress: function(address) {
      return getAddress(address);
    },
    onSelectAddress: function (address, locationId) {
      angular.extend(this.address, address);
    },
    clear: function() {
      return;
    },
    $save: function() {
      var self = this;
      return StoreService.put.call(self).then(function (store) {
        notifyService("Successfully updated store information.", 'alert-success');
        return store;
      }, function (err) {
        notifyService("Could not update store information, please try again", 'alert-danger');
        return err;
      });
    }
  }

  Store.build = function (options) {
    return new Store(deserialize(options));
  }

  return Store;

}]);
