angular.module('note.srvcs',[
  'resource.notes'
])
.factory('Note', ['$filter','NoteService', 'STORE', function ($filter, NoteService, STORE) {

  function Note(options) {
    angular.extend(this, options);
    this.originalContent = angular.copy(options.content);
    this.isOpen = false;
  }

  Note.prototype = {
    get text() {
      if(this.hasNote()) {
        return 'Notes';
      } else {
        return 'Add Note';
      }
    },
    get popoverTemplateUrl() {
      return 'payments/templates/payments-note-popover.html';
    },
    get paymentsPopoverTitle() {
      return 'Notes for ' + $filter('date')(this.date, 'EEEE, MMMM d');
    },
    get paymentsDayTemplateUrl() {
      return 'payments/templates/payments-day-note-popover.html';
    },
    showPopover: function () {
      this.isOpen = !this.isOpen;
      if(!this.isOpen) {
        this.$save();
      }
    },
    serialize: function() {
      var self = this;
      return {
        _id: self._id,
        content: self.content,
        date: self.date,
        store: {
          storeId: self.store._id
        }
      }
    },
    $save: function() {
      var self = this;
      if(angular.isDefined(self._id) && self.hasChanged() && self.hasNote()) {
        return NoteService.put.call(self.serialize()).then(function (note) {
          console.log("note has successfully been updated");
          self.originalContent = self.content;
          return note
        }, function (err) {
          console.log("could not update note:  " + err);
          return err;
        });
      } else if(self.hasNote() && angular.isUndefined(self._id)) {
        return NoteService.post.call(self.serialize()).then(function (note) {
          console.log("note has successfully been posted");
          self._id = note._id;
          return note;
        }, function (err) {
          console.log("could not post note:  " + err)
          return err;
        });
      } else if(!self.hasNote() && angular.isDefined(self._id)) {
        return NoteService['delete'].call(self.serialize()).then(function (noteId) {
          delete self._id;
          return noteId;
        }, function (err) {
          console.log("could not delete note:  " + err);
          return err;
        });
      }
    },
    hasChanged: function () {
      return (this.originalContent !== this.content);
    },
    hasNote: function() {
      return !_.isEmpty(this.content) && angular.isString(this.content);
    }
  }

  Note.build = function (note) {
    note.store = STORE;
    return new Note(note);
  }

  return Note;

}])
