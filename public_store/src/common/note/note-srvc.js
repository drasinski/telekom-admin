angular.module('resource.notes', [
  'constants',
  'service.xhr'
])
.factory('NoteService', ['xhr', 'STORE','apiPrefix', function (xhr, STORE, apiPrefix) {

  return {
    get: function(query) {
      var x = angular.extend({per_page:'0',sort_by:'created_date'},query);
      return xhr([STORE.storePrefix+'/notes',{params:x, cache: false}]);
    },
    post: function () {
      return xhr('post',[apiPrefix+this.store.storeId+'/notes',this]);
    },
    put: function () {
      return xhr('put',[apiPrefix+this.store.storeId+'/notes/'+this._id, this]);
    },
    delete: function() {
      return xhr('delete',[apiPrefix+this.store.storeId+'/notes/'+this._id]);
    }
  };
  
}])
