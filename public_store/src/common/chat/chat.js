angular.module('chat', [
  'luegg.directives',
  'service.socket',
  'user.srvcs',
  'constants',
  'service.xhr',
  'angularMoment',
  'ajoslin.promise-tracker'
])
.factory('Message',[ 'User', function (User) {
  function Message(options) {
    angular.extend(this, options);
  }

  function deserialize(message) {
    if(angular.isDefined(message.created_date)) {
      message.created_date = new Date(message.created_date);
    }
    if(angular.isDefined(message.last_modified_date)) {
      message.last_modified_date = new Date(message.last_modified_date);
    }
    return message;
  }

  Message.prototype = {
    serialize: function() {
      return {
        content: this.content,
        type: this.type
      }
    }
  }

  Message.build = function (options) {
    if(angular.isDefined(options.user)) {
      options.user = User.build(options.user);
    }
    return new Message(deserialize(options));
  }

  return Message;

}])
.factory('MessageService', ['xhr','STORE','apiPrefix', function (xhr, STORE, apiPrefix) {
  return {
    get: function (query) {
      var x = angular.extend({per_page:'10',sort_by:'created_date,desc'},query);
      return xhr([STORE.storePrefix+'/messages',{params:x, cache: false}]);
    }
  }
}])
.factory('Chat', ['$timeout', 'User','Message','TYPING_TIMER_LENGTH','MessageService','promiseTracker', '$q', function ($timeout, User, Message, TYPING_TIMER_LENGTH, MessageService, promiseTracker, $q) {

  function Chat(options) {
    var self = this;
    angular.extend(self, options);
    this.isHidden = true;
    this.messages = [];
    this.isLoadingMessagesTracker = promiseTracker();
    this.page = 1; // this is page for current messages in the log
    this.unreadMessages = 0;
    this.typing = false;
    this.typingUsers = []
    // SOCKET LISTENERS
    this.socket.on('new message', function (message) {
      self.messages.push(Message.build(message));
      if(self.isHidden && (message.user._id !== self.currentUser._id)) {
        self.unreadMessages += 1;
      } else if(!self.isHidden) {
        self.socket.emit('read messages');
      }
    });

    this.socket.on('stop typing', function (data) {
      removeChatTyping.call(self, data);
    });

    this.socket.on('typing', function (data) {
      addChatTyping.call(self, data);
    });

    this.socket.on('user joined', function (message) {
      self.messages.push(Message.build(message));
      // if(self.isHidden) {
      //   self.unreadMessages += 1;
      // }
    });

    this.socket.on('user left', function (message) {
      self.messages.push(Message.build(message));
      // if(self.isHidden) {
      //   self.unreadMessages += 1;
      // }
    });

    this.socket.on('on join', function (message) {
      self.messages.push(Message.build(message));
    });

    this.socket.on('read messages', function (message) {
      self.unreadMessages = 0;
    });

  }

  function removeChatTyping(data) {
    this.typingUsers = _.reject(this.typingUsers, function (user) {
      return (user._id === data._id);
    });
  }

  function addChatTyping(data) {
    if(angular.isUndefined(_.findWhere(this.typingUsers, {_id: data._id}))) {
      this.typingUsers.push(User.build(data));
    }
  }

  Chat.prototype = {

    get typingMessage() {
      if(this.isTyping()) {
        return this.typingUsers[0].name + ' is typing...';
      } else {
        return '';
      }
    },

    sendMessage: function() {
      var message = Message.build({content: this.content, user: this.currentUser, type:'message', created_date: new Date().getTime()});
      this.messages.push(message)
      this.socket.emit('new message', message.serialize());
      this.content = '';
      this.typing = false;
      this.socket.emit('stop typing');
    },

    selfAuthored: function (message) {
      return message.user._id === this.currentUser._id;
    },

    toggle: function() {
      this.isHidden = !this.isHidden;
      if(!this.isHidden && (this.unreadMessages > 0 )) {
        this.unreadMessages = 0;
        this.socket.emit('read messages');
      }
    },

    isValidContent: function() {
      return angular.isString(this.content) && (this.content.length > 0);
    },

    isTyping: function() {
      return (this.typingUsers.length > 0);
    },

    updateTyping: function() {
      var self = this;
      if (!self.typing) {
        self.typing = true;
        self.socket.emit('typing');
      }
      self.lastTypingTime = (new Date()).getTime();

      $timeout(function () {
        var timeDiff = new Date().getTime() - self.lastTypingTime;
        if (timeDiff >= TYPING_TIMER_LENGTH && self.typing) {
          self.socket.emit('stop typing');
          self.typing = false;
        }
      }, TYPING_TIMER_LENGTH);
    },

    getMoreMessages: function() {
      var self = this;
      var deferred = $q.defer();
      if(!self.isLoadingMessagesTracker.active() && angular.isUndefined(self.noMoreMessages)) {
        var promise = MessageService.get({page:(self.page+1)});
        self.isLoadingMessagesTracker.addPromise(promise);
        promise.then(function (loadedMessages) {
          if(_.isEmpty(loadedMessages)) {
            self.messages.unshift({type:'notification', content: 'No more messages.', created_date: new Date(-8640000000000000).getTime()});
            self.noMoreMessages = true;
          } else {
            self.messages = self.messages.concat(_.map(loadedMessages, Message.build));
            self.page = self.page+1;
          }
          deferred.resolve();
        }, function (err) {
          deferred.reject(err);
        });
      }
      return deferred.promise;
    },

    initMessages: function (messages) {
      var self = this;
      self.messages = self.messages.concat(_.map(messages, Message.build));
    }

  }

  Chat.build = function (socket, currentUser) {
    return new Chat({socket:socket, currentUser:currentUser});
  }

  return Chat;

}])
.directive('whenScrolled', ['$timeout', function ($timeout) {
    return function (scope, elm, attr) {
        var raw = elm[0];
        elm.bind('scroll', function() {
            if (raw.scrollTop <= 100) {
                var sh = raw.scrollHeight
                scope.$apply(attr.whenScrolled).then(function() {
                    $timeout(function() {
                        raw.scrollTop = raw.scrollHeight - sh;
                    });
                });
            }
        });
    };
}])
.directive('chatBox', ['Chat', 'socketIO','MessageService', function (Chat, socketIO, MessageService) {
  return {
    restrict: 'EA',
    templateUrl: 'chat/templates/chat.html',
    replace: true,
    link: function($scope, iElement, iAttrs) {

      $scope.chat = Chat.build(socketIO.socket, $scope.currentUser);
      MessageService.get({page:1}).then(function (messages) {
        $scope.chat.initMessages(messages);
      });

    }
  };
}]);
