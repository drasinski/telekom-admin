angular.module('service.socket', [
  'btford.socket-io',
  'client.notifications',
  'invoice.notifications'
])
.factory('socketIO', ['$window','$http','socketFactory','deferredSocketFactory','STORE','clientNotifications','invoiceNotifications', function ($window, $http, socketFactory, deferredSocketFactory, STORE, clientNotifications, invoiceNotifications) {

  var deferred_socket = deferredSocketFactory();
  var socket = socketFactory({
    ioSocket: deferred_socket
  });

  socket.on('disconnect', function() {
    delete $http.defaults.headers.common['Socket-Id'];
  });

  invoiceNotifications(socket);
  clientNotifications(socket);

  return {
    connect: function() { 
      var realSocket = io.connect($window.location.host, { forceNew:true, query:'storeId=' + STORE._id });
      realSocket.on('connect', function() {
        $http.defaults.headers.common['Socket-Id'] = realSocket.id;
      });
      socket.swapSocket(realSocket);
      socket.disconnected = false;
    },
    disconnect: function() {
      socket.disconnect();
      socket.disconnected = true;
    },
    socket: socket
  }


}])