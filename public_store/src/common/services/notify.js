angular.module('service.notify', [
  'cgNotify'
])
.factory('notifyService', ['notify', function (notify) {

  notify.config({
    position:'right',
    templateUrl: 'services/templates/notify-template.html',
    startTop: 85
  });

  // success: true/false
  return function (message, alert_type , dur) {
    notify({
      // position:'right',
      message: message,
      classes: alert_type,
      duration: dur || 2250
    });
  }

}]);