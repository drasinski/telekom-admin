angular.module('messagelength',[])
.factory('messageLength', [function () {
    return function messageLength(str) {
      if (angular.isUndefined(str.length)) {return 0;}
      var l = str.length, lc = 0, chunks = [], c = 0, chunkSize = 140;
      for (; lc < l; c++) {
        chunks[c] = str.slice(lc, lc += chunkSize);
      }
      return chunks.length;
    };
}])