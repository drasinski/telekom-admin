angular.module('service.printer', [
])
.factory('printer', ['$rootScope', '$compile', '$templateCache', '$timeout','$q', function ($rootScope, $compile, $templateCache, $timeout, $q) {
    var printHtml = function (html) {
      var deferred = $q.defer();
      var hiddenFrame = $('<iframe style="display: none"></iframe>').appendTo('body')[0];
      hiddenFrame.contentWindow.printAndRemove = function() {
          hiddenFrame.contentWindow.print();
          $(hiddenFrame).remove();
      };
      var htmlContent = "<!doctype html>"+
                  "<html>"+
                      '<head><base href="/"></head>' +
                      '<body onload="printAndRemove();">' +
                          html +
                      '</body>'+
                  "</html>";
      var doc = hiddenFrame.contentWindow.document.open("text/html", "replace");
      doc.write(htmlContent);
      $('link[rel="stylesheet"]').each(function (i, ele) {
        var cssLink = hiddenFrame.contentWindow.document.createElement('link');
        cssLink.href = $(this).attr('href');
        cssLink.rel = 'stylesheet';
        cssLink.type = 'text/css';
        doc.getElementsByTagName("head")[0].appendChild(cssLink);
      });
      deferred.resolve();
      doc.close();
      return deferred.promise;
    };

    var openNewWindow = function (html) {
      var newWindow = window.open("printTest.html");
      newWindow.addEventListener('load', function(){ 
          $(newWindow.document.body).html(html);
      }, false);
    };

    var print = function (templateUrl, data) {
      var template = $templateCache.get(templateUrl);
      var printScope = $rootScope.$new()
      angular.extend(printScope, data);
      var element = $compile($('<div>' + template + '</div>'))(printScope);
      var waitForRenderAndPrint = function() {
          if(printScope.$$phase) {
              $timeout(waitForRenderAndPrint);
          } else {
              // Replace printHtml with openNewWindow for debugging
              printHtml(element.html());
              // openNewWindow(element.html());
              printScope.$destroy();
          }
      };
      waitForRenderAndPrint();
    };

    var printFromScope = function (templateUrl, scope) {
      var deferred = $q.defer();
      $rootScope.isBeingPrinted = true;
      var template = $templateCache.get(templateUrl);
      var printScope = scope;
      var element = $compile($('<div>' + template + '</div>'))(printScope);
      var waitForRenderAndPrint = function() {
          if (printScope.$$phase) {
              $timeout(waitForRenderAndPrint);
          } else {
            printHtml(element.html()).then(function() {
              $rootScope.isBeingPrinted = false;
              deferred.resolve();
            }, function() {
              deferred.reject();
            });
          }
      }
      waitForRenderAndPrint();
      return deferred.promise;
    };

    return {
        print: print,
        printFromScope:printFromScope
    }

}]);
