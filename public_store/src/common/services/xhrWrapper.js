angular.module('service.xhr',[])
.factory('xhr', ['$http','$q', function ($http, $q) {

  return function (type, config) {
    if (!config && angular.isArray(type)) {
      config = type;
      type = 'get';
    }
    var deferred = $q.defer();
    $http[type].apply($http, config)
    .success(function (data) {
      deferred.resolve(data);
    })
    .error(function (reason) {
      deferred.reject(reason);
    });
    return deferred.promise;
  };

}]);
