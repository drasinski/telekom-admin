angular.module('pageLoadSpinner', [
  'ajoslin.promise-tracker'
])

.factory('pageLoad', ['promiseTracker', function (promiseTracker) {
  return promiseTracker({
    activationDelay: 500
    // minDuration: 750
  });
}])
.factory('httpRequestTracker', ['$http', function ($http){

  var httpRequestTracker = {};
  httpRequestTracker.hasPendingRequests = function hasPendingRequests() {
    return $http.pendingRequests.length > 0;
  };

  return httpRequestTracker;
}])
.run(['$rootScope','pageLoad', function ($rootScope, pageLoad) {
  var deferred;

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    if(deferred) {
      deferred.reject();
    }
    deferred = pageLoad.createPromise();
  });

  $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    deferred.resolve();
  });

  $rootScope.$on('$stateChangeError', function () {
    deferred.resolve();
  });

}])

;
