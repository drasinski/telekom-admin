angular.module('service.address',[])
.factory('getAddress', ['$http', function ($http) {
  return function (address) {
    //, result_type:'street_address|administrative_area_level_1|locality|'
    var params = {address: address, sensor: false, components: 'country:USA'}; 
    return $http.get('http://maps.googleapis.com/maps/api/geocode/json',{params: params, headers: {'Socket-Id':undefined}}).then(function (res) {
      return _.map(res.data.results, function (item) {
        return {
          formatted_address: item.formatted_address,
          location: {
            coordinates: [ item.geometry.location.lng, item.geometry.location.lat ],
            type: 'Point'
          }
        }
      });
    });
  };
}]);