angular.module('resource.types', [
  'constants',
  'angular-data.DSCacheFactory',
  'service.xhr'
])
.factory('TypeService', ['$q','xhr','STORE','apiPrefix','DSCacheFactory', function ($q, xhr, STORE, apiPrefix, DSCacheFactory) {

  var DSCache = DSCacheFactory('typeCache', {
    maxAge: 43200000,
    cacheFlushInterval: 46800000,
    deleteOnExpire: 'aggressive'
  });


  return {
    get: function(query) {
      query = query || {};
      var cache = DSCache.get(JSON.stringify(query));
      if(angular.isDefined(cache)) {
        return $q.when(cache);
      } else {
        var x = angular.extend({per_page:'0',sort_by:'description'},query);
        return xhr([STORE.storePrefix+'/types',{params:x, cache: false }]).then(function (data) {
          DSCache.put(JSON.stringify(query), data);
          return data;
        });
      }
    },
    post: function () {
      var self = this;
      return xhr('post',[apiPrefix+this.store.storeId+'/types', this]).then(function (resp) {
        DSCache.removeAll();
        return resp;
      });
    },
    delete: function () {
      var self = this;
      return xhr('delete',[apiPrefix+this.store.storeId+'/types/'+this._id]).then(function (resp) {
        DSCache.removeAll();
        return resp;
      });
    },
    put: function () {
      var self = this;
      return xhr('put',[apiPrefix+this.store.storeId+'/types/'+this._id, this]).then(function (resp) {
        DSCache.removeAll();
        return resp;
      });
    }
  }


}])