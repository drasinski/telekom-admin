angular.module('transaction.srvcs',[
  'constants',
])
.factory('Transaction', ['OTHER','STORE','findEpaySKU', function ( OTHER, STORE, findEpaySKU) {

  function Transaction(options) {
    angular.extend(this, options);
    /* This is in cases of dailylog where phone has already been sold */
    var self = this;
  };

  function money_round(num) {
    return Math.round(num*100)/100;
  }

  function deserialize(trans) {
    var key = trans.inventoryId || trans.otherId || trans.typeId;
    trans.category_slug = key.category_slug || 'other';
    trans.category = key.category || 'Other';
    if(trans.inventoryId) {
      trans.icon = 'fa-mobile-phone';
    } else {
      trans.icon = key.icon || 'fa-question';
    }
    delete trans['total'];
    /* taxflag variable */
    trans.taxflag = (trans.taxtotal > 0);
    delete trans.taxtotal;
    trans.servicecharge = trans.servicecharge || 0;
    trans.servicechargeflag = (trans.servicecharge > 0);
    trans.amtsale = trans.amtsale || 0;
    trans.saleflag = (trans.amtsale > 0);
    return trans;
  }

  var choices = {
    'other': OTHER,
    'phone-sale':[]
  };

  Transaction.choices = choices;


  Transaction.prototype = {

    get epaySku() {
      return findEpaySKU.call(this);
    },

    serialize: function() {
      var obj = {
        taxtotal: this.taxtotal,
        total: this.total,
        servicecharge: this.servicecharge,
        amount: this.amount,
        amtsale: this.amtsale,
      }
      if(angular.isObject(this.inventoryId)) {
        obj.inventoryId = this.inventoryId._id;
      }
      if(angular.isObject(this.typeId)) {
        obj.typeId = this.typeId._id;
      }
      if(angular.isObject(this.otherId)) {
        obj.otherId = this.otherId;
      }
      return obj;
    },

    isValidAmount: function(key) {
      return angular.isNumber(this[key]);
    },

    hasSale: function () {
      return this.saleflag;
    },

    isEpayTrans: function() {
      return angular.isObject(this.typeId) && (this.typeId.system.replace(/[^\w]/,'').toLowerCase().indexOf('epay') > -1);
    },

    isPhoneBillTrans: function() {
      return angular.isObject(this.typeId) && (this.typeId.category_slug === 'phone-bill');
    },

    hasAmountPrediction: function() {
      return angular.isDefined(this.typeId) && angular.isArray(this.typeId.prediction) && (this.typeId.prediction.length > 1);
    },

    predictSrvcCharge: function() {
      var x = 0;
      if(angular.isObject(this.typeId) && angular.isUndefined(this.typeId._id)) {
        this.setTypeId();
      }
      if(angular.isDefined(this.getPrediction())) {
        return this.getPrediction().servicecharge
      } else {
        return 0;
      }
    },

    get taxtotal() {
      if(this.taxflag) {
        if(this.isPhoneBillTrans() && STORE.slug === "cellone") {
          return money_round(3/100*(this.amount-this.amtsale));
        } else {
          return money_round(STORE.tax_fee/100*(this.amount-this.amtsale));
        }
      } else {
        return 0;
      }
    },

    get total() {
      return this.amount + this.servicecharge - this.amtsale + this.taxtotal;
    },

    isValidDescAmount: function() {
      if(angular.isObject(this.typeId)) {
        return angular.isString(this.typeId.description) && (this.typeId.description.length >= 1) && angular.isDefined(this.typeId._id) && angular.isNumber(this.amount);
      } else if(angular.isObject(this.inventoryId)) {
        return angular.isString(this.inventoryId.description) && (this.inventoryId.description.length >= 1) && angular.isDefined(this.inventoryId._id) && angular.isNumber(this.amount);
      } else if(angular.isObject(this.otherId)) {
        return angular.isString(this.otherId.description) && (this.otherId.description.length >= 1) && angular.isNumber(this.otherId.fixed_cost) && angular.isNumber(this.amount);
      }
    },

    // Need to test this function invoice-small drtv.
    setNewType: function(type) {
      var obj = {};
      if(type.category_slug === 'other') {
        key = 'otherId';
      } else if(type.category_slug === 'phone-sale') {
        key = 'inventoryId';
      } else {
        key = 'typeId';
      }
      obj[key] = type;
      angular.extend(this, deserialize(obj));
    },

    isValid: function() {
      return angular.isNumber(this.taxtotal) && 
             angular.isNumber(this.amtsale) &&
             angular.isNumber(this.servicecharge) &&
             this.isValidDescAmount() &&
             (this.total >= 0);
    },

    isOther: function() {
      return (this.category_slug === 'other');
    },

    hasTax: function() {
      return this.taxflag;
    },

    setPrediction: function (item) {
      if(angular.isObject(item)) {
        this.taxflag = item.tax;
        this.setSrvcCharge(item.servicecharge);
      }
    },

    getPrediction: function() {
      var self = this;
      if(angular.isObject(self.typeId) && angular.isDefined(self.amount) && angular.isArray(self.typeId.prediction) && (self.typeId.prediction.length >= 1)) {
        return _.findWhere(self.typeId.prediction, {amount:self.amount });
      }
    },

    hasSrvcCharge: function() {
      return this.servicechargeflag;
    },

    addRemoveSale: function(val) {
      this.saleflag = !this.saleflag;
      if(this.saleflag) {
        this.amtsale = val || 5;
      } else {
        this.amtsale = 0;
      }
    },

    addRemoveTax: function() {
      this.taxflag = !this.taxflag;
    },

    setSrvcCharge: function(val) {
      if(val > 0) {
        this.servicechargeflag = true;
        this.servicecharge = val;
      } else if(val === 0) {
        this.servicechargeflag = false;
        this.servicecharge = 0;
      }
    },

    addRemoveSrvcCharge: function(val) {
      this.servicechargeflag = !this.servicechargeflag;
      if(this.servicechargeflag) {
        this.servicecharge = val || this.predictSrvcCharge();
      } else {
        this.servicecharge = 0;
      }
    },

    getChoices: function() {
      return choices[this.category_slug];
    },

    setPrice: function() {
      this.amount = this.inventoryId.amount;
      this.taxflag = true;
    },

    setTypeId: function() {
      var self = this;
      var type = _.findWhere(choices[self.typeId.category_slug], { description: self.typeId.description, category_slug: self.typeId.category_slug });
      angular.extend(self.typeId, type);
      angular.extend(self.typeId.prediction, type.prediction);
      if(angular.isArray(self.typeId.prediction) && (self.typeId.prediction.length >= 1)) {
        // if(angular.isUndefined(self.amount) ) {
          var x = _.findWhere(self.typeId.prediction, {default_pred: true });
          if(angular.isDefined(x)) {
            self.amount = x.amount;
            self.taxflag = x.tax;
            self.setSrvcCharge(x.servicecharge);
          } else {
            self.amount = 0;
          }
        // }
      }
    },

    addPhone: function(phone) {
      choices['phone-sale'].push(phone);
      this.inventoryId = phone;
      // angular.extend(this.inventoryId,phone)
      this.amount = phone.amount;
      this.taxflag = true;
    },

    getPhrase: function(language) {
      return getTransType.call(this,language);
    }

  };

  function getTransType(language) {
    var type = this.typeId || this.inventoryId || this.otherId;
    var transType = {
      'phone-bill' : type.description + ' '+ billHash[language],
      'internet-bill': type.description + ' ' + activationHash[language],
      'activation' : type.description + ' ' + activationHash[language],
      'internet-activation': type.description + ' ' + activationHash[language],
      'international': type.description +' '+ billHash[language],
      'utilities': type.description + ' '+ billHash[language],
      'phone-sale': type.description,
      'other':type.description
    };
    return transType[this.category_slug] || this.category;
  }

  /* private vars for invoice SMS */
  var activationHash = {
    'english':'activation',
    'czech':'aktivace',
    'spanish':'activacion',
    'ukraine':'активація',
    'polish':'aktywacja'
  };
  var billHash = {
    'polish':'rachunek',
    'english':'bill',
    'spanish':'facturar',
    'czech':'ucet',
    'ukraine':'законопроект'
  };

  /* Static method */
  Transaction.build = function (data, excludePhones) {
    return new Transaction(deserialize(data), excludePhones);
  };

  // [inventory, types]
  Transaction.initChoices = function (options) {
    var inventory = options[0];
    var types = options[1];
    /* reset choices array */
    for(prop in choices) {
      if(choices.hasOwnProperty(prop) && (prop !== 'other')) {
        delete choices[prop];
      }
    }
    if(angular.isArray(inventory)) {
      choices['phone-sale'] = angular.copy(inventory);
    }
    if(angular.isArray(types)) {
      angular.extend(choices, _.groupBy(types, 'category_slug'));
    }
  };

  return Transaction;

}])
.factory('findEpaySKU',[function() {

  return function () {
    var description = this.typeId.description.replace(/[^\w]/,'').toLowerCase();
    if(this.isEpayTrans()) {
      if(description.indexOf('ultra') > -1) {
        if(description.indexOf('wallet') > -1) {
          if(this.amount >= 2.5 && this.amount <= 10) {
            return "00843788026257";
          } else {
            return "00843788026264";
          }
        } else {
          switch(this.amount) {
            case 19:
              return "00843788021269";
            case 24:
              return "00843788028817";
            case 29:
              return "00843788015459";
            case 34:
              return "00843788028824";
            case 39:
              return "00843788015466";
            case 44:
              return "00843788028831";
            case 49:
              return "00843788015473";
            case 54:
              return "00843788028848";
            case 59:
              return "00843788017354";
            case 64:
              return "00843788028855";
            case 69:
              return "00843788028862";
            case 74:
              return "00843788028879";
            case 79:
              return "00843788028886";
            case 84:
              return "00843788028893";
            case 89:
              return "00843788028909";
            case 99:
              return "00843788028244";
          }
        }
      } else if(description.indexOf("lyca") > -1) {
        switch(this.amount) {
          case 10:
            return "0843788028442";
          case 20:
            return "0843788028459";
          case 23:
            return "0843788027452";
          case 29:
            return "0843788024918";
          case 35:
            return "0843788027469";
          case 39:
            return "0843788024925";
          case 45:
            return "0843788027476";
          case 49:
            return "0843788024932";
          case 50:
            return "0843788028466";
          case 55:
            return "0843788027483";
          case 59:
            return "0843788024949";
          case 100:
            return "0843788028473";          
        }
      } else if(description.indexOf("redpocket") > -1) {
        switch(this.amount) {
          case 12.99:
            return "0843788029234";
          case 19.99:
            return "0843788029098";
          case 22.99:
            return "0843788029241";
          case 24.99:
            return "0843788029104";
          case 29.99:
            return "0843788029111";
          case 34.99:
            return "0843788029128";
          case 39.99:
            return "0843788029135";
          case 49.99:
            return "0843788029142";
          case 54.99:
            return "0843788029159";
          case 59.99:
            return "0843788029166";
          case 69.99:
            return "0843788029173";
        }
      } else if(description.indexOf("tmobile") > -1) {
        return "0843788005092";
      } else if((description.indexOf("h20") > -1) || (description.indexOf("h2o") > -1)) {
        switch(this.amount) {
          case 30:
            return "0843788017163";
          case 35:
            return "0843788032111";
          case 40:
            return "0843788007362";
          case 50:
            return "0843788011499";
          case 60:
            return "0843788011505";
          case 65:
            return "0843788029968";
        }
      } else if((description.indexOf("simple") > -1)) {
        switch(this.amount) {
          case 25:
            return "0843788012656";
          case 40:
            return "0843788016432";
          // case 50: (TTD+ILD)
          //   return "0843788016449";
          case 55:
            return "0843788030216";
          case 65:
            return "0843788030230";
          // case 50: (TTD)
          //   return "0843788014353";
          case 60:
            return "0843788014360";
        }
      } else if((description.indexOf("gosmart") > -1)) {
        return "0843788015909";
      }
    }
  }

}])
;




