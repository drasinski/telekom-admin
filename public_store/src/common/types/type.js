angular.module('type.srvcs',[
  'resource.types',
  'store.srvcs',
  'service.notify'
])
.factory('Type', ['TypeService','notifyService','Store', function (TypeService, notifyService, Store) {

  function Type(options) {
    _.defaults(this, options, {
      prediction: [],
      active:true
    });
    if(_.isEmpty(this.profit)) {
      this.profit.push(new ProfitCalc({}));  
    }
    this.oldType = angular.copy(this.serialize());
  }

  function ProfitCalc(options) {
    _.defaults(this, options, {
        has_amount:true,
        amount_comm:100,
        amount_fixed:0,
        has_tax: false,
        tax_comm:100,
        tax_fixed:0,
        has_servicecharge:false,
        servicecharge_comm: 100,
        servicecharge_fixed:0,
        fixed_cost:0,
        start_date: moment().format(),
        end_date: undefined,
        end_date_max: moment().format()
    });
  }

  ProfitCalc.build = function (options) {
    return new ProfitCalc(deserializeProfit(options));
  }

  function deserializeProfit(options) {
    if(angular.isDefined(options.start_date)) {
      options.start_date = new Date(options.start_date);
      options.end_date_min = moment(options.start_date).format();
    }
    if(angular.isDefined(options.end_date)) {
      options.end_date = new Date(options.end_date);
    }
    return options;
  }

  ProfitCalc.prototype = {
    isValid: function() {
      return (this.has_tax || this.has_amount || this.has_servicecharge) &&
        angular.isNumber(this.amount_comm) &&
        (this.amount_comm >= 0) &&
        (this.amount_comm <= 100) &&
        angular.isNumber(this.amount_fixed) &&
        angular.isNumber(this.servicecharge_comm) &&
        (this.servicecharge_comm >= 0) &&
        (this.servicecharge_comm <= 100) &&
        angular.isNumber(this.servicecharge_fixed) &&
        angular.isNumber(this.tax_comm) &&
        (this.tax_comm >= 0) &&
        (this.tax_comm <= 100) &&
        angular.isNumber(this.tax_fixed) &&
        angular.isNumber(this.fixed_cost);
    },
    hasAmount: function() {
      return _.without([this.has_amount, this.has_tax, this.has_servicecharge ], false).length;
    },
    isCurrent: function() {
      return angular.isUndefined(this.end_date);
    },
    isNew: function() {
      return angular.isUndefined(this._id) || this.new_flag;
    }
  };

  function isValidPred() {
    return angular.isNumber(this.servicecharge) && 
           angular.isNumber(this.amount) && 
           angular.isDefined(this.default_pred) && 
           angular.isDefined(this.tax) &&
           (0 <= this.servicecharge) &&
           (0 <= this.amount);
  }

  Type.prototype = {

    get currentProfitCalc() {
      return _.findWhere(this.profit, {end_date:undefined});
    },

    revert: function() {
      if(this.hasChanged()) {
        angular.extend(this, this.oldType);
        this.store = Store.build(this.oldType.store);
        this.oldType = angular.copy(this.serialize());
      }
    },

    newProfitCalcIsDisabled: function() {
      return (_.filter(this.profit, function (profit) {
        return moment(profit.end_date).isSame(moment().subtract(1,'days'),'day') || moment(profit.start_date).isSame(moment(),'day');
      }).length > 0);
    },

    checkCurrent: function(profit) {
      return !profit.isCurrent();
    },

    changeStartDate: function (profit, old_end_date) {
      var self = this;
      var old_end_date = moment(old_end_date.replace(/["']/g, "")).add(1,'days');
      var upDateProfit = _.find(self.profit, function (p) {
        return moment(p.start_date).isSame(old_end_date,'day');
      });
      if(angular.isDefined(upDateProfit)) {
        var i = _.indexOf(self.profit, upDateProfit);
        self.profit[i].start_date = moment(profit.end_date).add(1,'days').format();
        self.profit[i].end_date_min = self.profit[i].start_date;
        if((i!==self.profit.length-1)&&(moment(self.profit[i].start_date).isAfter(self.profit[i].end_date,'day') || moment(self.profit[i].start_date).isSame(self.profit[i].end_date,'day'))) {
          // var x = _.find(self.profit, function (p) {
          //   return moment(self.profit[i].end_date).add('days',1).isSame(p.start_date,'day');
          // });
          // var idx = _.indexOf(self.profit, x);
          // console.log(idx);
          // if(idx >= 0) {
          //   self.profit[idx].start_date = moment(self.profit[i].start_date).add(1,'days').format();
          // }
          self.profit[i].end_date = self.profit[i].start_date;
          self.profit[i+1].start_date = moment(self.profit[i].start_date).add(1,'days').format();
        }
      }
      console.log(this.profit)
    },

    removeProfitCacl: function(profit) {
      var prev_day = moment(profit.start_date).subtract(1,'days');
      var prevProfit = _.find(this.profit, function (p) {
        return moment(p.end_date).isSame(prev_day,'day');
      });
      if(angular.isDefined(prevProfit)) {
        var prevProfitIdx = _.indexOf(this.profit, prevProfit);
        this.profit[prevProfitIdx].end_date = profit.end_date;
        this.profit[prevProfitIdx].end_date_max = moment(this.profit[prevProfitIdx].end_date_max).add(1,'days').format();
      }
      var i = _.indexOf(this.profit, profit);
      this.profit.splice(i, 1);
      if(i===0) {
        this.profit[0].start_date = profit.start_date;
        this.profit[0].end_date_min = profit.end_date_min;
      }
      console.log(this.profit);
    },

    addProfitCalc: function () {
      if(!this.newProfitCalcIsDisabled()) {
        var latestDate = this.profit.reduce(function (a, b) { return (a.end_date > b.end_date) ? a.end_date : b.end_date; });
        var profitCalc = ProfitCalc.build({start_date: new Date()});
        if(angular.isUndefined(this.currentProfitCalc.end_date)) {
          // this.currentProfitCalc.start_date = moment(latestDate).add(1,'day').format();
          this.currentProfitCalc.end_date_min = moment(this.currentProfitCalc.start_date).format();
          this.currentProfitCalc.end_date_max = moment().subtract(1,'day').format();
          this.currentProfitCalc.new_flag = true;
          this.currentProfitCalc.end_date = moment().subtract(1,'day').format();
        }
        if(this.profit.length >= 2) {
          this.profit[this.profit.length-2].end_date_max = moment(this.profit[this.profit.length-1].end_date_max).subtract(1,'days').format();
        }
        // this.currentProfitCalc.max_end_date = moment().subtract(1,'day').format();
        this.profit.push(profitCalc);
      }
    },

    isNewType: function() {
      return angular.isUndefined(this._id);
    },

    isValid: function() {
      return angular.isDefined(this.category) && angular.isDefined(this.system) && angular.isDefined(this.description) && this.isValidProfits() && this.isValidPredictions();
    },

    isValidProfits: function() {
      return _.reduce(this.profit, function (memo, profit) {
        return profit.isValid() && memo;
      },true);
    },

    hasChanged: function() {
      return !angular.equals(this.serialize(), this.oldType);
    },

    $save: function() {
      var self = this;
      if(this.isNewType()) {
        return TypeService.post.call(self.serialize()).then(function (type) {
          self._id = type._id;
          notifyService("Successfully created " + self.description + ".", 'alert-success');
          self.oldType = angular.copy(self.serialize());
          return type;
        }, function (err) {
          notifyService("Could not create " + self.description+".", 'alert-danger');
          return err;
        });
      } else {
        return TypeService.put.call(self.serialize()).then(function (type) {
          notifyService("Successfully updated " + self.description, 'alert-success');
          self.oldType = angular.copy(self.serialize());
          return type;
        }, function (err) {
          notifyService("Could not update " + self.description + " please try again.", 'alert-danger');
          return err;
        });
      }
    },

    $update: function() {
      if(!this.isNewType()) {
        return TypeService.put.call(this.serialize());
      }
    },

    $remove: function() {
      var self = this;
      return TypeService['delete'].call(self).then(function (resp) {
        notifyService(self.description + " was succesfully removed.", 'alert-success');
        return resp;
      }, function (err) {
        notifyService(self.description + " could not be removed. Please try again.", 'alert-danger');
        return err;
      });
    },

    addPred: function() {
      var default_pred = false;
      if(this.prediction.length === 0) {
        default_pred = true;
      }
      this.prediction.push({
        servicecharge: 0,
        amount:0,
        tax: false,
        default_pred: default_pred
      });
    },

    removePred: function(pred) {
      var i = this.prediction.indexOf(pred);
      if(i >= 0) {
        this.prediction.splice(i,1);
      }
    },

    getDefaultPred: function() {
      if(angular.isArray(this.prediction) && (this.prediction.length > 0)) {
        return _.findWhere(this.prediction, {default_pred: true}) || this.prediction[0];
      }
    },

    setDefaultPred: function(inputPred) {
      if(inputPred.default_pred) {
        this.prediction = _.map(this.prediction, function (pred) {
          if(inputPred !== pred) {
            pred.default_pred = false;
          }
          return pred;
        });
      }
    },

    isValidPredictions: function() {
      var self = this;
      return _.reduce(self.prediction, function (memo, val) {
        return memo && isValidPred.call(val);
      },true);
    },

    serialize: function() {
     return {
        _id: this._id,
       category: this.category,
       active: this.active,
       description: this.description,
       icon: this.icon,
       system: this.system,
       profit: this.profit,
       prediction: this.prediction,
       store: this.store.serialize()
     };
    }

  }

  Type.build = function (type) {
    type.profit = _.map(type.profit, ProfitCalc.build );
    type.store = Store.build(type.store);
    return new Type(deserialize(type));
  }

  function deserialize(type) {
    angular.isDefined(type.last_modified_date) && (type.last_modified_date = new Date(type.last_modified_date));
    if(angular.isDefined(type.created_date)) {
      type.created_date = new Date(type.created_date);
    } else {
      type.created_date = new Date();
    }
    return type;
  }

  return Type;

}])